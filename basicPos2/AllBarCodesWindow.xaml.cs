﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for AllBarCodesWindow.xaml
    /// </summary>
    public partial class AllBarCodesWindow : Window
    {
        ObservableCollection<BarcodeWithName> _items;
        public AllBarCodesWindow(ObservableCollection<BarcodeWithName> items)
        {
            _items = items;
            InitializeComponent();

            listBox.ItemsSource = items;


        }

        private void printBtn_Click(object sender, RoutedEventArgs e)
        {
            PrintDialog printDlg = new PrintDialog();
            if (printDlg.ShowDialog() == true)
            {
                printDlg.PrintVisual(listBox, "Bar-Code Printing.");
            }
                
        }
    }
}

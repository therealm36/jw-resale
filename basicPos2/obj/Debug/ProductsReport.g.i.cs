﻿#pragma checksum "..\..\ProductsReport.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "586D890C066F56DB7D71051BF5E58718"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using basicPos2;


namespace basicPos2 {
    
    
    /// <summary>
    /// ProductsReport
    /// </summary>
    public partial class ProductsReport : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 25 "..\..\ProductsReport.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\ProductsReport.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker Datestart;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\ProductsReport.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker Dateend;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\ProductsReport.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button addProductBtn;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\ProductsReport.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label1;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\ProductsReport.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label2;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\ProductsReport.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button excelBtn;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\ProductsReport.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button csvBtn;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\ProductsReport.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button pdfBtn;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\ProductsReport.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid produtsreportdataGrid;
        
        #line default
        #line hidden
        
        
        #line 109 "..\..\ProductsReport.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label grandtotallabel;
        
        #line default
        #line hidden
        
        
        #line 111 "..\..\ProductsReport.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox textBox;
        
        #line default
        #line hidden
        
        
        #line 112 "..\..\ProductsReport.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label totallabel;
        
        #line default
        #line hidden
        
        
        #line 113 "..\..\ProductsReport.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label totaldiscountlabel;
        
        #line default
        #line hidden
        
        
        #line 114 "..\..\ProductsReport.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label totalpaidlabel;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/basicPos2;component/productsreport.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\ProductsReport.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.label = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.Datestart = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 3:
            this.Dateend = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 4:
            this.addProductBtn = ((System.Windows.Controls.Button)(target));
            
            #line 67 "..\..\ProductsReport.xaml"
            this.addProductBtn.Click += new System.Windows.RoutedEventHandler(this.button5_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.label1 = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.label2 = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.excelBtn = ((System.Windows.Controls.Button)(target));
            
            #line 72 "..\..\ProductsReport.xaml"
            this.excelBtn.Click += new System.Windows.RoutedEventHandler(this.button1_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.csvBtn = ((System.Windows.Controls.Button)(target));
            
            #line 73 "..\..\ProductsReport.xaml"
            this.csvBtn.Click += new System.Windows.RoutedEventHandler(this.button2_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.pdfBtn = ((System.Windows.Controls.Button)(target));
            
            #line 74 "..\..\ProductsReport.xaml"
            this.pdfBtn.Click += new System.Windows.RoutedEventHandler(this.button3_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.produtsreportdataGrid = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 11:
            this.grandtotallabel = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.textBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 111 "..\..\ProductsReport.xaml"
            this.textBox.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.SelectAddress);
            
            #line default
            #line hidden
            
            #line 111 "..\..\ProductsReport.xaml"
            this.textBox.GotKeyboardFocus += new System.Windows.Input.KeyboardFocusChangedEventHandler(this.SelectAddress);
            
            #line default
            #line hidden
            
            #line 111 "..\..\ProductsReport.xaml"
            this.textBox.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.SelectivelyIgnoreMouseButton);
            
            #line default
            #line hidden
            
            #line 111 "..\..\ProductsReport.xaml"
            this.textBox.KeyDown += new System.Windows.Input.KeyEventHandler(this.textBox_KeyDown);
            
            #line default
            #line hidden
            return;
            case 13:
            this.totallabel = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.totaldiscountlabel = ((System.Windows.Controls.Label)(target));
            return;
            case 15:
            this.totalpaidlabel = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}


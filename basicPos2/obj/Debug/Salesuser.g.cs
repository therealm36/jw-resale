﻿#pragma checksum "..\..\Salesuser.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "32A4DF34E87AC4305B9DE228C016637F"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using basicPos2;


namespace basicPos2 {
    
    
    /// <summary>
    /// Salesuser
    /// </summary>
    public partial class Salesuser : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 26 "..\..\Salesuser.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\Salesuser.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label1;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\Salesuser.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label2;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\Salesuser.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button excelBtn;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\Salesuser.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button csvBtn;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\Salesuser.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button pdfBtn;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\Salesuser.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button addProductBtn;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\Salesuser.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid produtsdataGrid;
        
        #line default
        #line hidden
        
        
        #line 110 "..\..\Salesuser.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Datesearch;
        
        #line default
        #line hidden
        
        
        #line 111 "..\..\Salesuser.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label7_Copy1;
        
        #line default
        #line hidden
        
        
        #line 112 "..\..\Salesuser.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox StatusSearch;
        
        #line default
        #line hidden
        
        
        #line 115 "..\..\Salesuser.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox textBox;
        
        #line default
        #line hidden
        
        
        #line 116 "..\..\Salesuser.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Total;
        
        #line default
        #line hidden
        
        
        #line 117 "..\..\Salesuser.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Discount;
        
        #line default
        #line hidden
        
        
        #line 118 "..\..\Salesuser.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label GrandTotal;
        
        #line default
        #line hidden
        
        
        #line 119 "..\..\Salesuser.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Paid;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/basicPos2;component/salesuser.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Salesuser.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.label = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.label1 = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.label2 = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.excelBtn = ((System.Windows.Controls.Button)(target));
            
            #line 43 "..\..\Salesuser.xaml"
            this.excelBtn.Click += new System.Windows.RoutedEventHandler(this.button1_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.csvBtn = ((System.Windows.Controls.Button)(target));
            
            #line 44 "..\..\Salesuser.xaml"
            this.csvBtn.Click += new System.Windows.RoutedEventHandler(this.button2_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.pdfBtn = ((System.Windows.Controls.Button)(target));
            
            #line 45 "..\..\Salesuser.xaml"
            this.pdfBtn.Click += new System.Windows.RoutedEventHandler(this.button3_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.addProductBtn = ((System.Windows.Controls.Button)(target));
            
            #line 48 "..\..\Salesuser.xaml"
            this.addProductBtn.Click += new System.Windows.RoutedEventHandler(this.button5_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.produtsdataGrid = ((System.Windows.Controls.DataGrid)(target));
            
            #line 50 "..\..\Salesuser.xaml"
            this.produtsdataGrid.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.produtsdataGrid_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 12:
            this.Datesearch = ((System.Windows.Controls.TextBox)(target));
            
            #line 110 "..\..\Salesuser.xaml"
            this.Datesearch.GotKeyboardFocus += new System.Windows.Input.KeyboardFocusChangedEventHandler(this.SelectAddress);
            
            #line default
            #line hidden
            
            #line 110 "..\..\Salesuser.xaml"
            this.Datesearch.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.SelectivelyIgnoreMouseButton);
            
            #line default
            #line hidden
            
            #line 110 "..\..\Salesuser.xaml"
            this.Datesearch.KeyDown += new System.Windows.Input.KeyEventHandler(this.Datesearch_KeyDown);
            
            #line default
            #line hidden
            
            #line 110 "..\..\Salesuser.xaml"
            this.Datesearch.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.textBox_TextChanged);
            
            #line default
            #line hidden
            
            #line 110 "..\..\Salesuser.xaml"
            this.Datesearch.MouseUp += new System.Windows.Input.MouseButtonEventHandler(this.textBox_MouseUp);
            
            #line default
            #line hidden
            
            #line 110 "..\..\Salesuser.xaml"
            this.Datesearch.LostFocus += new System.Windows.RoutedEventHandler(this.textBox_LostFocus);
            
            #line default
            #line hidden
            return;
            case 13:
            this.label7_Copy1 = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.StatusSearch = ((System.Windows.Controls.TextBox)(target));
            
            #line 112 "..\..\Salesuser.xaml"
            this.StatusSearch.GotKeyboardFocus += new System.Windows.Input.KeyboardFocusChangedEventHandler(this.SelectAddress);
            
            #line default
            #line hidden
            
            #line 112 "..\..\Salesuser.xaml"
            this.StatusSearch.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.SelectivelyIgnoreMouseButton);
            
            #line default
            #line hidden
            
            #line 112 "..\..\Salesuser.xaml"
            this.StatusSearch.KeyDown += new System.Windows.Input.KeyEventHandler(this.StatusSearch_KeyDown);
            
            #line default
            #line hidden
            
            #line 112 "..\..\Salesuser.xaml"
            this.StatusSearch.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.textBox_TextChanged);
            
            #line default
            #line hidden
            
            #line 112 "..\..\Salesuser.xaml"
            this.StatusSearch.MouseUp += new System.Windows.Input.MouseButtonEventHandler(this.textBox_MouseUp);
            
            #line default
            #line hidden
            
            #line 112 "..\..\Salesuser.xaml"
            this.StatusSearch.LostFocus += new System.Windows.RoutedEventHandler(this.textBox_LostFocus);
            
            #line default
            #line hidden
            return;
            case 15:
            this.textBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 115 "..\..\Salesuser.xaml"
            this.textBox.GotKeyboardFocus += new System.Windows.Input.KeyboardFocusChangedEventHandler(this.SelectAddress);
            
            #line default
            #line hidden
            
            #line 115 "..\..\Salesuser.xaml"
            this.textBox.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.SelectivelyIgnoreMouseButton);
            
            #line default
            #line hidden
            
            #line 115 "..\..\Salesuser.xaml"
            this.textBox.KeyDown += new System.Windows.Input.KeyEventHandler(this.textBox_KeyDown);
            
            #line default
            #line hidden
            
            #line 115 "..\..\Salesuser.xaml"
            this.textBox.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.textBox_TextChanged);
            
            #line default
            #line hidden
            
            #line 115 "..\..\Salesuser.xaml"
            this.textBox.MouseUp += new System.Windows.Input.MouseButtonEventHandler(this.textBox_MouseUp);
            
            #line default
            #line hidden
            
            #line 115 "..\..\Salesuser.xaml"
            this.textBox.LostFocus += new System.Windows.RoutedEventHandler(this.textBox_LostFocus);
            
            #line default
            #line hidden
            return;
            case 16:
            this.Total = ((System.Windows.Controls.Label)(target));
            
            #line 116 "..\..\Salesuser.xaml"
            this.Total.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.SelectAddress);
            
            #line default
            #line hidden
            
            #line 116 "..\..\Salesuser.xaml"
            this.Total.GotKeyboardFocus += new System.Windows.Input.KeyboardFocusChangedEventHandler(this.SelectAddress);
            
            #line default
            #line hidden
            
            #line 116 "..\..\Salesuser.xaml"
            this.Total.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.SelectivelyIgnoreMouseButton);
            
            #line default
            #line hidden
            return;
            case 17:
            this.Discount = ((System.Windows.Controls.Label)(target));
            
            #line 117 "..\..\Salesuser.xaml"
            this.Discount.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.SelectAddress);
            
            #line default
            #line hidden
            
            #line 117 "..\..\Salesuser.xaml"
            this.Discount.GotKeyboardFocus += new System.Windows.Input.KeyboardFocusChangedEventHandler(this.SelectAddress);
            
            #line default
            #line hidden
            
            #line 117 "..\..\Salesuser.xaml"
            this.Discount.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.SelectivelyIgnoreMouseButton);
            
            #line default
            #line hidden
            return;
            case 18:
            this.GrandTotal = ((System.Windows.Controls.Label)(target));
            
            #line 118 "..\..\Salesuser.xaml"
            this.GrandTotal.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.SelectAddress);
            
            #line default
            #line hidden
            
            #line 118 "..\..\Salesuser.xaml"
            this.GrandTotal.GotKeyboardFocus += new System.Windows.Input.KeyboardFocusChangedEventHandler(this.SelectAddress);
            
            #line default
            #line hidden
            
            #line 118 "..\..\Salesuser.xaml"
            this.GrandTotal.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.SelectivelyIgnoreMouseButton);
            
            #line default
            #line hidden
            return;
            case 19:
            this.Paid = ((System.Windows.Controls.Label)(target));
            
            #line 119 "..\..\Salesuser.xaml"
            this.Paid.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.SelectAddress);
            
            #line default
            #line hidden
            
            #line 119 "..\..\Salesuser.xaml"
            this.Paid.GotKeyboardFocus += new System.Windows.Input.KeyboardFocusChangedEventHandler(this.SelectAddress);
            
            #line default
            #line hidden
            
            #line 119 "..\..\Salesuser.xaml"
            this.Paid.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.SelectivelyIgnoreMouseButton);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 9:
            
            #line 76 "..\..\Salesuser.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.viewbtn_Click);
            
            #line default
            #line hidden
            break;
            case 10:
            
            #line 81 "..\..\Salesuser.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.editbtn_Click);
            
            #line default
            #line hidden
            break;
            case 11:
            
            #line 86 "..\..\Salesuser.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.deletebtn_Click);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}


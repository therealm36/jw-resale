﻿#pragma checksum "..\..\Pos.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "6D092E45EB402B8764482079B1BFD7BB"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using basicPos2;


namespace basicPos2 {
    
    
    /// <summary>
    /// Pos
    /// </summary>
    public partial class Pos : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 44 "..\..\Pos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox customerCombo;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\Pos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button addCustomerBtn;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\Pos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox refNoTxt;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\Pos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox addItemCombo;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\Pos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid billGrid;
        
        #line default
        #line hidden
        
        
        #line 113 "..\..\Pos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label;
        
        #line default
        #line hidden
        
        
        #line 114 "..\..\Pos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label totalItemsLbl;
        
        #line default
        #line hidden
        
        
        #line 118 "..\..\Pos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy;
        
        #line default
        #line hidden
        
        
        #line 119 "..\..\Pos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label totalLbl;
        
        #line default
        #line hidden
        
        
        #line 123 "..\..\Pos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button discountBtn;
        
        #line default
        #line hidden
        
        
        #line 124 "..\..\Pos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label discountLbl;
        
        #line default
        #line hidden
        
        
        #line 128 "..\..\Pos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label_Copy2;
        
        #line default
        #line hidden
        
        
        #line 129 "..\..\Pos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label totalPayableLbl;
        
        #line default
        #line hidden
        
        
        #line 140 "..\..\Pos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button holdBtn;
        
        #line default
        #line hidden
        
        
        #line 141 "..\..\Pos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button cancelBtn;
        
        #line default
        #line hidden
        
        
        #line 142 "..\..\Pos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button orderBtn;
        
        #line default
        #line hidden
        
        
        #line 143 "..\..\Pos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button returnBtn;
        
        #line default
        #line hidden
        
        
        #line 144 "..\..\Pos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button paymentBtn;
        
        #line default
        #line hidden
        
        
        #line 152 "..\..\Pos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox catagoryCombo;
        
        #line default
        #line hidden
        
        
        #line 154 "..\..\Pos.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox productGridList;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/basicPos2;component/pos.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Pos.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.customerCombo = ((System.Windows.Controls.ComboBox)(target));
            
            #line 45 "..\..\Pos.xaml"
            this.customerCombo.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.customerCombo_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.addCustomerBtn = ((System.Windows.Controls.Button)(target));
            
            #line 46 "..\..\Pos.xaml"
            this.addCustomerBtn.Click += new System.Windows.RoutedEventHandler(this.addCustomerBtn_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.refNoTxt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.addItemCombo = ((System.Windows.Controls.ComboBox)(target));
            
            #line 54 "..\..\Pos.xaml"
            this.addItemCombo.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.addItemCombo_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 5:
            this.billGrid = ((System.Windows.Controls.DataGrid)(target));
            
            #line 55 "..\..\Pos.xaml"
            this.billGrid.AddingNewItem += new System.EventHandler<System.Windows.Controls.AddingNewItemEventArgs>(this.billGrid_AddingNewItem);
            
            #line default
            #line hidden
            
            #line 55 "..\..\Pos.xaml"
            this.billGrid.DataContextChanged += new System.Windows.DependencyPropertyChangedEventHandler(this.billGrid_DataContextChanged);
            
            #line default
            #line hidden
            return;
            case 9:
            this.label = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.totalItemsLbl = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.label_Copy = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.totalLbl = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.discountBtn = ((System.Windows.Controls.Button)(target));
            
            #line 123 "..\..\Pos.xaml"
            this.discountBtn.Click += new System.Windows.RoutedEventHandler(this.discountBtn_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.discountLbl = ((System.Windows.Controls.Label)(target));
            return;
            case 15:
            this.label_Copy2 = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.totalPayableLbl = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.holdBtn = ((System.Windows.Controls.Button)(target));
            return;
            case 18:
            this.cancelBtn = ((System.Windows.Controls.Button)(target));
            
            #line 141 "..\..\Pos.xaml"
            this.cancelBtn.Click += new System.Windows.RoutedEventHandler(this.cancelBtn_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.orderBtn = ((System.Windows.Controls.Button)(target));
            return;
            case 20:
            this.returnBtn = ((System.Windows.Controls.Button)(target));
            return;
            case 21:
            this.paymentBtn = ((System.Windows.Controls.Button)(target));
            
            #line 144 "..\..\Pos.xaml"
            this.paymentBtn.Click += new System.Windows.RoutedEventHandler(this.paymentBtn_Click);
            
            #line default
            #line hidden
            return;
            case 22:
            this.catagoryCombo = ((System.Windows.Controls.ComboBox)(target));
            
            #line 152 "..\..\Pos.xaml"
            this.catagoryCombo.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.catagoryCombo_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 23:
            this.productGridList = ((System.Windows.Controls.ListBox)(target));
            
            #line 154 "..\..\Pos.xaml"
            this.productGridList.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.productGridList_SelectionChanged);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 6:
            
            #line 68 "..\..\Pos.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click_1);
            
            #line default
            #line hidden
            break;
            case 7:
            
            #line 78 "..\..\Pos.xaml"
            ((System.Windows.Controls.TextBox)(target)).TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextBox_TextChanged);
            
            #line default
            #line hidden
            
            #line 78 "..\..\Pos.xaml"
            ((System.Windows.Controls.TextBox)(target)).LostFocus += new System.Windows.RoutedEventHandler(this.TextBox_LostFocus);
            
            #line default
            #line hidden
            
            #line 78 "..\..\Pos.xaml"
            ((System.Windows.Controls.TextBox)(target)).PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.TextBox_PreviewTextInput);
            
            #line default
            #line hidden
            break;
            case 8:
            
            #line 97 "..\..\Pos.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            break;
            case 24:
            
            #line 158 "..\..\Pos.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click_2);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}


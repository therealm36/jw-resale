﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for PerGramPricexaml.xaml
    /// </summary>
    public partial class PerGramPriceXaml : Window
    {
        public PerGramPriceXaml()
        {
            InitializeComponent();
            kt18Txt.Text = RealUtility.K18.ToString();
            kt21Txt.Text = RealUtility.K21.ToString();
            kt24Txt.Text = RealUtility.K22.ToString();

            
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            float num;
            if (float.TryParse(kt18Txt.Text,NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out num)
                 && float.TryParse(kt21Txt.Text, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out num)
                 && float.TryParse(kt24Txt.Text, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out num))
            {
                RealUtility.K18 = float.Parse(kt18Txt.Text);
                RealUtility.K21 = float.Parse(kt21Txt.Text);
                RealUtility.K22 = float.Parse(kt24Txt.Text);

                updateDb();
            }
            else
            {
                MessageBox.Show("Invalid values", "Invalid input");
            }


            this.Close();

        }

        private void updateDb()
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "Insert into tec_gold_price(date,kt24,kt22,kt18) Values ('" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + RealUtility.K22 + "','" + RealUtility.K21 + "','" + RealUtility.K18 + "')";
                cmd.ExecuteNonQuery();


            }
            catch (Exception)
            {
                MessageBox.Show("Failed to add to Price detail", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }
    }
}

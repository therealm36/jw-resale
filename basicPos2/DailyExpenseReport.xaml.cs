﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for DailyExpenseReport.xaml
    /// </summary>
    public partial class DailyExpenseReport : UserControl
    {
        public DailyExpenseReport()
        {
            InitializeComponent();
            produtsdataGrid.ItemsSource = basicPos2.models.DailyReport.DailyExpensereport(DateTime.Today.AddDays(-7).ToString(), DateTime.Today.ToString());
            Datestart.Text = DateTime.Today.AddDays(-7).ToString();
            Dateend.Text = DateTime.Today.ToString();
            totallabel.Content = ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.Total);
          
        }
        private void button5_Click(object sender, RoutedEventArgs e)
        {
            Datestart.BorderBrush = Brushes.Black;
            Dateend.BorderBrush = Brushes.Black;
            if (Datestart.SelectedDate == null)
            {
                Datestart.BorderBrush = Brushes.Red;
                MessageBox.Show("Please input a valid starting date");
            }
            else
            {
                if (Dateend.SelectedDate == null)
                {
                    Dateend.BorderBrush = Brushes.Red;
                    MessageBox.Show("Please input a valid ending date");
                }
                else
                {
                    produtsdataGrid.ItemsSource = basicPos2.models.DailyReport.DailyExpensereport(Datestart.Text, Dateend.Text);
                    totallabel.Content = ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.Total);

                }
            }
        }
        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {

            IEnumerable<basicPos2.models.DailyReport> backup = new ObservableCollection<basicPos2.models.DailyReport>((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource);

            if (e.Key == Key.Return)
            {
                if (!textBox.Text.ToString().Equals(""))
                {

                    IEnumerable<basicPos2.models.DailyReport> temp = new ObservableCollection<basicPos2.models.DailyReport>();
                    temp = ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Where(item => item.Date.ToLower().Contains(textBox.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                    totallabel.Content = ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.Total);

                }
                else
                {
                    textBox.Text = "[Date]";
                    produtsdataGrid.ItemsSource = backup;
                    produtsdataGrid.Items.Refresh();
                    totallabel.Content = ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.Total);

                }
            }
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            RealUtility.ExportToPdfexpensereport(produtsdataGrid);
        }




        private void button2_Click(object sender, RoutedEventArgs e)
        {
            RealUtility.Exporttocsvexpensereport(produtsdataGrid);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            try { 
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");
            RealUtility.GenerateExcelexpensereport(((ObservableCollection<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).ToDataTable());
            RealUtility.workBook.SaveAs(RealUtility.pathfornarnia+ "DailyExpensesReport--" + RealUtility.todaydate + ".xls", System.IO.FileMode.Create, FileAccess.ReadWrite);
            RealUtility.workBook.Close();
            RealUtility.excel.Quit();

            MessageBox.Show("Successfuly Exported as Excel File", "Success");
            }
            catch
            {
                MessageBox.Show("Please close all instances of excel and try again", "Error");
            }

        }
        private void SelectAddress(object sender, RoutedEventArgs e)

        {

            System.Windows.Controls.TextBox tb = (sender as System.Windows.Controls.TextBox);

            if (tb != null)

            {

                tb.SelectAll();

            }

        }



        private void SelectivelyIgnoreMouseButton(object sender,

            MouseButtonEventArgs e)

        {

            System.Windows.Controls.TextBox tb = (sender as System.Windows.Controls.TextBox);

            if (tb != null)

            {

                if (!tb.IsKeyboardFocusWithin)

                {

                    e.Handled = true;

                    tb.Focus();

                }

            }

        }
    }
}

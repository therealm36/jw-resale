﻿using basicPos2.models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for Catagories.xaml
    /// </summary>
    public partial class Catagories : UserControl
    {
        public Catagories()
        {
            InitializeComponent();
            produtsdataGrid.ItemsSource = Catagory.AllCatagories;
        }
        private void button_Click(object sender, RoutedEventArgs e)
        {

        }



        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void dataGrid_Loaded(object sender, RoutedEventArgs e)
        {


        }

        private void viewbtn_Click(object sender, RoutedEventArgs e)
        {
            Catagory i = (Catagory)produtsdataGrid.SelectedItem;
            ImageViewer imagepop = new ImageViewer(i.Image);
            imagepop.Show();

        }

        private void deletebtn_Click(object sender, RoutedEventArgs e)
        {

            Catagory.DeleteFromAllCatagories((Catagory)produtsdataGrid.SelectedItem);
        }
        private static popupcatagory editpop=null;
        private void editbtn_Click(object sender, RoutedEventArgs e)
        {
            if ( editpop==null||editpop.IsLoaded==false)
            {
                editpop = new popupcatagory(this, (Catagory)produtsdataGrid.SelectedItem);
                editpop.Title = "Edit Category";
                editpop.Show();
            }else
            {
                editpop.Activate();
            }

        }
        private static popupcatagory addProductsPop=null;
        private void button5_Click(object sender, RoutedEventArgs e)
        {
            if (addProductsPop == null||addProductsPop.IsLoaded == false)
            {
                addProductsPop = new popupcatagory(this, null);
                addProductsPop.Title = "Add Category";
                addProductsPop.Show();
            }else
            {
                addProductsPop.Activate();
            }

        }
        private void button3_Click(object sender, RoutedEventArgs e)
        {
            RealUtility.ExportToPdfcategories(produtsdataGrid);
        }




        private void button2_Click(object sender, RoutedEventArgs e)
        {
            RealUtility.Exporttocsvcategories(produtsdataGrid);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");
                RealUtility.GenerateExcelcategories(Catagory.AllCatagories.ToDataTable());
                RealUtility.workBook.SaveAs(RealUtility.pathfornarnia + "CategoriesList--" + RealUtility.todaydate + ".xls", System.IO.FileMode.Create, FileAccess.ReadWrite);
                RealUtility.workBook.Close();
                RealUtility.excel.Quit();

                MessageBox.Show("Successfuly Exported as Excel File", "Success");
            }
            catch
            {
                MessageBox.Show("Please close all instances of excel and try again", "Error");
            }


        }



        private void button6_Click(object sender, RoutedEventArgs e)
        {
            RealUtility.ImportFromExcelcategories();
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {


            (sender as System.Windows.Controls.Button).ContextMenu.IsEnabled = true;
            (sender as System.Windows.Controls.Button).ContextMenu.PlacementTarget = (sender as System.Windows.Controls.Button);
            (sender as System.Windows.Controls.Button).ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            (sender as System.Windows.Controls.Button).ContextMenu.IsOpen = true;
        }

        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {

            IEnumerable<Catagory> backup = new ObservableCollection<Catagory>((IEnumerable<Catagory>)produtsdataGrid.ItemsSource);
           
            if (e.Key == Key.Return)
            {
                if (!textBox.Text.ToString().Equals(""))
                {

                    IEnumerable<Catagory> temp = new ObservableCollection<Catagory>();
                    temp = ((IEnumerable<Catagory>)produtsdataGrid.ItemsSource).Where(item => item.Code.ToLower().Contains(textBox.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                }
                else
                {
                    if (textBox.Text.Equals("") && textBox_Copy.Text.Equals("[Name]") )
                    {
                        textBox.Text = "[Code]";
                        produtsdataGrid.ItemsSource = Catagory.AllCatagories;
                        produtsdataGrid.Items.Refresh();
                    }
                    else
                    {
                        textBox.Text = "[Code]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();
                    }
                }
            }
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void textBox_MouseUp(object sender, MouseButtonEventArgs e)
        {

        }
        private void SelectAddress(object sender, RoutedEventArgs e)

        {

            System.Windows.Controls.TextBox tb = (sender as System.Windows.Controls.TextBox);

            if (tb != null)

            {

                tb.SelectAll();

            }

        }



        private void SelectivelyIgnoreMouseButton(object sender,

            MouseButtonEventArgs e)

        {

            System.Windows.Controls.TextBox tb = (sender as System.Windows.Controls.TextBox);

            if (tb != null)

            {

                if (!tb.IsKeyboardFocusWithin)

                {

                    e.Handled = true;

                    tb.Focus();

                }

            }

        }

        private void textBox_Copy_MouseUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void textBox_Copy_KeyDown(object sender, KeyEventArgs e)
        {

            IEnumerable<Catagory> backup = new ObservableCollection<Catagory>((IEnumerable<Catagory>)produtsdataGrid.ItemsSource);
           
            if (e.Key == Key.Return)
            {
                if (!textBox_Copy.Text.ToString().Equals(""))
                {

                    IEnumerable<Catagory> temp = new ObservableCollection<Catagory>();
                    temp = ((IEnumerable<Catagory>)produtsdataGrid.ItemsSource).Where(item => item.Name.ToLower().Contains(textBox_Copy.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                }
                else
                {
                    if (textBox.Text.Equals("[Code]") && textBox_Copy.Text.Equals("") )
                    {
                        textBox_Copy.Text = "[Name]";
                        produtsdataGrid.ItemsSource = Catagory.AllCatagories;
                        produtsdataGrid.Items.Refresh();
                    }
                    else
                    {
                        textBox_Copy.Text = "[Name]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();
                    }
                }
            }
        }

        private void textBox_Copy1_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        

        

        private void textBox_Copy3_TextChanged(object sender, TextChangedEventArgs e)
        {


        }

        
        private void textBox_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox.Text = "[Code]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void textBox_Copy_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy.Text = "[Name]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void textBox_Copy1_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy1.Text = "[Type]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void textBox_Copy2_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy2.Text = "[Catagory]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void textBox_Copy3_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy3.Text = "[Quantity]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }
    }
    /* public class item :items
     {





         }
     public class item2 :items
     {

         public String Image { set; get; }



     }
     public class items {
         public BitmapImage Image { set; get; }
         public String Code { set; get; }
         public string Name { set; get; }
         public String Type { set; get; }
         public string Catagory { set; get; }
         public String Quantity { set; get; }
         public string Cost { set; get; }
         public string AlertQuantity { set; get; }

         public String Price { set; get; }
         public String Description { set; get; }
     }*/


}


﻿using basicPos2.models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for Customers.xaml
    /// </summary>
    public partial class Customers : UserControl
    {

        public Customers()
        {
            InitializeComponent();
            produtsdataGrid.ItemsSource = Customer.AllCustomers;


        }

        private void button_Click(object sender, RoutedEventArgs e)
        {

        }



        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void dataGrid_Loaded(object sender, RoutedEventArgs e)
        {


        }

        private void viewbtn_Click(object sender, RoutedEventArgs e)
        {
            //Product i = (Product)dataGrid.SelectedItem;
            //ImageViewer imagepop = new ImageViewer(i.Image);
            //imagepop.Show();

        }

        private void deletebtn_Click(object sender, RoutedEventArgs e)
        {

            Customer.DeleteFromAllCustomers((Customer)produtsdataGrid.SelectedItem);
        }
        private static CustomerWindow editpop = null;
        private void editbtn_Click(object sender, RoutedEventArgs e)
        {
            if (editpop == null||editpop.IsLoaded == false)
            {
                CustomerWindow editpop = new CustomerWindow((Customer)produtsdataGrid.SelectedItem);
                editpop.Title = "Edit Customer";
                editpop.Show();
            }else
            {
                editpop.Activate();
            }

        }
        private static CustomerWindow addProductsPop = null;
        private void button5_Click(object sender, RoutedEventArgs e)
        {
            if (addProductsPop == null||addProductsPop.IsLoaded == false)
            {
                addProductsPop = new CustomerWindow(null);
                addProductsPop.Title = "Add Customer";
                addProductsPop.Show();
            }else
            {
                addProductsPop.Activate();
            }

        }
        private void button3_Click(object sender, RoutedEventArgs e)
        {
            RealUtility.ExportToPdfCustomers(produtsdataGrid);
        }




        private void button2_Click(object sender, RoutedEventArgs e)
        {
            RealUtility.Exporttocsvcustomers(produtsdataGrid);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            try { 
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");
            RealUtility.GenerateExcelCustomers(Customer.AllCustomers.ToDataTable());
            RealUtility.workBook.SaveAs(RealUtility.pathfornarnia+ "CustomersList--" + RealUtility.todaydate + ".xls", System.IO.FileMode.Create, FileAccess.ReadWrite);
            RealUtility.workBook.Close();
            RealUtility.excel.Quit();

            MessageBox.Show("Successfuly Exported as Excel File", "Success");
            }
            catch
            {
                MessageBox.Show("Please close all instances of excel and try again", "Error");
            }

        }



        private void button6_Click(object sender, RoutedEventArgs e)
        {
            // RealUtility.ImportFromExcel();
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {


            (sender as System.Windows.Controls.Button).ContextMenu.IsEnabled = true;
            (sender as System.Windows.Controls.Button).ContextMenu.PlacementTarget = (sender as System.Windows.Controls.Button);
            (sender as System.Windows.Controls.Button).ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            (sender as System.Windows.Controls.Button).ContextMenu.IsOpen = true;
        }

        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {

            IEnumerable<Customer> backup = new ObservableCollection<Customer>((IEnumerable<Customer>)produtsdataGrid.ItemsSource);
         
            if (e.Key == Key.Return)
            {
                if (!textBox.Text.ToString().Equals(""))
                {

                    IEnumerable<Customer> temp = new ObservableCollection<Customer>();
                    temp = ((IEnumerable<Customer>)produtsdataGrid.ItemsSource).Where(item => item.Name.ToLower().Contains(textBox.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                }
                else
                {
                    if (textBox.Text.Equals("") && textBox_Copy1.Text.Equals("[Phone]") && textBox_Copy3.Text.Equals("[Custom Field]") && createdby_Copy3.Text.Equals("[Custom Field]") && textemail.Text.Equals("[Email]"))
                    {
                        textBox.Text = "[Name]";
                        produtsdataGrid.ItemsSource = Customer.AllCustomers;
                        produtsdataGrid.Items.Refresh();
                    }
                    else
                    {
                        textBox.Text = "[Name]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();
                    }
                }
            }
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void textBox_MouseUp(object sender, MouseButtonEventArgs e)
        {

        }
        private void SelectAddress(object sender, RoutedEventArgs e)

        {

            System.Windows.Controls.TextBox tb = (sender as System.Windows.Controls.TextBox);

            if (tb != null)

            {

                tb.SelectAll();

            }

        }



        private void SelectivelyIgnoreMouseButton(object sender,

            MouseButtonEventArgs e)

        {

            System.Windows.Controls.TextBox tb = (sender as System.Windows.Controls.TextBox);

            if (tb != null)

            {

                if (!tb.IsKeyboardFocusWithin)

                {

                    e.Handled = true;

                    tb.Focus();

                }

            }

        }

        private void textBox_Copy_MouseUp(object sender, MouseButtonEventArgs e)
        {

        }


        private void textBox_Copy1_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void textBox_Copy1_KeyDown(object sender, KeyEventArgs e)
        {
            IEnumerable<Customer> backup = new ObservableCollection<Customer>((IEnumerable<Customer>)produtsdataGrid.ItemsSource);

            if (e.Key == Key.Return)
            {
                if (!textBox_Copy1.Text.ToString().Equals(""))
                {

                    IEnumerable<Customer> temp = new ObservableCollection<Customer>();
                    temp = ((IEnumerable<Customer>)produtsdataGrid.ItemsSource).Where(item => item.Phone.ToLower().Contains(textBox_Copy1.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                }
                else
                {
                    if (textBox.Text.Equals("[Name]") && textBox_Copy1.Text.Equals("") && textBox_Copy3.Text.Equals("[Custom Field]") && createdby_Copy3.Text.Equals("[Custom Field]") && textemail.Text.Equals("[Email]"))
                    {
                        textBox_Copy1.Text = "[Phone]";
                        produtsdataGrid.ItemsSource = Customer.AllCustomers;
                        produtsdataGrid.Items.Refresh();
                    }
                    else
                    {
                        textBox_Copy1.Text = "[Phone]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();
                    }
                }
            }
        }



        private void textBox_Copy3_TextChanged(object sender, TextChangedEventArgs e)
        {


        }

        private void textBox_Copy3_KeyDown(object sender, KeyEventArgs e)
        {
            IEnumerable<Customer> backup = new ObservableCollection<Customer>((IEnumerable<Customer>)produtsdataGrid.ItemsSource);

            if (e.Key == Key.Return)
            {
                if (!textBox_Copy3.Text.ToString().Equals(""))
                {

                    IEnumerable<Customer> temp = new ObservableCollection<Customer>();
                    temp = ((IEnumerable<Customer>)produtsdataGrid.ItemsSource).Where(item => item.Cf1.ToLower().Contains(textBox_Copy3.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                }
                else
                {
                    if (textBox.Text.Equals("[Name]") && textBox_Copy1.Text.Equals("[Phone]") && textBox_Copy3.Text.Equals("") && createdby_Copy3.Text.Equals("[Custom Field]") && textemail.Text.Equals("[Email]"))
                    {
                        textBox_Copy3.Text = "[Custom Field]";
                        produtsdataGrid.ItemsSource = Customer.AllCustomers;
                        produtsdataGrid.Items.Refresh();
                    }
                    else
                    {
                        textBox_Copy3.Text = "[Custom Field]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();
                    }
                }
            }
        }

        private void textBox_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox.Text = "[Code]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void textBox_Copy_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy.Text = "[Name]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void textBox_Copy1_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy1.Text = "[Type]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void textBox_Copy2_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy2.Text = "[Catagory]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void textBox_Copy3_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy3.Text = "[Quantity]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void createdby_KeyDown(object sender, KeyEventArgs e)
        {
            IEnumerable<Customer> backup = new ObservableCollection<Customer>((IEnumerable<Customer>)produtsdataGrid.ItemsSource);

            if (e.Key == Key.Return)
            {
                if (!createdby_Copy3.Text.ToString().Equals(""))
                {

                    IEnumerable<Customer> temp = new ObservableCollection<Customer>();
                    temp = ((IEnumerable<Customer>)produtsdataGrid.ItemsSource).Where(item => item.Cf2.ToLower().Contains(createdby_Copy3.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                }
                else
                {
                    if (textBox.Text.Equals("[Name]") && textBox_Copy1.Text.Equals("[Phone]") && textBox_Copy3.Text.Equals("[Custom Field]") && createdby_Copy3.Text.Equals("") && textemail.Text.Equals("[Email]"))
                    {
                        createdby_Copy3.Text = "[Custom Field]";
                        produtsdataGrid.ItemsSource = Customer.AllCustomers;
                        produtsdataGrid.Items.Refresh();
                    }
                    else
                    {
                        createdby_Copy3.Text = "[Custom Field]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();
                    }
                }
            }
        }

        private void textemail_KeyDown(object sender, KeyEventArgs e)
        {
            IEnumerable<Customer> backup = new ObservableCollection<Customer>((IEnumerable<Customer>)produtsdataGrid.ItemsSource);

            if (e.Key == Key.Return)
            {
                if (!textemail.Text.ToString().Equals(""))
                {

                    IEnumerable<Customer> temp = new ObservableCollection<Customer>();
                    temp = ((IEnumerable<Customer>)produtsdataGrid.ItemsSource).Where(item => item.Email.ToLower().Contains(textemail.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                }
                else
                {
                    if (textBox.Text.Equals("[Name]") && textBox_Copy1.Text.Equals("[Phone]") && textBox_Copy3.Text.Equals("[Custom Field]") && createdby_Copy3.Text.Equals("[Custom Field]") && textemail.Text.Equals(""))
                    {
                        textemail.Text = "[Email]";
                        produtsdataGrid.ItemsSource = Customer.AllCustomers;
                        produtsdataGrid.Items.Refresh();
                    }
                    else
                    {
                        textemail.Text = "[Email]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();
                    }
                }
            }
        }
    }
    /* public class item :items
     {





         }
     public class item2 :items
     {

         public String Image { set; get; }



     }
     public class items {
         public BitmapImage Image { set; get; }
         public String Code { set; get; }
         public string Name { set; get; }
         public String Type { set; get; }
         public string Catagory { set; get; }
         public String Quantity { set; get; }
         public string Cost { set; get; }
         public string AlertQuantity { set; get; }

         public String Price { set; get; }
         public String Description { set; get; }
     }*/


}


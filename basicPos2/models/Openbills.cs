﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace basicPos2.models
{
    public partial class Openbills : INotifyPropertyChanged
    {
        int _id;
        string _name;
        string _reference;
        string _date;
        string _total_items;
        string _grand_total;


        //string _image;


        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
                RaisePropertyChanged("Id");
            }
        }

        public string CustomerName
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
                RaisePropertyChanged("CustomerName");
            }
        }

        public string Date
        {
            get
            {
                return _date;
            }

            set
            {
                _date = value;
                RaisePropertyChanged("Date");
            }
        }
        public string TotalItems
        {
            get
            {
                return _total_items;
            }

            set
            {
                _total_items = value;
                RaisePropertyChanged("TotalItems");
            }
        }
        public string GrandTotal
        {
            get
            {
                return _grand_total;
            }

            set
            {
                _grand_total = value;
                RaisePropertyChanged("GrandTotal");
            }
        }

        public string Reference
        {
            get
            {
                return _reference;
            }

            set
            {
                _reference = value;
                RaisePropertyChanged("Reference");
            }
        }




        /*public string Image
        {
            get
            {
                return _image;
            }

            set
            {
                _image = value;
                RaisePropertyChanged("Image");
            }
        }*/




        private void RaisePropertyChanged(string propertyName)
        {

            if (null != PropertyChanged)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }


    partial class Openbills
    {
        static ObservableCollection<Openbills> _allopenbills = new ObservableCollection<Openbills>();


        public static ObservableCollection<Openbills> Allopenbills
        {
            get
            {
                if (_allopenbills.Count == 0)
                {

                    getFromDatabase();
                }

                return _allopenbills;
            }
        }
        
        public static void DeleteFromAllOpenBills(Openbills item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_suspended_sales WHERE id=" + item.Id;
                cmd.ExecuteNonQuery();
                _allopenbills.Remove(item);

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to delete", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }
        

        public static void ClearAllSuppliers()
        {
            _allopenbills.Clear();
        }

        
        public static void getFromDatabase()
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT *,Date_format(date,'%d/%m/%Y') as x FROM tec_suspended_sales";
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new Openbills();
                            product.Id = int.Parse(reader["id"].ToString());
                            product.CustomerName = reader["customer_name"].ToString();
                            product.Reference = reader["hold_ref"].ToString();
                            product.Date = reader["x"].ToString();
                            product.TotalItems = reader["total_items"].ToString()+"("+ reader["total_quantity"].ToString()+")";
                            product.GrandTotal = reader["grand_total"].ToString();




                            _allopenbills.Insert(0,product);
                        }

                    }
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Load from database", "Error-Openned Bills");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }
    }
}

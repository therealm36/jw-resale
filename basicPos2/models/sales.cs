﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using MySql.Data.MySqlClient;
using System.Windows;
using System.Data.SqlClient;

namespace basicPos2.models
{
   public partial class Sales:INotifyPropertyChanged
    {
        int _id;
        string _date;
        int _customerId;
        string _customerName;
        float _total;
        float _productDiscount;
        float _orderDiscount;
        float _totalDiscount;
        float _grandTotal;
        int _totalItems;
        float _totalQuantity;
        float _paid;
        int createdBy;
        string _note;
        int _storeId;
        float _status;
        ObservableCollection<SaleItem> _saleItems;
        public ObservableCollection<SaleItem> SaleItems
        {
            get
            {
                return _saleItems;
            }

            set
            {
                _saleItems = value;
            }
        }

        public float ProductDiscount
        {
            get
            {
                try
                {
                    return _saleItems.Sum(item => item.ItemDiscount * item.Quantity);
                }
                catch
                {
                    return 0;
                }
                
            }

            set
            {
                _productDiscount = value;
                RaisePropertyChanged("ProductDiscount");
                RaisePropertyChanged("TotalDiscount");
                RaisePropertyChanged("GrandTotal");
            }
        }
        public int TotalItems
        {
            get
            {
                try
                {
                    return _saleItems.Count;
                }
                catch
                {
                    return 0;
                }
            }

            set
            {
                _totalItems = value;
            }
        }

        public float TotalQuantity
        {
            get
            {
                try
                {
                    return _saleItems.Sum(item => item.Quantity);
                }
                catch { return 0; }
            }

            set
            {
                _totalQuantity = value;
            }
        }

        public int CreatedBy
        {
            get
            {
                try
                {
                    return createdBy;
                }
                catch { return 0; }
            }

            set
            {
                createdBy = value;
            }
        }

        public string Note
        {
            get
            {
                return _note;
            }

            set
            {
                _note = value;
            }
        }

        public int StoreId
        {
            get
            {
                try
                {
                    return _storeId;
                }
                catch
                {
                    return 0;
                }
            }

            set
            {
                _storeId = value;
            }
        }
        public float OrderDiscount
        {
            get
            {
                try
                {
                    return _orderDiscount;
                }
                catch
                {
                    return 0;
                }
            }

            set
            {
                _orderDiscount = value;
                RaisePropertyChanged("OrderDiscount");
                RaisePropertyChanged("TotalDiscount");
                RaisePropertyChanged("GrandTotal");
            }
        }

        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        

        public int CustomerId
        {
            get
            {
                try
                {
                    return _customerId;
                }
                catch { return 0; }
            }

            set
            {
                _customerId = value;
            }
        }
        public string Date
        {
            get
            {
                return _date;
            }

            set
            {
                _date = value;
            }
        }
        public string CustomerName
        {
            get
            {
                return _customerName;
            }

            set
            {
                _customerName = value;
                RaisePropertyChanged("CustomerName");
            }
        }

        public float Total
        {
            get
            {
                return _total;
            }

            set
            {
                _total = value;
                RaisePropertyChanged("Total");
                RaisePropertyChanged("GrandTotal");
            }
        }

       
       
        public float TotalDiscount
        {
            get
            {
                return _orderDiscount + _productDiscount;
            }

            set
            {
                _totalDiscount = value;
            }
        }

        public float GrandTotal
        {
            get
            {
                return _grandTotal;

            }

            set
            {
                _grandTotal = value;
                RaisePropertyChanged("Status");

                RaisePropertyChanged("GrandTotal");
            }
        }

       
        public float Paid
        {
            get
            {
                return _paid;
            }

            set
            {
                _paid = value;
                RaisePropertyChanged("Status");
                RaisePropertyChanged("Paid");
            }
        }
        public float Status
        {
            get
            {
                return _grandTotal - _paid; ;
            }

            set
            {
                _status = value;
            }
        }

       

       
        public void RaisePropertyChanged(string propertyName)
        {

            if (null != PropertyChanged)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;

       
    }

    public partial class Sales
    {

        public static Sales currentSale = new Sales { SaleItems=SaleItem.CurrentBill};

      
        static ObservableCollection<Sales> _allsales = new ObservableCollection<Sales>();



        public static ObservableCollection<Sales> AllSales
        {
            get
            {
                if (_allsales.Count == 0)
                {

                    getFromDatabase();
                }

                return _allsales;
            }
        }

        public static void AddToAllCatagories(Sales item)
        {


            _allsales.Insert(0,item);

        }

        public static void DeleteFromAllSales(Sales item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_sales WHERE id=" + item.Id;
                cmd.ExecuteNonQuery();
                _allsales.Remove(item);

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to delete", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }
        public static void editCategory(int i, Sales item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_sales WHERE id=" + item.Id;
                cmd.ExecuteNonQuery();
                cmd.CommandText = "Insert into tec_sales(id,date,customer_id,customer_name,total,product_discount,order_discount,total_discount,grand_total,total_items,total_quantity,paid,created_by,note,store_id,status) Values ('" + item.Id + "','" + Convert.ToDateTime(item.Date).ToString("yyyy/MM/dd") + "','" + item.CustomerId + "','" + item.CustomerName + "','" + item.Total + "','" + item.ProductDiscount + "','" + item.OrderDiscount + "','" + item.TotalDiscount + "','" + item.GrandTotal + "','" + item.TotalItems + "','" + item.TotalQuantity + "','" + item.Paid + "','" + item.CreatedBy + "','" + item.Note + "','" + item.StoreId + "','" + item.Status + "')";
                cmd.ExecuteNonQuery();
                _allsales.RemoveAt(i);
                _allsales.Insert(i, item);

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to edit", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        public static void ClearAllSales()
        {
            _allsales.Clear();
        }

        public override string ToString()
        {
            return Date + " (" + CustomerName + " )";

        }






        public static void getFromDatabase()
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT * FROM tec_sales";
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new Sales();
                            product.Id = int.Parse(reader["id"].ToString());
                            product.Date = reader["date"].ToString();
                            product.CustomerId =int.Parse( reader["customer_id"].ToString());
                            product.CustomerName = reader["customer_name"].ToString();
                            var sales = new Sales();
                            sales.Id = int.Parse(reader["id"].ToString());
                            sales.Date = reader["date"].ToString();
                            sales.CustomerId =int.Parse( reader["customer_id"].ToString());
                            sales.CustomerName = reader["customer_name"].ToString();
                            sales.Total = float.Parse(reader["total"].ToString());
                            sales.ProductDiscount = float.Parse(reader["product_discount"].ToString());
                            sales.OrderDiscount = float.Parse(reader["order_discount"].ToString());
                            sales.TotalDiscount = float.Parse(reader["total_discount"].ToString());
                            sales.GrandTotal = float.Parse(reader["grand_total"].ToString());
                            sales.TotalItems = int.Parse(reader["total_items"].ToString());
                            sales.TotalQuantity = float.Parse(reader["total_quantity"].ToString());
                            sales.Paid = float.Parse(reader["paid"].ToString());
                            sales.CreatedBy = int.Parse(reader["created_by"].ToString());
                            sales.Note = reader["note"].ToString();
                            
                            sales.StoreId = int.Parse(reader["store_id"].ToString());
                            _allsales.Insert(0,sales);
                        }

                    }
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Load from database", "Error-Sales");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        public static Sales getSalesItems(Sales sale)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT * FROM tec_sale_items where sale_id='"+sale.Id+"'"; 
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        var items = new ObservableCollection<SaleItem>();
                        while (reader.Read())
                        {
                            
                            SaleItem slaesItem = new SaleItem();
                            slaesItem.Id = int.Parse(reader["id"].ToString());
                            slaesItem.ProductId = reader["product_id"].ToString();
                            slaesItem.Quantity = float.Parse(reader["quantity"].ToString());
                      
                            slaesItem.ItemDiscount = float.Parse(reader["discount"].ToString());
                            slaesItem.RealUnitPrice = float.Parse(reader["real_unit_price"].ToString());
                            slaesItem.ProductCode = reader["product_code"].ToString();
                            slaesItem.ProductName = reader["product_name"].ToString();

                            items.Add(slaesItem);
                        }
                        sale.SaleItems = items;
                        
                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error-Sales");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                    
                }
                
            }
            return sale;

        }

        public static void RunSalesTransaction(string myConnString, string paymentNote, string paiBy, Pos _pos)
        {
            MySqlConnection myConnection = new MySqlConnection(RealUtility.dbString);
            myConnection.Open();

            MySqlCommand myCommand = myConnection.CreateCommand();
            MySqlTransaction myTrans;

      
            myTrans = myConnection.BeginTransaction();
            myCommand.Connection = myConnection;
            myCommand.Transaction = myTrans;

            try
            {
                currentSale.Date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                var item = currentSale;
                myCommand.CommandText = "Insert into tec_sales(date,customer_id,customer_name,total,product_discount,order_discount,total_discount,grand_total,total_items,total_quantity,paid,created_by,note,store_id,status) Values ('" + item.Date + "','" + item.CustomerId + "','" + item.CustomerName + "','" + item.Total + "','" + item.ProductDiscount + "','" + item.OrderDiscount + "','" + item.TotalDiscount + "','" + item.GrandTotal + "','" + item.TotalItems + "','" + item.TotalQuantity + "','" + item.Paid + "','" + item.CreatedBy + "','" + item.Note + "','" + item.StoreId + "','" + item.Status + "')";
                myCommand.ExecuteNonQuery();

                int id = Convert.ToInt32((long) myCommand.LastInsertedId);

                currentSale.Id = id;
                

                //Payment m = new Payment { Date = currentSale.Date, Amount = currentSale.Paid, PaidBy = paiBy, Reference = paymentNote, SaleID = currentSale.Id };
               // myCommand.CommandText = "Insert into tec_payments(date,sale_id,customer_id,paid_by,amount,reference) Values ('" + item.Date + "','" + m.SaleID + "','" +currentSale.CustomerId+"','"+ m.PaidBy + "','" + m.Amount + "','" + m.Reference + "')";
               // myCommand.ExecuteNonQuery();

                id = Convert.ToInt32((long)myCommand.LastInsertedId);

                //m.Id = id;
                

                var salesItems = currentSale.SaleItems;
                for (int i=0; i < currentSale.SaleItems.Count; i++)
                {
                    myCommand.CommandText = "Insert into tec_sale_items(sale_id,product_id,quantity,unit_price,net_unit_price, discount,item_discount,subtotal,real_unit_price,cost,product_code,product_name,comment) Values ('" 
                        + currentSale.Id + "','" + salesItems[i].ProductId + "','" + salesItems[i].Quantity + "','" + salesItems[i].UnitPrice + "','" + salesItems[i].UnitPrice + "','"+ salesItems[i].ItemDiscount + "','" + salesItems[i].ItemDiscount*salesItems[i].Quantity + "','" + salesItems[i].SubTotal + "','" + salesItems[i].RealUnitPrice + "','" + salesItems[i].Cost + "','" + salesItems[i].ProductCode + "','" + salesItems[i].ProductName + "','" + salesItems[i].Comment + "')";
                    myCommand.ExecuteNonQuery();
                }

                for (int i = 0; i < currentSale.SaleItems.Count; i++)
                {
                   
                    myCommand.CommandText = "update tec_products set quantity=quantity-" + salesItems[i].Quantity + " where id=" + salesItems[i].ProductId;
                    myCommand.ExecuteNonQuery();
                }


                myTrans.Commit();
                Sales.AddToAllCatagories(currentSale);
           
                Console.WriteLine("All records written to database");
                Bill bill = new Bill(_pos,true,null);
                bill.ShowDialog();
            }
            catch (Exception e)
            {
                try
                {
                    myTrans.Rollback();
                }
                catch (SqlException ex)
                {
                    if (myTrans.Connection != null)
                    {
                        Console.WriteLine("An exception of type " + ex.GetType() +
                        " was encountered while attempting to roll back the transaction.");
                        Console.WriteLine(ex.Message);
                    }
                }

                Console.WriteLine("An exception of type " + e.GetType() +
                " was encountered while inserting the data.");
                Console.WriteLine(e.Message);
                Console.WriteLine("Neither record was written to database.");
            }
            finally
            {
                myConnection.Close();
            }
        }

    }
}

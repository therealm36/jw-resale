﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace basicPos2.models
{
    public partial class DailyReport : INotifyPropertyChanged
    {
        
        string _date;
        float _total;
        float _total_discount;
        float _grand_total;
        float _total_paid;

        string _product_date;
        int _customer_id;
        int _goldsmith_id;
        float _moneygiven;
        float _moneytogive;
        float _goldgiven;
        float _goldtogive;


        //string _image;



        public string ProductDate
        {
            get
            {
                return _product_date;
            }

            set
            {
                _product_date = value;
                RaisePropertyChanged("ProductDate");
            }
        }



        public string CustomerName
        {
            get
            {
                try { 
                    return Customer.AllCustomers.First(item => item.Id == _customer_id).Name;
                }catch(Exception e)
                {
                    return "";
                }
            }

         }

        public string GoldSmithName
        {
            get
            {
                try {
                    //MessageBox.Show(_goldsmith_id+"");
                    return Supplier.AllSupliers.First(item => item.Id == _goldsmith_id).Name;
                }catch(Exception e)
                {
                    return "";
                }
            }

        }

        public string Date
        {
            get
            {
                return _date;
            }

            set
            {
                _date = value;
                RaisePropertyChanged("Date");
            }
        }
        

        public float Total
        {
            get
            {
                return _total;
            }

            set
            {
                _total = value;
                RaisePropertyChanged("Total");

            }
        }
        public float TotalDiscount
        {
            get
            {
                return _total_discount;
            }

            set
            {
                _total_discount = value;
                RaisePropertyChanged("TotalDiscount");
            }
        }
        public float GrandTotal
        {
            get
            {
                return _grand_total;
            }

            set
            {
                _grand_total = value;
                RaisePropertyChanged("GrandTotal");
            }
        }
        public float TotalPaid
        {
            get
            {
                return _total_paid;
            }

            set
            {
                _total_paid = value;
                RaisePropertyChanged("TotalPaid");
            }
        }

        public float MoneyToGive
        {
            get
            {
                return _moneytogive;
            }

            set
            {
                _moneytogive = value;
                RaisePropertyChanged("MoneyToGive");
            }
        }
        public float MoneyGiven
        {
            get
            {
                return _moneygiven;
            }

            set
            {
                _moneygiven = value;
                RaisePropertyChanged("MoneyGiven");
            }
        }
        public float GoldGiven
        {
            get
            {
                return _goldgiven;
            }

            set
            {
                _goldgiven = value;
                RaisePropertyChanged("GoldGiven");
            }
        }
        public float GoldToGive
        {
            get
            {
                return _goldtogive;
            }

            set
            {
                _goldtogive = value;
                RaisePropertyChanged("GoldToGive");
            }
        }

        public int CustomerID
        {
            get
            {
                return _customer_id;
            }

            set
            {
                _customer_id = value;
                RaisePropertyChanged("CustomerID");
            }
        }
        public int GoldSmithID
        {
            get
            {
                return _goldsmith_id;
            }

            set
            {
                _goldsmith_id = value;
                RaisePropertyChanged("GoldSmithID");
            }
        }

        private void RaisePropertyChanged(string propertyName)
        {

            if (null != PropertyChanged)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }


    partial class DailyReport
    {
        static ObservableCollection<DailyReport> _dailyreports= new ObservableCollection<DailyReport>();


        public static ObservableCollection<DailyReport> Dailyreport(string datestart,string dateend)
        {
            
                _dailyreports.Clear();
                MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

                try
                {
                    connection.Open();
                    string sql = "SELECT Date_format(date,'%m/%d/%Y') as datedis,sum(total) as totalday,sum(total_discount) as totdis,sum(grand_total) as grandtotalday,sum(paid) as totalpaid FROM `tec_sales` where date >= '"+ Convert.ToDateTime(datestart).ToString("yyyy/MM/dd") + "' and date<='" + Convert.ToDateTime(dateend).ToString("yyyy/MM/dd") + " 23:59:59' group by datedis";
                    using (var command = new MySqlCommand(sql, connection))
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var product = new DailyReport();
                                product.Date = reader["datedis"].ToString();
                                product.Total = float.Parse(reader["totalday"].ToString());
                               product.TotalDiscount = float.Parse(reader["totdis"].ToString());
                                product.GrandTotal = float.Parse(reader["grandtotalday"].ToString());
                               product.TotalPaid = float.Parse(reader["totalpaid"].ToString());
                                _dailyreports.Insert(0,product);
                            }

                        }
                    }

                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString(), "Error");
                }

                finally
                {
                    if (connection.State == System.Data.ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }

                return _dailyreports;
            }


        static ObservableCollection<DailyReport> _dailyjewelryreports = new ObservableCollection<DailyReport>();


        public static ObservableCollection<DailyReport> DailyJewelryreport(int id)
        {

            _dailyjewelryreports.Clear();
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);
            
            
            try
            {
                connection.Open();
                string sql = "SELECT *,goldgiven as goldtogive,moneygiven as moneytogive from tec_payments where customer_id='j"+id+"'";
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new DailyReport();
                            //MessageBox.Show("ID:::" + reader["id"].ToString() + "   0" + reader["moneytogive"].ToString());
                            product.MoneyToGive= float.Parse(reader["moneytogive"].ToString());

                            product.GoldToGive = float.Parse(reader["goldtogive"].ToString());
                            product.CustomerID = id;
                            _dailyjewelryreports.Insert(0, product);

                        }

                    }
                }
              /*  string sqli = "SELECT sum(goldgiven) as goldgiven,sum(moneygiven)  as moneygiven,sum(price) as price from tec_jw_sales where customer_id=" + id;
                using (var command = new MySqlCommand(sqli, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {

                            product.MoneyToGive += float.Parse("0"+reader["moneygiven"].ToString());
                            product.MoneyGiven = float.Parse("0" + reader["price"].ToString());
                            product.GoldGiven = float.Parse("0" + reader["goldgiven"].ToString());

                        }

                    }
                }*/
                

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error-jw-report");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return _dailyjewelryreports;
        }




        public static float DailyJewelrypricereport(int id)
        {

      
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            var product = new DailyReport();
            try
            {
                connection.Open();
                string sql = "SELECT sum(price) as moneytogive from tec_jw_sales where customer_id=" + id;
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {

                            return float.Parse("0" + reader["moneytogive"].ToString());

                            

                        }

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error-jw-price-report");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return 0;
        }


        public static float Dailygoldpricereport(int id)
        {

          
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            var product = new DailyReport();
            try
            {
                connection.Open();
                string sql = "SELECT sum(price) as moneytogive from tec_gs_purchases where customer_id=" + id;
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {

                            return float.Parse("0" + reader["moneytogive"].ToString());



                        }

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error-jw-price-report");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return 0;
        }







        static ObservableCollection<DailyReport> _dailygoldsmithreports = new ObservableCollection<DailyReport>();


        public static ObservableCollection<DailyReport> DailyGoldSmithreport(int id)
        {
            //MessageBox.Show("ID recied gold report " + id);
            _dailygoldsmithreports.Clear();
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            
            try
            {
                connection.Open();
                string sql = "SELECT goldgiven as goldtogive,moneygiven as moneytogive from tec_payments where customer_id='g" + id+"'";
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new DailyReport();
                            product.MoneyGiven = float.Parse(reader["moneytogive"].ToString());

                            product.GoldGiven = float.Parse(reader["goldtogive"].ToString());
                           // MessageBox.Show(""+product.GoldGiven);
                            product.GoldSmithID = id;
                            _dailygoldsmithreports.Insert(0, product);
                        }

                    }
                }
                /*string sqli = "SELECT sum(goldgiven) as goldgiven,sum(moneygiven) as moneygiven from tec_gs_purchases where customer_id=" + id;
                using (var command = new MySqlCommand(sqli, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {

                            product.MoneyToGive = float.Parse("0" + reader["moneygiven"].ToString());

                            product.GoldToGive = float.Parse("0" + reader["goldgiven"].ToString());

                        }

                    }
                }*/
               // MessageBox.Show("Goldlent" + product.GoldGiven + "Moneylent" + product.MoneyGiven + "Moneytogive" + product.MoneyToGive + "Goldtogive" + product.GoldToGive);
               

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error-gs-report");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return _dailygoldsmithreports;
        }
























        static ObservableCollection<DailyReport> _dailypurchasereports = new ObservableCollection<DailyReport>();


        public static ObservableCollection<DailyReport> Dailypurchasereport(string datestart, string dateend)
        {

            _dailypurchasereports.Clear();
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT Date_format(date,'%m/%d/%Y') as datedis,sum(total) as totalday FROM `tec_purchases` where date >= '" + Convert.ToDateTime(datestart).ToString("yyyy/MM/dd") + "' and date<='" + Convert.ToDateTime(dateend).ToString("yyyy/MM/dd") + " 23:59:59' group by datedis";
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new DailyReport();
                            product.Date = reader["datedis"].ToString();
                            product.Total = float.Parse(reader["totalday"].ToString());
                           _dailypurchasereports.Insert(0,product);
                        }

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return _dailypurchasereports;
        }
        static ObservableCollection<DailyReport> _dailyexpensereports = new ObservableCollection<DailyReport>();


        public static ObservableCollection<DailyReport> DailyExpensereport(string datestart, string dateend)
        {

            _dailyexpensereports.Clear();
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT Date_format(date,'%m/%d/%Y') as datedis,sum(amount) as totalday FROM `tec_expenses` where date >= '" + Convert.ToDateTime(datestart).ToString("yyyy/MM/dd") + "' and date<='" + Convert.ToDateTime(dateend).ToString("yyyy/MM/dd") + " 23:59:59' group by datedis";
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new DailyReport();
                            product.Date = reader["datedis"].ToString();
                            product.Total = float.Parse(reader["totalday"].ToString());
                           _dailyexpensereports.Insert(0,product);
                        }

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return _dailyexpensereports;
        }
        static ObservableCollection<DailyReport> _dailyproductsreports = new ObservableCollection<DailyReport>();


        public static ObservableCollection<DailyReport> DailyProductsreport(string datestart,string dateend)
        {

            _dailyproductsreports.Clear();
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "select Date_format(date,'%m/%d/%Y') as datedis,product_code,product_name,sum(quantity) as sold,sum(quantity*cost) as costx,sum(quantity*unit_price)as income from tec_sale_items left join tec_sales on tec_sales.id=tec_sale_items.sale_id WHERE date>='" + Convert.ToDateTime(datestart).ToString("yyyy/MM/dd") + "' and date<='" + Convert.ToDateTime(dateend).ToString("yyyy/MM/dd") + " 23:59:59'  group by product_id";
                    using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new DailyReport();
                            product.Date = reader["product_name"].ToString()+"("+reader["product_code"].ToString()+")";
                            product.Total = float.Parse(reader["sold"].ToString());
                            product.GrandTotal= float.Parse(reader["costx"].ToString());
                            product.TotalDiscount= float.Parse(reader["income"].ToString());
                            product.ProductDate = reader["datedis"].ToString();
                            product.TotalPaid = (product.TotalDiscount - product.GrandTotal);
                            _dailyproductsreports.Insert(0,product);
                        }

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return _dailyproductsreports;
        }
    }

    }


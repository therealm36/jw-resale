﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Collections.ObjectModel;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Windows;
using basicPos2.models;
namespace basicPos2.models
{
    public partial class Supplier:INotifyPropertyChanged
    {
        int _id;
        string _name;
        string _cf1;
        string _cf2;
        string _phone;
        string _email;


        //string _image;


        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
                RaisePropertyChanged("Id");
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
                RaisePropertyChanged("Name");
            }
        }

        public string CF1
        {
            get
            {
                return _cf1;
            }

            set
            {
                _cf1 = value;
                RaisePropertyChanged("CF1");
            }
        }
        public string CF2
        {
            get
            {
                return _cf2;
            }

            set
            {
                _cf2 = value;
                RaisePropertyChanged("CF2");
            }
        }
        public string Phone
        {
            get
            {
                return _phone;
            }

            set
            {
                _phone = value;
                RaisePropertyChanged("Phone");
            }
        }

        public string Email
        {
            get
            {
                return _email;
            }

            set
            {
                _email = value;
                RaisePropertyChanged("Email");
            }
        }




        /*public string Image
        {
            get
            {
                return _image;
            }

            set
            {
                _image = value;
                RaisePropertyChanged("Image");
            }
        }*/




        private void RaisePropertyChanged(string propertyName)
        {

            if (null != PropertyChanged)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }


    partial class Supplier
    {
       static ObservableCollection<Supplier> _allsuppliers= new ObservableCollection<Supplier>();


        public static ObservableCollection<Supplier> AllSupliers
        {
            get
            {
                if (_allsuppliers.Count == 0)
                {

                    getFromDatabase();
                }

                return _allsuppliers;
            }
        }

        public static void AddToAllSuppliers(Supplier item)
        {


            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "Insert into tec_suppliers(name,cf1,cf2,phone,email) Values ('"  + item.Name + "','" + item.CF1 + "','" + item.CF2 + "','" + item.Phone + "','" + item.Email + "');select last_insert_id() as x;";
                MySqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    item.Id = int.Parse(dr["x"].ToString());
                }
                _allsuppliers.Insert(0,item);

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to add to Supplier", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }

        public static void DeleteFromAllSuppliers(Supplier item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_suppliers WHERE id=" + item.Id;
                cmd.ExecuteNonQuery();
                _allsuppliers.Remove(item);

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to delete", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }
        public static void editProduct(int i, Supplier item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_suppliers WHERE id=" + item.Id;
                cmd.ExecuteNonQuery();
                cmd.CommandText = "Insert into tec_suppliers(id,name,cf1,cf2,phone,email) Values ('" + item.Id + "','" + item.Name + "','" + item.CF1 + "','" + item.CF2 + "','" + item.Phone + "','" + item.Email + "')";
                cmd.ExecuteNonQuery();
                _allsuppliers.RemoveAt(i);
                _allsuppliers.Insert(i, item);

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to edit", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        public static void ClearAllSuppliers()
        {
            _allsuppliers.Clear();
        }

        public override string ToString()
        {
            return Name + " (" + Phone + " )";

        }
        public static void getFromDatabase()
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT * FROM tec_suppliers";
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new Supplier();
                            product.Id = int.Parse(reader["id"].ToString());
                            product.Name = reader["name"].ToString();
                            product.CF1 = reader["cf1"].ToString();
                            product.CF2 = reader["cf2"].ToString();
                            product.Phone= reader["phone"].ToString();
                            product.Email= reader["email"].ToString();
                            



                            _allsuppliers.Insert(0,product);
                        }

                    }
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Load from database", "Error-Suppliers");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }
    }
}

﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using basicPos2;
namespace basicPos2.models
{
    public partial class Purchases : INotifyPropertyChanged
    {
        int _id;
        string _date;
        string _datedis;
        string _time;
        string _note;
        float _total;
        int _supplier_id;
        string _recieved;
        string _reference;


        //string _image;
        public string Recieved
        {
            get
            {
                return _recieved;
            }

            set
            {
                _recieved = value;
                RaisePropertyChanged("Recieved");
            }
        }
        public string Time
        {
            get
            {
                return _time;
            }

            set
            {
                _time = value;
                RaisePropertyChanged("Time");

                RaisePropertyChanged("Datedis");
            }
        }
        public int SupplierID
        {
            get
            {
                return _supplier_id;
            }

            set
            {
                _supplier_id = value;
                RaisePropertyChanged("SupplierID");
            }
        }

        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
                RaisePropertyChanged("Id");
            }
        }

        public string Date
        {
            get
            {
                return _date;
            }

            set
            {
                _date = value;
                RaisePropertyChanged("Date");
                RaisePropertyChanged("Datedis");

            }
        }
        public string Datedis
        {
            get
            {
                return _date + " " + _time;
            }

            set
            {

                RaisePropertyChanged("Datedis");
            }
        }
        

        public string Note
        {
            get
            {
                return _note;
            }

            set
            {
                _note = value;
                RaisePropertyChanged("Note");
            }
        }
        public float Total
        {
            get
            {
                return _total;
            }

            set
            {
                _total = value;
                RaisePropertyChanged("Total");
            }
        }
       
       
        public string Reference
        {
            get
            {
                return _reference;
            }

            set
            {
                _reference = value;
                RaisePropertyChanged("Reference");
            }
        }



        /*public string Image
        {
            get
            {
                return _image;
            }

            set
            {
                _image = value;
                RaisePropertyChanged("Image");
            }
        }*/




        private void RaisePropertyChanged(string propertyName)
        {

            if (null != PropertyChanged)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }


    partial class Purchases
    {
        static ObservableCollection<Purchases> _allpurchases= new ObservableCollection<Purchases>();


        public static ObservableCollection<Purchases> AllPurchases
        {
            get
            {
                if (_allpurchases.Count == 0)
                {

                    getFromDatabase();
                }

                return _allpurchases;
            }
        }

        public static void AddToAllPurchases(Purchases item)
        {


            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "Insert into tec_purchases(id,reference,date,note,total,supplier_id,received) Values ('" + item.Id + "','" + item.Reference + "','" + Convert.ToDateTime(item.Date).ToString("yyyy/MM/dd")+" "+RealUtility.convto24(item.Time) + "','" + item.Note + "','" + item.Total + "','" + item.SupplierID + "','" + item.Recieved+ "')";
                cmd.ExecuteNonQuery();
                _allpurchases.Insert(0,item);

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }

        public static void DeleteFromAllPurchases(Purchases item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_purchases WHERE id=" + item.Id;
                cmd.ExecuteNonQuery();
                _allpurchases.Remove(item);

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to delete", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }
        public static void addCombo(Purchases p, ObservableCollection<PurchaseItem> children)
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);
            
            Purchases.AddToAllPurchases(p);
            for (int i = 0; i < children.Count(); i++)
            {
                try
                {

                    connection.Open();
                    MySqlCommand cmd;
                    cmd = connection.CreateCommand();
                    if (p.Recieved.Equals("Received"))
                    {
                        cmd.CommandText = "update tec_products set quantity=quantity+" + children.ElementAt(i).Quantity + " Where id=" + children.ElementAt(i).ProductID;
                        cmd.ExecuteNonQuery();
                    }

                    cmd.CommandText = "Insert into tec_purchase_items(product_id,purchase_id,subtotal,quantity,cost) Values ('" +  children.ElementAt(i).ProductID + "','" + p.Id + "','" + children.ElementAt(i).Subtotal+ "','" + children.ElementAt(i).Quantity+ "','" + children.ElementAt(i).Cost +"')";
                    cmd.ExecuteNonQuery();



                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to add to combo purchase", "Error");
                    break;
                }

                finally
                {
                    if (connection.State == System.Data.ConnectionState.Open)
                    {
                        Product.ClearAllProducts();
                        Product.getFromDatabase();
                        connection.Close();
                    }
                }
            }


        }
        public static void editCombo(Purchases initial, Purchases p, ObservableCollection<PurchaseItem> children, ObservableCollection<PurchaseItem> initialchildren)
        {
            
            
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);


            int l = _allpurchases.IndexOf(initial);
            DeleteFromAllPurchases(initial);
            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "Insert into tec_purchases(id,reference,date,note,total,supplier_id,received) Values ('" + p.Id + "','" + p.Reference + "','" + Convert.ToDateTime(p.Date).ToString("yyyy/MM/dd") +" "+ RealUtility.convto24(p.Time) + "','" + p.Note + "','" + p.Total + "','" + p.SupplierID + "','" + p.Recieved + "')";
                cmd.ExecuteNonQuery();
                
                
                _allpurchases.Insert(l, p);

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            for(int i = 0; i < initialchildren.Count; i++)
            {
                try
                {
                    connection.Open();
                    MySqlCommand cmd;
                    cmd = connection.CreateCommand();
                    if (!children.Any(item => item.Id == initialchildren.ElementAt(i).Id))
                    {

                        cmd.CommandText = "update tec_products set quantity=quantity-" + initialchildren.ElementAt(i).Quantity + " Where id=" + initialchildren.ElementAt(i).ProductID;
                        cmd.ExecuteNonQuery();
                    }
                  
                }
                catch(Exception e)
                {

                }
                finally
                {
                    if (connection.State == System.Data.ConnectionState.Open)
                    {

                        connection.Close();
                        Product.ClearAllProducts();
                        Product.getFromDatabase();
                    }
                }
            }
            for (int i = 0; i < children.Count(); i++)
            {
                try
                {

                    connection.Open();
                    MySqlCommand cmd;
                    cmd = connection.CreateCommand();
                    
                    if (i < initialchildren.Count())
                    {
                        
                        if (children.Any(item => item.Id == initialchildren.ElementAt(i).Id))
                        {
                           
                            cmd.CommandText = "update tec_products set quantity=quantity-" + initialchildren.ElementAt(i).Quantity + " Where id=" + initialchildren.ElementAt(i).ProductID;
                            cmd.ExecuteNonQuery();
                        }else
                        {
                            
                        }
                    }else
                    {
                        
                    }
                    if (initialchildren.Any(item => item.Id == children.ElementAt(i).Id) && p.Recieved.Equals("Received"))
                    {
                        cmd.CommandText = "update tec_products set quantity=quantity-"+initialchildren.ElementAt(i).Quantity+"+" + children.ElementAt(i).Quantity + " Where id=" + children.ElementAt(i).ProductID;
                        cmd.ExecuteNonQuery();
                    }else
                    {
                        cmd.CommandText = "update tec_products set quantity=quantity+" + children.ElementAt(i).Quantity + " Where id=" + children.ElementAt(i).ProductID;
                        cmd.ExecuteNonQuery();
                    }
                    cmd.CommandText = "Insert into tec_purchase_items(product_id,purchase_id,subtotal,quantity,cost) Values ('" + children.ElementAt(i).ProductID + "','" + p.Id + "','" + children.ElementAt(i).Subtotal + "','" + children.ElementAt(i).Quantity + "','" + children.ElementAt(i).Cost + "')";
                    cmd.ExecuteNonQuery();
                    




                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString(), "Error");
                    break;
                }

                finally
                {
                    if (connection.State == System.Data.ConnectionState.Open)
                    {

                        connection.Close();
                        Product.ClearAllProducts();
                        Product.getFromDatabase();
                    }
                }
            }

        }
        public static void getComboItems(int i, ObservableCollection<PurchaseItem> toreturn)
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT * FROM tec_purchase_items WHERE purchase_id=" + i;
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new PurchaseItem();
                            product.Id = int.Parse(reader["id"].ToString());
                            product.ProductID = int.Parse(reader["product_id"].ToString());
                           product.PurchaseID = int.Parse(reader["purchase_id"].ToString());
                            product.Cost = float.Parse(reader["cost"].ToString());
                           product.Quantity = int.Parse(reader["quantity"].ToString());
                           product.Subtotal = float.Parse(reader["subtotal"].ToString());
                           product.Name = Product.AllProducts.First(item => item.Id == product.ProductID).ToString();


                            if (product != null)
                            {
                                toreturn.Insert(0,product);
                            }
                        }

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();

                }

            }

        }

        public static void getFromDatabase()
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT *,Date_format(date,'%m/%d/%Y') as x,time_format(date , '%r') as y FROM tec_purchases ";
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new Purchases();
                            product.Id = int.Parse(reader["id"].ToString());
                            product.Reference = reader["reference"].ToString();
                            product.Date = reader["x"].ToString();
                            product.Time = reader["y"].ToString();

                            product.Note = reader["note"].ToString();
                            product.Total = float.Parse(reader["total"].ToString());
                            if (!reader["supplier_id"].ToString().Equals(""))
                            {
                                product.SupplierID = int.Parse(reader["supplier_id"].ToString());
                            }
                            product.Recieved = reader["received"].ToString();
                            _allpurchases.Insert(0,product);
                        }

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }
    }
    }

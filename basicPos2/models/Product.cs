﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Collections.ObjectModel;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Windows;
using basicPos2.models;
using System.Windows.Media.Imaging;
using System.IO;
using System.Windows.Media;

namespace basicPos2.models
{
    public partial class Product:INotifyPropertyChanged
    {
        int _id;
        string _code;
        string _name;
        int _catagoryId;
        string _catagory;
        float _price=0;
        byte[] _image;
        string _vis;
        float _cost;
        float _labour_cost;
        float _weight= 0;


        float _quantity;
        ImageSource _imagei;
        string _type="1";
        string _details;
        string _metaltype;
        string _design;
        float _length;
        float _width;
        float _silversize;
        float _plasticsize;
        string _metalcolor;

        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
                RaisePropertyChanged("Id");
            }
        }
        
        public string Details
        {
            get
            {
                return _details;
            }

            set
            {
                _details = value;
                RaisePropertyChanged("Details");
            }
        }
        
        public ImageSource Imagei
        {
            get
            {
                return _imagei;
            }

            set
            {
                _imagei = value;
                RaisePropertyChanged("Imagei");
            }
        }
        public int CatagoryId
        {
            get
            {
                return _catagoryId;
            }

            set
            {
                _catagoryId = value;
                RaisePropertyChanged("Catagory");
                RaisePropertyChanged("CatagoryId");
            }
        }
        public byte[] Image
        {
            get
            {
                return _image;
            }

            set
            {
                _image = value;
                RaisePropertyChanged("Image");
            }
        }
        public string Vis
        {
            get
            {
                return _vis;
            }
            set
            {
                _vis = value;
                RaisePropertyChanged("Vis");
            }
        }

        public string MetalColor
        {
            get
            {
                return _metalcolor;
            }

            set
            {
                _metalcolor = value;
                RaisePropertyChanged("MetalColor");
            }
        }

        public string Code
        {
            get
            {
                return _code;
            }

            set
            {
                _code = value.ToUpper();
                RaisePropertyChanged("Code");
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value.ToUpper();
                RaisePropertyChanged("Name");
            }
        }

       
        public float Price
        {
            get
            {
                if (_price ==0)
                {
                    
                    int sudometaltype = 0;
                    if (MetalType.Equals("24")) { sudometaltype = 22; }
                    if (MetalType.Equals("22")) { sudometaltype = 21; }
                    if (MetalType.Equals("18")) { sudometaltype = 18; }
                    //MessageBox.Show("sudo "+sudometaltype+", real "+MetalType);
                    return LabourCost + OtherCost + Weight * RealUtility.getRate(sudometaltype+ "K");

                }else
                {
                    return _price;
                }
            }
            set
            {
                _price = value;
            }
            
        }

        

        public float Cost
        {
            get
            {
                return _cost;
            }

            set
            {
                _cost = value;
                RaisePropertyChanged("OtherCost");
                RaisePropertyChanged("Cost");
            }
        }

        public float Quantity
        {
            get
            {
                return _quantity;
            }

            set
            {
                _quantity = value;
                RaisePropertyChanged("Quantity");
            }
        }

       

        public string Design
        {
            get
            {
                return _design;
            }

            set
            {
                _design = value;
                RaisePropertyChanged("Design");
            }
        }
        public string MetalType
        {
            get
            {
                return _metaltype;
            }

            set
            {
                _metaltype = value;
                RaisePropertyChanged("MetalType");
            }
        }
        public float Weight
        {
            get
            {
                return _weight;
            }

            set
            {
                _weight = value;
                RaisePropertyChanged("Weight");
            }
        }

        public float LabourCost
        {
            get
            {
                return _labour_cost;
            }

            set
            {
                _labour_cost = value;
                RaisePropertyChanged("LabourCost");
                RaisePropertyChanged("OtherCost");
            }
        }
        public float OtherCost
        {
            get
            {
                return Cost - LabourCost;
            }


        }
        public float SilverSize
        {
            get
            {
                return _silversize;
            }

            set
            {
                _silversize = value;
                RaisePropertyChanged("SilverSize");
            }
        }
        public float PlasticSize
        {
            get
            {
                return _plasticsize;
            }

            set
            {
                _plasticsize = value;
                RaisePropertyChanged("PlasticSize");
            }
        }
        public float Length
        {
            get
            {
                return _length;
            }

            set
            {
                _length = value;
                RaisePropertyChanged("Length");
            }
        }
        public float Width
        {
            get
            {
                return _width;
            }

            set
            {
                _width = value;
                RaisePropertyChanged("Width");
            }
        }

        public string Catagory
        {
            get
            {
                return (basicPos2.models.Catagory.AllCatagories.First(item => item.Id == CatagoryId)).Name;
            }

           set
            {
                _catagory = value;
            }
        }

        public string Type
        {
            get
            {
                return _type;
            }

            set
            {
                _type = value;
            }
        }

        private void RaisePropertyChanged(string propertyName)
        {

            if (null != PropertyChanged)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }

    public enum Type
        {
           
        }

    partial class Product
    {
        static ObservableCollection<Product> _allProdutcs = new ObservableCollection<Product>();


        public static ObservableCollection<Product> AllProducts
        {
            get {
                if (_allProdutcs.Count==0)
                {
          
                    getFromDatabase();
                }

                return _allProdutcs;
            }
        }

        public static void AddToAllProduts(Product item)
        {
            

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);
            
            try
            {
               
                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "Insert into tec_products(code,name,price,cost,quantity,type,metalcolor,length,width,silversize,plasticsize,details,category_id,image,design,labour_cost,metal_type,weight) Values ('" + item.Code + "','" + item.Name + "','" + item.Price + "','" + item.Cost+ "','" + item.Quantity+ "','" + item.Type+ "','" + item.MetalColor + "','" + item.Length + "','" + item.Width + "','" + item.SilverSize + "','" + item.PlasticSize + "','" + item.Details + "','" + item.CatagoryId + "',@image,'" + item.Design + "','" + item.LabourCost + "','" + item.MetalType + "','" + item.Weight + "');select last_insert_id() as xid;";
                cmd.Parameters.Add("@image", MySqlDbType.LongBlob).Value = item.Image;
                MySqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    item.Id = int.Parse(dr["xid"].ToString());
                }
               
                _allProdutcs.Insert(0,item);

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }

        public static void DeleteFromAllProducts(Product item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                
                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_products WHERE id="+item.Id;
                cmd.ExecuteNonQuery();
                _allProdutcs.Remove(item);

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to delete", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            
        }
        public static void editProduct(int i,Product item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_products WHERE id=" + item.Id;
                cmd.ExecuteNonQuery();
                cmd.CommandText = "Insert into tec_products(id,code,name,price,cost,quantity,type,metalcolor,length,width,silversize,plasticsize,details,category_id,image,design,labour_cost,metal_type,weight) Values ('" + item.Id + "','" + item.Code + "','" + item.Name + "','" + item.Price + "','" + item.Cost + "','" + item.Quantity + "','" + item.Type + "','" + item.MetalColor + "','" + item.Length + "','" + item.Width + "','" + item.SilverSize + "','" + item.PlasticSize + "','" + item.Details + "','" + item.CatagoryId + "',@image,'" + item.Design + "','" + item.LabourCost + "','" + item.MetalType + "','" + item.Weight + "');select last_insert_id() as xid;";
                cmd.Parameters.Add("@image", MySqlDbType.LongBlob).Value = item.Image;

                MySqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    item.Id = int.Parse(dr["xid"].ToString());
                }

                
                _allProdutcs.RemoveAt(i);
                _allProdutcs.Insert(i, item);

            }
            catch (Exception e)
            {
                MessageBox.Show("Failed to edit"+e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }
        /*public static Byte[] ConvertToByteFromBitmapImage(BitmapImage bitmapImage)
        {
            byte[] data;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();

            encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }

            return data;
        }*/

        public static void ClearAllProducts()
        {
            _allProdutcs.Clear();
        }

        public override string ToString()
        {
            return Name + " (" + Code + " )"; 
           
        }

        public static void addCombo(Product p,ObservableCollection<Product> children)
        {
            
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            for (int i = 0; i < children.Count(); i++)
            {
                p.Cost += children.ElementAt(i).Cost * children.ElementAt(i).Quantity;
                

            }
            Product.AddToAllProduts(p);
            for (int i = 0; i < children.Count(); i++)
            {
                try
                {

                    connection.Open();
                    MySqlCommand cmd;
                    cmd = connection.CreateCommand();
                    cmd.CommandText = "Insert into tec_combo_items(product_id,item_code,quantity) Values ('" +p.Id+"','"+children.ElementAt(i).Code+"','"+ children.ElementAt(i).Quantity+ "')";

                    cmd.ExecuteNonQuery();
                    


                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to add to Combo Product", "Error");
                    break;
                }

                finally
                {
                    if (connection.State == System.Data.ConnectionState.Open)
                    {
                        
                        connection.Close();
                    }
                }
            }
            

        }
        public static void editCombo(Product initial,Product p,ObservableCollection<Product> children)
        {
            




            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);
            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_products WHERE id=" + initial.Id;
                cmd.ExecuteNonQuery();
                cmd.CommandText = "Insert into tec_products(code,name,price,cost,quantity,type,metalcolor,length,width,silversize,plasticsize,details,category_id,image,design,labour_cost,metal_type,weight) Values ('" + p.Id + "','" + p.Code + "','" + p.Name + "','" + p.Price + "','" + p.Cost + "','" + p.Quantity + "','" + p.Type + "','" + p.MetalColor + "','" + p.Length + "','" + p.Width + "','" + p.SilverSize + "','" + p.PlasticSize + "','" + p.Details + "','" + p.CatagoryId + "',@image,'" + p.Design + "','" + p.LabourCost + "','" + p.MetalType + "','" + p.Weight + "');select last_insert_id() as xid;";
                cmd.Parameters.Add("@image", MySqlDbType.LongBlob).Value = p.Image;

                MySqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    p.Id = int.Parse(dr["xid"].ToString());
                }


                int l = _allProdutcs.IndexOf(initial);
                _allProdutcs.RemoveAt(l);
                _allProdutcs.Insert(l, p);


            }
            catch (Exception)
            {
                MessageBox.Show("Failed to add to Product", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            for (int i = 0; i < children.Count(); i++)
            {
                p.Cost += children.ElementAt(i).Cost * children.ElementAt(i).Quantity;


            }
            
            for (int i = 0; i < children.Count(); i++)
            {
                try
                {

                    connection.Open();
                    MySqlCommand cmd;
                    cmd = connection.CreateCommand();
                    cmd.CommandText = "Insert into tec_combo_items(product_id,item_code,quantity) Values ('" + p.Id + "','" + children.ElementAt(i).Code + "','" + children.ElementAt(i).Quantity + "')";
                    cmd.ExecuteNonQuery();



                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to add to Combo Product", "Error");
                    break;
                }

                finally
                {
                    if (connection.State == System.Data.ConnectionState.Open)
                    {

                        connection.Close();
                    }
                }
            }

        }
        public static void getComboProducts(int i, ObservableCollection<Product> toreturn)
        {
            
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT tec_products.id,tec_products.code,tec_products.name,tec_products.type,tec_products.price,tec_products.cost,tec_products.details,tec_products.length,tec_products.width,tec_products.plasticsize,tec_products.silversize,tec_products.metalcolor,tec_combo_items.quantity FROM tec_products INNER JOIN tec_combo_items on tec_products.code=tec_combo_items.item_code WHERE tec_combo_items.product_id=" + i;
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new Product();
                            product.Id = int.Parse(reader["id"].ToString());
                            product.Code = reader["code"].ToString();
                           product.Name = reader["name"].ToString()+"("+ reader["code"].ToString()+")" ;
                            product.Type = reader["type"].ToString();
                            product.MetalColor= reader["metalcolor"].ToString();

                            product.Price = float.Parse(reader["price"].ToString());
                            product.Cost = float.Parse(reader["cost"].ToString());
                           product.Details = reader["details"].ToString();
                           product.Length = float.Parse(reader["length"].ToString());
                            product.Width = float.Parse(reader["width"].ToString());
                            product.SilverSize = float.Parse(reader["silversize"].ToString());

                            product.PlasticSize = float.Parse(reader["plasticsize"].ToString());
                            product.Quantity = float.Parse(reader["quantity"].ToString());




                            toreturn.Insert(0,product);
                        }

                    }
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Load Combo from database", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                    
                }
                
            }
            
        }

        public static void getFromDatabase()
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);
            
            try
            {
                connection.Open();
                string sql = "SELECT *,tec_categories.name as x FROM tec_products INNER JOIN tec_categories on tec_products.category_id=tec_categories.id";
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new Product();
                            product.Id = int.Parse(reader["id"].ToString());
                            product.Code = reader["code"].ToString();
                            product.Name = reader["name"].ToString();

                            if (!reader["image"].ToString().Equals(""))
                            {
                               product.Image = (byte[])reader["image"];
                                product.Imagei = (ImageSource)new ImageSourceConverter().ConvertFrom(product.Image);
                                product.Vis = "Hidden";
                            }
                            product.Type = reader["type"].ToString();
                            product.Weight = float.Parse(reader["weight"].ToString());
                            product.Cost = float.Parse(reader["cost"].ToString());
                            product.Details = reader["details"].ToString();
                            product.CatagoryId = int.Parse(reader["category_id"].ToString());
                            // product.Catagory = reader["x"].ToString();
                            product.Length = float.Parse(reader["length"].ToString());
                            product.Width = float.Parse(reader["width"].ToString());
                            product.SilverSize = float.Parse(reader["silversize"].ToString());
                            product.PlasticSize = float.Parse(reader["plasticsize"].ToString());

                            product.Quantity = float.Parse(reader["quantity"].ToString());
                            product.Design= reader["design"].ToString();
                            product.MetalColor = reader["metalcolor"].ToString();
                            product.MetalType= reader["metal_type"].ToString();
                            product.LabourCost = float.Parse(reader["labour_cost"].ToString());




                            _allProdutcs.Insert(0,product);
                        }
                        
                    }
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Load from database", "Error-Products"); 
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

    }
}

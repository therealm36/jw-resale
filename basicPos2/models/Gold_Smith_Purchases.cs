﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Collections.ObjectModel;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Windows;
using basicPos2.models;
using System.Windows.Media.Imaging;
using System.IO;
using System.Windows.Media;


namespace basicPos2.models
{
    public partial class Gold_Smith_Purchases : INotifyPropertyChanged
    {
        int _id;
        int _customerId;
        string _date;
        string _time;
        string _datedis;
        float _goldgiven = 0;
        float _totalquant = 0;//currently it is 0 all over the place and is not used currently
        float _moneygiven = 0;
        float _price = 0;

        string _details;

        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
                RaisePropertyChanged("Id");
            }
        }
        public string Time
        {
            get
            {
                return _time;
            }

            set
            {
                _time = value;
                RaisePropertyChanged("Time");

                RaisePropertyChanged("Datedis");
            }
        }
        public string Details
        {
            get
            {
                return _details;
            }

            set
            {
                _details = value;
                RaisePropertyChanged("Details");
            }
        }
        public int CustomerID
        {
            get
            {
                return _customerId;
            }

            set
            {
                _customerId = value;
                RaisePropertyChanged("CustomerID");
            }
        }

        public string Date
        {
            get
            {
                return _date;
            }

            set
            {
                _date = value;
                RaisePropertyChanged("Date");
            }
        }
        public string CustomerName
        {
            get
            {
                return Supplier.AllSupliers.First(item => item.Id == CustomerID).Name;
            }

        }
        public string Datedis
        {
            get
            {
                return _date + " " + _time;
            }

            set
            {

                RaisePropertyChanged("Datedis");
            }
        }
        public float GoldGiven
        {
            get
            {
                return _goldgiven;
            }

            set
            {
                _goldgiven = value;
                RaisePropertyChanged("GoldGiven");
            }
        }
        public float Price
        {
            get
            {
                return _price;
            }

            set
            {
                _price = value;
                RaisePropertyChanged("Price");
            }
        }
        public float TotalQuantity
        {
            get
            {
                return _totalquant;
            }

            set
            {
                _totalquant = value;
                RaisePropertyChanged("TotalQuantity");
            }
        }
        public float MoneyGiven
        {
            get
            {
                return _moneygiven;
            }

            set
            {
                _moneygiven = value;
                RaisePropertyChanged("MoneyGiven");
            }
        }

       
        private void RaisePropertyChanged(string propertyName)
        {

            if (null != PropertyChanged)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }

    partial class Gold_Smith_Purchases
    {
        static ObservableCollection<Gold_Smith_Purchases> _allpurchases = new ObservableCollection<Gold_Smith_Purchases>();


        public static ObservableCollection<Gold_Smith_Purchases> AllGoldSmithPurchases
        {
            get
            {
                if (_allpurchases.Count == 0)
                {

                    getFromDatabase();
                }

                return _allpurchases;
            }
        }

        public static void AddToAllGSPurchases(Gold_Smith_Purchases item)
        {


            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd = connection.CreateCommand();
                cmd.CommandText = "Insert into tec_gs_purchases(date,customer_id,goldgiven,totalquantity,moneygiven,price,details) Values ('" + Convert.ToDateTime(item.Date).ToString("yyyy/MM/dd") + " " + RealUtility.convto24(item.Time) + "'," + item.CustomerID + "," + 0 + "," + item.TotalQuantity + "," + 0  + ","  + item.Price + ",'" + item.Details + "');select last_insert_id() as x;";
                MySqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    item.Id = int.Parse(dr["x"].ToString());
                }
                _allpurchases.Insert(0, item);
                Payment.AddPayment(new Payment { SaleID = "gssp" + item.Id, CustomerId = "g" + item.CustomerID, Moneygiven = item.MoneyGiven, Goldgiven = item.GoldGiven, Reference = "Initial Pay", Date = item.Date, Time = item.Time });

                

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }

        public static void DeleteFromAllJWSales(Gold_Smith_Purchases item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
               // MessageBox.Show("ID::" + item.Id, "id");
                connection.Open();
                string sql = "SELECT * FROM tec_gs_purchase_items where gs_purchase_id= " + item.Id + ";delete  from tec_gs_purchases where id = " + item.Id;
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        //MessageBox.Show("a", "b");
                        while (reader.Read())
                        {
                          //  MessageBox.Show("aa", "b");
                          //  MessageBox.Show("Quantity::" + float.Parse(reader["quantity"].ToString()) + "  ID::" + int.Parse(reader["product_id"].ToString()), "info");
                            alterfunc(int.Parse(reader["product_id"].ToString()), float.Parse(reader["quantity"].ToString()));
                        }

                       // MessageBox.Show("aaa", "b");
                    }
                }


                Product.ClearAllProducts();
                Product.getFromDatabase();
                // delfunc(item.Id);
                _allpurchases.Remove(item);

            }
            catch (Exception e)
            {
                MessageBox.Show("Failed to delete" + e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {

                    Payment.sDeleteFromAllGivenPayment("gssp" + item.Id);
                    connection.Close();

                }
            }

        }
        public static void editGSPurchases(int i, Gold_Smith_Purchases item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_gs_purchases WHERE id=" + item.Id;
                cmd.ExecuteNonQuery();
                cmd.CommandText = "Insert into tec_gs_purchases(date,customer_id,goldgiven,totalquantity,moneygiven,details) Values ('" + Convert.ToDateTime(item.Date).ToString("yyyy/MM/dd") + "'," + item.CustomerID + "," + item.GoldGiven + "," + item.TotalQuantity + "," + item.MoneyGiven  + ",'" + item.Details + "');select last_insert_id() as x;";
                cmd.ExecuteNonQuery();


                _allpurchases.RemoveAt(i);
                _allpurchases.Insert(i, item);

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }
        /*public static Byte[] ConvertToByteFromBitmapImage(BitmapImage bitmapImage)
        {
            byte[] data;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();

            encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }

            return data;
        }*/

        public static void ClearAllProducts()
        {
            _allpurchases.Clear();
        }


        public static void addGSPurchase(Gold_Smith_Purchases p, ObservableCollection<Product> children)
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);
            AddToAllGSPurchases(p);
            for (int i = 0; i < children.Count(); i++)
            {
                try
                {

                    connection.Open();
                    MySqlCommand cmd;
                    cmd = connection.CreateCommand();
                    cmd.CommandText = "Insert into tec_gs_purchase_items(gs_purchase_id,product_id,quantity) Values ('" + p.Id + "','" + children.ElementAt(i).Id + "','" + children.ElementAt(i).Quantity + "')";
                    //MessageBox.Show(children.ElementAt(i).Name.Substring(children.ElementAt(i).Name.Length - 14),"Name last part");
                    cmd.ExecuteNonQuery();
                    if ((children.ElementAt(i).Name.Length - 14 - children.ElementAt(i).Code.Length - 2 - 2) >= 0)
                    {
                        if (children.ElementAt(i).Name.Substring(children.ElementAt(i).Name.Length - 14 - children.ElementAt(i).Code.Length - 2 - 2, 14).Equals("(ONLY ORDERED)"))
                        {
                            //MessageBox.Show("Only ord \n" + children.ElementAt(i).Name.Substring(children.ElementAt(i).Name.Length - 14 - children.ElementAt(i).Code.Length - 2 - 2, 14));
                            cmd.CommandText = "update tec_products set quantity= " + children.ElementAt(i).Quantity + " , Name='" + children.ElementAt(i).Name.Substring(0, children.ElementAt(i).Name.Length - 14 - children.ElementAt(i).Code.Length - 2 - 2) + "(COMPLETED)'" + "where id= " + children.ElementAt(i).Id;
                            cmd.ExecuteNonQuery();
                        }
                        else
                        {
                          //  MessageBox.Show("notonlyord \n" + children.ElementAt(i).Name.Substring(children.ElementAt(i).Name.Length - 14 - children.ElementAt(i).Code.Length - 2 - 2, 14));
                            cmd.CommandText = "update tec_products set quantity=quantity + " + children.ElementAt(i).Quantity + " where id= " + children.ElementAt(i).Id;
                            cmd.ExecuteNonQuery();
                        }
                    }else
                    {
                        cmd.CommandText = "update tec_products set quantity=quantity + " + children.ElementAt(i).Quantity + " where id= " + children.ElementAt(i).Id;
                        cmd.ExecuteNonQuery();
                    }



                }
                catch (Exception e)
                {
                    MessageBox.Show("Failed to add purchases"+e.ToString(), "Error gs_purchase");
                    break;
                }

                finally
                {
                    if (connection.State == System.Data.ConnectionState.Open)
                    {
                        Product.ClearAllProducts();
                        Product.getFromDatabase();
                        connection.Close();
                    }
                }
            }


        }
        public static void editCombo(Gold_Smith_Purchases initial, Gold_Smith_Purchases p, ObservableCollection<Product> children)
        {





            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);
            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_gs_purchases WHERE id=" + initial.Id;
                cmd.ExecuteNonQuery();
                cmd.CommandText = "Insert into tec_gs_purcahses(date,customer_id,goldgiven,totalquantity,moneygiven,details) Values ('" + Convert.ToDateTime(p.Date).ToString("yyyy/MM/dd") + "'," + p.CustomerID + "," + p.GoldGiven + "," + p.TotalQuantity + "," + p.MoneyGiven  + ",'" + p.Details + "');select last_insert_id() as x;";
                cmd.ExecuteNonQuery();


                int l = _allpurchases.IndexOf(initial);
                _allpurchases.RemoveAt(l);
                _allpurchases.Insert(l, p);


            }
            catch (Exception)
            {
                MessageBox.Show("Failed to add to Product", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            for (int i = 0; i < children.Count(); i++)
            {
                try
                {

                    connection.Open();
                    MySqlCommand cmd;
                    cmd = connection.CreateCommand();
                    cmd.CommandText = "Insert into tec_gs_purchase_items(jw_sale_id,product_id,quantity) Values ('" + p.Id + "','" + children.ElementAt(i).Id + "','" + children.ElementAt(i).Quantity + "')";
                    cmd.ExecuteNonQuery();



                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to add to Combo Product", "Error");
                    break;
                }

                finally
                {
                    if (connection.State == System.Data.ConnectionState.Open)
                    {

                        connection.Close();
                    }
                }
            }

        }


        public static void alterfunc(int id, float quantity)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "update tec_products set quantity=quantity - " + quantity + " where id= " + id;
                cmd.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        public static void delfunc(int id)
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT * FROM tec_gs_purchases_item where gs_purchases_id= " + id;
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            alterfunc(int.Parse(reader["product_id"].ToString()), float.Parse(reader["quantity"].ToString()));
                        }

                    }
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Load from database", "Error-Products");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }



        public static void getComboProducts(int i, ObservableCollection<Product> toreturn)
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT tec_products.id,tec_products.code,tec_products.name,tec_products.type,tec_products.price,tec_products.cost,tec_products.details,tec_products.length,tec_products.width,tec_products.plasticsize,tec_products.silversize,tec_products.metalcolor,tec_gs_purchase_items.quantity FROM tec_products INNER JOIN tec_gs_purchase_items on tec_products.id=tec_gs_purchase_items.product_id WHERE tec_gs_purchase_items.gs_purchase_id=" + i;
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new Product();
                            product.Id = int.Parse(reader["id"].ToString());
                            product.Code = reader["code"].ToString();
                            product.Name = reader["name"].ToString() + "(" + reader["code"].ToString() + ")";
                            product.Type = reader["type"].ToString();
                            product.MetalColor = reader["metalcolor"].ToString();

                            product.Price = float.Parse(reader["price"].ToString());
                            product.Cost = float.Parse(reader["cost"].ToString());
                            product.Details = reader["details"].ToString();
                            product.Length = float.Parse(reader["length"].ToString());
                            product.Width = float.Parse(reader["width"].ToString());
                            product.SilverSize = float.Parse(reader["silversize"].ToString());

                            product.PlasticSize = float.Parse(reader["plasticsize"].ToString());
                            product.Quantity = float.Parse(reader["quantity"].ToString());



                            toreturn.Insert(0, product);
                        }

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Failed to Load Combo from database"+e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();

                }

            }

        }

        public static void getFromDatabase()
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT *,Date_format(date,'%m/%d/%Y') as x,time_format(date , '%r') as y FROM tec_gs_purchases";
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var GSPurchase = new Gold_Smith_Purchases();
                            GSPurchase.Id = int.Parse(reader["id"].ToString());
                            GSPurchase.Date = reader["x"].ToString();
                            GSPurchase.Time = reader["y"].ToString();
                            
                            GSPurchase.CustomerID = int.Parse(reader["customer_id"].ToString());


                            GSPurchase.GoldGiven = float.Parse(reader["goldgiven"].ToString());
                            GSPurchase.TotalQuantity = float.Parse(reader["totalquantity"].ToString());
                            
                            GSPurchase.MoneyGiven = float.Parse(reader["moneygiven"].ToString());
                            GSPurchase.Details = reader["details"].ToString();


                            _allpurchases.Insert(0, GSPurchase);
                        }

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Failed to Load from database \n "+e.ToString(), "Error-gssp");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

    }
}


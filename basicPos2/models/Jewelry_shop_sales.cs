﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Collections.ObjectModel;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Windows;
using basicPos2.models;
using System.Windows.Media.Imaging;
using System.IO;
using System.Windows.Media;


namespace basicPos2.models
{
    public partial class Jewelry_shop_sales : INotifyPropertyChanged
    {
        int _id;
        int _customerId;
        string _date;
        string _time;
        string _datedis;
        float _goldgiven = 0;
        float _pricedummy = -1;


        float _totalquantity = 0;//currently it is 0 all over the place and is not used currently
        float _moneygiven = 0;
        
        string _details;
        public string Date
        {
            get
            {
                return _date;
            }

            set
            {
                _date = value;
                RaisePropertyChanged("Date");
            }
        }

        public float Price
        {
            get
            {
                
                //ObservableCollection<Product> fake = new ObservableCollection<Product>();
                //MessageBox.Show("" + Id, "ID");
                return _pricedummy;
             
            }
            set
            {
                _pricedummy = value;
                RaisePropertyChanged("Price");
            }
            
        }
        public string Time
        {
            get
            {
                return _time;
            }

            set
            {
                _time = value;
                RaisePropertyChanged("Time");

                RaisePropertyChanged("Datedis");
            }
        }


        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
                RaisePropertyChanged("Id");
            }
        }

        public string Details
        {
            get
            {
                return _details;
            }

            set
            {
                _details = value;
                RaisePropertyChanged("Details");
            }
        }
        public string CustomerName
        {
            get
            {
                return Customer.AllCustomers.First(item=>item.Id==CustomerID).Name;
            }

        }
        public int CustomerID
        {
            get
            {
                return _customerId;
            }

            set
            {
                _customerId = value;
                RaisePropertyChanged("CustomerID");
            }
        }
        public string Datedis
        {
            get
            {
                return _date + " " + _time;
            }

            set
            {

                RaisePropertyChanged("Datedis");
            }
        }
        public float GoldGiven
        {
            get
            {
                return _goldgiven;
            }

            set
            {
                _goldgiven = value;
                RaisePropertyChanged("GoldGiven");
            }
        }

        public float TotalQuantity
        {
            get
            {
                return _totalquantity;
            }

            set
            {
                _totalquantity = value;
                RaisePropertyChanged("TotalQuantity");
            }
        }
        public float MoneyGiven
        {
            get
            {
                return _moneygiven;
            }

            set
            {
                _moneygiven = value;
                RaisePropertyChanged("MoneyGiven");
            }
        }

        private void RaisePropertyChanged(string propertyName)
        {

            if (null != PropertyChanged)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }

    partial class Jewelry_shop_sales
    {
        static ObservableCollection<Jewelry_shop_sales> _allsales = new ObservableCollection<Jewelry_shop_sales>();


        public static ObservableCollection<Jewelry_shop_sales> AllJewelrySales
        {
            get
            {
                if (_allsales.Count == 0)
                {

                    getFromDatabase();
                }

                return _allsales;
            }
        }

        public static void AddToAllJWSales(Jewelry_shop_sales item,float p)
        {


            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd = connection.CreateCommand();
                cmd.CommandText = "Insert into tec_jw_sales(price,date,customer_id,goldgiven,totalquantity,moneygiven,details) Values ("+p+",'" + Convert.ToDateTime(item.Date).ToString("yyyy/MM/dd") + " " + RealUtility.convto24(item.Time) + "'," + item.CustomerID + "," + 0 + "," + item.TotalQuantity + "," + 0 + ",'" + item.Details + "');select last_insert_id() as x;";
                MySqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    item.Id = int.Parse(dr["x"].ToString());
                }
                _allsales.Insert(0, item);
                Payment.AddPayment(new Payment { SaleID = "jwss" + item.Id, CustomerId = "j" + item.CustomerID, Moneygiven = item.MoneyGiven, Goldgiven = item.GoldGiven, Reference = "Initial Pay", Date = item.Date, Time = item.Time });

                

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }

        public static void DeleteFromAllJWSales(Jewelry_shop_sales item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                //MessageBox.Show("ID::" + item.Id, "id");
                connection.Open();
                string sql = "SELECT * FROM tec_jw_sales_items where jw_sale_id= " + item.Id+";delete  from tec_jw_sales where id = " + item.Id;
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        //MessageBox.Show("a", "b");
                        while (reader.Read())
                        {
                           // MessageBox.Show("aa", "b");
                           // MessageBox.Show("Quantity::" + float.Parse(reader["quantity"].ToString()) + "  ID::" + int.Parse(reader["product_id"].ToString()), "info");
                            alterfunc(int.Parse(reader["product_id"].ToString()), float.Parse(reader["quantity"].ToString()));
                        }

                       //MessageBox.Show("aaa" , "b");
                    }
                }


                Product.ClearAllProducts();
                Product.getFromDatabase();
               // delfunc(item.Id);
                _allsales.Remove(item);

            }
            catch (Exception e)
            {
                MessageBox.Show("Failed to delete"+e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    Payment.sDeleteFromAllGivenPayment("jwss"+item.Id);        
                    connection.Close();
                    
                }
            }

        }
        public static void editJWsales(int i, Jewelry_shop_sales item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_jw_sales WHERE id=" + item.Id;
                cmd.ExecuteNonQuery();
                cmd.CommandText = "Insert into tec_jw_sales(price,date,customer_id,goldgiven,totalquantity,moneygiven,details) Values (" + item._pricedummy + ",'" + Convert.ToDateTime(item.Date).ToString("yyyy/MM/dd") + "'," + item.CustomerID + "," + item.GoldGiven + "," + item.TotalQuantity + "," + item.MoneyGiven +  ",'" + item.Details + "');select last_insert_id() as x;";
                cmd.ExecuteNonQuery();

                
                _allsales.RemoveAt(i);
                _allsales.Insert(i, item);

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }
        /*public static Byte[] ConvertToByteFromBitmapImage(BitmapImage bitmapImage)
        {
            byte[] data;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();

            encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }

            return data;
        }*/

        public static void ClearAllProducts()
        {
            _allsales.Clear();
        }


        public static void addJWSale(Jewelry_shop_sales p, ObservableCollection<Product> children)
        {
            float temp = 0;
          //  MessageBox.Show("noo" + temp);
            for (int i = 0; i < children.Count(); i++)
            {
                temp += Product.AllProducts.First(item=>item.Id==children.ElementAt(i).Id).Price* children.ElementAt(i).Quantity;
                //MessageBox.Show("ii" + temp);
            }
                MySqlConnection connection = new MySqlConnection(RealUtility.dbString);
            AddToAllJWSales(p,p.Price);
            for (int i = 0; i < children.Count(); i++)
            {
                try
                {
                    if ((children.ElementAt(i).Name.Length - 14 - children.ElementAt(i).Code.Length - 2 - 2) >= 0)
                    {
                        if (!children.ElementAt(i).Name.Substring(children.ElementAt(i).Name.Length - 14 - children.ElementAt(i).Code.Length - 2 - 2, 14).Equals("(ONLY ORDERED)"))
                        {
                            connection.Open();
                            MySqlCommand cmd;
                            cmd = connection.CreateCommand();
                            //MessageBox.Show(children.ElementAt(i).Id + ""+"   Name::"+ children.ElementAt(i).Name, "prodid" );
                            cmd.CommandText = "Insert into tec_jw_sales_items(jw_sale_id,product_id,quantity) Values ('" + p.Id + "','" + children.ElementAt(i).Id + "','" + children.ElementAt(i).Quantity + "')";

                            cmd.ExecuteNonQuery();
                            cmd.CommandText = "update tec_products set quantity=quantity - " + children.ElementAt(i).Quantity + " where id= " + children.ElementAt(i).Id;

                            cmd.ExecuteNonQuery();
                        }
                        else
                        {
                            MessageBox.Show("The Item -->" + children.ElementAt(i).Name + "<-- is only ordered and doesn't exists in your inventory, you will have to purchase it from a goldsmith or change its status to (COMPLETED) instead of (ONLY ORDERED) if you wish to sell it", "ERROR");
                            //DeleteFromAllJWSales(p);
                           // _allsales.Remove(p);
                        }
                    }else
                    {
                        connection.Open();
                        MySqlCommand cmd;
                        cmd = connection.CreateCommand();
                        //MessageBox.Show(children.ElementAt(i).Id + ""+"   Name::"+ children.ElementAt(i).Name, "prodid" );
                        cmd.CommandText = "Insert into tec_jw_sales_items(jw_sale_id,product_id,quantity) Values ('" + p.Id + "','" + children.ElementAt(i).Id + "','" + children.ElementAt(i).Quantity + "')";

                        cmd.ExecuteNonQuery();
                        cmd.CommandText = "update tec_products set quantity=quantity - " + children.ElementAt(i).Quantity + " where id= " + children.ElementAt(i).Id;

                        cmd.ExecuteNonQuery();
                    }

                }
                catch (Exception e)
                {
                    MessageBox.Show("Failed to make sales. Insufficent product quantity"+ e.ToString(), "Error");
                    break;
                }

                finally
                {
                    if (connection.State == System.Data.ConnectionState.Open)
                    {
                        Product.ClearAllProducts();
                        Product.getFromDatabase();
                        connection.Close();
                    }
                }
            }


        }
        public static void editCombo(Jewelry_shop_sales initial, Jewelry_shop_sales p, ObservableCollection<Product> children)
        {





            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);
            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_jw_sales WHERE id=" + initial.Id;
                cmd.ExecuteNonQuery();
                cmd.CommandText = "Insert into tec_jw_sales(price,date,customer_id,goldgiven,totalquantity,moneygiven,details) Values (" + p._pricedummy + ",'" + Convert.ToDateTime(p.Date).ToString("yyyy/MM/dd") + "'," + p.CustomerID + "," + p.GoldGiven + "," + p.TotalQuantity + "," + p.MoneyGiven + ",'" + p.Details + "');select last_insert_id() as x;";
                cmd.ExecuteNonQuery();


                int l = _allsales.IndexOf(initial);
                _allsales.RemoveAt(l);
                _allsales.Insert(l, p);


            }
            catch (Exception e)
            {
                MessageBox.Show("Failed to add to Product "+e.ToString(), "Error-jwsalesdelte");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            for (int i = 0; i < children.Count(); i++)
            {
                try
                {

                    connection.Open();
                    MySqlCommand cmd;
                    cmd = connection.CreateCommand();
                    cmd.CommandText = "Insert into tec_jw_sales_items(jw_sale_id,product_id,quantity) Values ('" + p.Id + "','" + children.ElementAt(i).Id + "','" + children.ElementAt(i).Quantity + "')";
                    cmd.ExecuteNonQuery();



                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to add to Combo Product", "Error");
                    break;
                }

                finally
                {
                    if (connection.State == System.Data.ConnectionState.Open)
                    {

                        connection.Close();
                    }
                }
            }

        }
        public static float getComboProducts(int i, ObservableCollection<Product> toreturn)
        {
            float tempprice = 0;
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT tec_products.id,tec_products.code,tec_products.name,tec_products.type,tec_products.price,tec_products.cost,tec_products.details,tec_products.length,tec_products.width,tec_products.plasticsize,tec_products.silversize,tec_products.metalcolor,tec_jw_sales_items.quantity FROM tec_products INNER JOIN tec_jw_sales_items on tec_products.id=tec_jw_sales_items.product_id WHERE tec_jw_sales_items.jw_sale_id=" + i;
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new Product();
                            product.Id = int.Parse(reader["id"].ToString());
                            product.Code = reader["code"].ToString();
                            product.Name = reader["name"].ToString() + "(" + reader["code"].ToString() + ")";
                            product.Type = reader["type"].ToString();
                            product.MetalColor = reader["metalcolor"].ToString();

                            product.Price = float.Parse(reader["price"].ToString());
                            product.Cost = float.Parse(reader["cost"].ToString());
                            product.Details = reader["details"].ToString();
                            product.Length = float.Parse(reader["length"].ToString());
                            product.Width = float.Parse(reader["width"].ToString());
                            product.SilverSize = float.Parse(reader["silversize"].ToString());

                            product.PlasticSize = float.Parse(reader["plasticsize"].ToString());
                            product.Quantity = float.Parse(reader["quantity"].ToString());

                            tempprice += product.Price*product.Quantity;

                           // MessageBox.Show(tempprice + "", "Tempprice");
                            toreturn.Insert(0, product);
                        }

                    }
                }
             //   MessageBox.Show(tempprice + "", "Tempprice1");
                return tempprice;
            }
            catch (Exception e)
            {
                MessageBox.Show("Failed to Load Combo from database "+e.ToString(), "Error");
                return 0;
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();

                }

            }

        }



        public static void alterfunc(int id,float quantity)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "update tec_products set quantity=quantity + " + quantity +" where id= "+id;
                cmd.ExecuteNonQuery();
               
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }
/*
        public static void delfunc(int id)
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
               // cmd.CommandText = ";
               // cmd.ExecuteNonQuery();
                

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to delete", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }
        */


        public static void getFromDatabase()
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT *,Date_format(date,'%m/%d/%Y') as x,time_format(date , '%r') as y FROM tec_jw_sales";
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var JWsale = new Jewelry_shop_sales();
                            JWsale.Id = int.Parse(reader["id"].ToString());
                            JWsale.Date = reader["x"].ToString();
                            JWsale.Time = reader["y"].ToString();
                            JWsale.CustomerID = int.Parse(reader["customer_id"].ToString());
                            JWsale._pricedummy= float.Parse(reader["price"].ToString());

                            JWsale.GoldGiven = float.Parse(reader["goldgiven"].ToString());
                            JWsale.TotalQuantity = float.Parse(reader["totalquantity"].ToString());
                            JWsale.MoneyGiven = float.Parse(reader["moneygiven"].ToString());
                            JWsale.Details = reader["details"].ToString();


                            _allsales.Insert(0, JWsale);
                        }

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Failed to Load from database /n"+e.ToString(), "Error-jwsales");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

    }
}


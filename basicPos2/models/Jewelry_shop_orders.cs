﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Collections.ObjectModel;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Windows;
using basicPos2.models;
using System.Windows.Media.Imaging;
using System.IO;
using System.Windows.Media;


namespace basicPos2.models
{
    public partial class Jewelry_shop_orders : INotifyPropertyChanged
    {
        int _id;
        int _customerId;
        string _date;
        string _time;
        string _datedis;
        string _datedue;
        string _timedue;
        string _datedisdue;

        float _goldgiven = 0;
        float _totalquantity=0;//currently it is 0 all over the place and is not used currently
        float _moneygiven=0;
        string _details;


        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
                RaisePropertyChanged("Id");
            }
        }
        public string Date
        {
            get
            {
                return _date;
            }

            set
            {
                _date = value;
                RaisePropertyChanged("Date");
            }
        }
        public string CustomerName
        {
            get
            {
                return Customer.AllCustomers.First(item => item.Id == CustomerID).Name;
            }

        }
        public string Time
        {
            get
            {
                return _time;
            }

            set
            {
                _time = value;
                RaisePropertyChanged("Time");

                RaisePropertyChanged("Datedis");
            }
        }


        public string Datedue
        {
            get
            {
                return _datedue;
            }

            set
            {
                _datedue = value;
                RaisePropertyChanged("Datedue");
            }
        }
        public string Timedue
        {
            get
            {
                return _timedue;
            }

            set
            {
                _timedue = value;
                RaisePropertyChanged("Timedue");

                RaisePropertyChanged("Datedisdue");
            }
        }

        public float TotalQuantity
        {
            get
            {
                return _totalquantity;
            }

            set
            {
                _totalquantity = value;
                RaisePropertyChanged("TotalQuantity");
            }
        }
        public string Details
        {
            get
            {
                return _details;
            }

            set
            {
                _details = value;
                RaisePropertyChanged("Details");
            }
        }
        public int CustomerID
        {
            get
            {
                return _customerId;
            }

            set
            {
                _customerId= value;
                RaisePropertyChanged("CustomerID");
            }
        }

        public string Datedis
        {
            get
            {
                return _date + " " + _time;
            }

            set
            {

                RaisePropertyChanged("Datedis");
            }
        }

        public string Datedisdue
        {
            get
            {
                return _datedue + " " + _timedue;
            }

            set
            {

                RaisePropertyChanged("Datedisdue");
            }
        }


        public float GoldGiven
        {
            get
            {
                return _goldgiven;
            }

            set
            {
                _goldgiven = value;
                RaisePropertyChanged("GoldGiven");
            }
        }

      
        public float MoneyGiven
        {
            get
            {
                return _moneygiven;
            }

            set
            {
                _moneygiven = value;
                RaisePropertyChanged("MoneyGiven");
            }
        }

       
        private void RaisePropertyChanged(string propertyName)
        {

            if (null != PropertyChanged)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }

    partial class Jewelry_shop_orders
    {
        static ObservableCollection<Jewelry_shop_orders> _allorders = new ObservableCollection<Jewelry_shop_orders>();


        public static ObservableCollection<Jewelry_shop_orders> AllJewelryOrders
        {
            get
            {
                if (_allorders.Count == 0)
                {

                    getFromDatabase();
                }

                return _allorders;
            }
        }

        public static void AddToAllJWOrders(Jewelry_shop_orders item)
        {


            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd = connection.CreateCommand();
                cmd.CommandText = "Insert into tec_jw_orders(date,datedue,customer_id,totalquantity,goldgiven,moneygiven,details) Values ('" + Convert.ToDateTime(item.Date).ToString("yyyy/MM/dd") + " " + RealUtility.convto24(item.Time) + "','"+ Convert.ToDateTime(item.Datedue).ToString("yyyy/MM/dd") + " " + RealUtility.convto24(item.Timedue) + "'," + item.CustomerID + "," + 0 + "," + item.TotalQuantity + "," + 0 + ",'" + item.Details+ "');select last_insert_id() as x;";
                MySqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    item.Id = int.Parse(dr["x"].ToString());
                }
                _allorders.Insert(0, item);
                Payment.AddPayment(new Payment { SaleID = "jwso" + item.Id, CustomerId = "j" + item.CustomerID, Moneygiven = item.MoneyGiven, Goldgiven = item.GoldGiven, Reference = "Initial Pay", Date = item.Date, Time = item.Time });

               

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }

        public static void DeleteFromAllJWOrders(Jewelry_shop_orders item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_jw_orders WHERE id=" + item.Id;
                cmd.ExecuteNonQuery();
                _allorders.Remove(item);

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to delete", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {

                    Payment.sDeleteFromAllGivenPayment("jwso" + item.Id);
                    connection.Close();
                }
            }

        }
        public static void editJWorders(int i, Jewelry_shop_orders item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_jw_orders WHERE id=" + item.Id;
                cmd.ExecuteNonQuery();
                cmd.CommandText = "Insert into tec_jw_orders(date,customer_id,goldgiven,totalquantity,moneygiven,details) Values ('" + Convert.ToDateTime(item.Date).ToString("yyyy/MM/dd") + "'," + item.CustomerID + "," + item.GoldGiven + "," + item.TotalQuantity + "," + item.MoneyGiven  + ",'" + item.Details + "');select last_insert_id() as x;";
                cmd.ExecuteNonQuery();


                _allorders.RemoveAt(i);
                _allorders.Insert(i, item);

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }
        /*public static Byte[] ConvertToByteFromBitmapImage(BitmapImage bitmapImage)
        {
            byte[] data;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();

            encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }

            return data;
        }*/

        public static void ClearAllProducts()
        {
            _allorders.Clear();
        }
        

        public static void addJWOrder(Jewelry_shop_orders p, ObservableCollection<Product> children)
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);
            AddToAllJWOrders(p);
            for (int i = 0; i < children.Count(); i++)
            {
                try
                {

                    connection.Open();
                    MySqlCommand cmd;
                    cmd = connection.CreateCommand();
                    cmd.CommandText = "Insert into tec_jw_orders_items(jw_order_id,product_id,quantity) Values ('" + p.Id + "','" + children.ElementAt(i).Id + "','" + children.ElementAt(i).Quantity + "')";

                    cmd.ExecuteNonQuery();
                  



                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to add to Combo Product", "Error");
                    break;
                }

                finally
                {
                    if (connection.State == System.Data.ConnectionState.Open)
                    {

                        Product.ClearAllProducts();
                        Product.getFromDatabase();
                        connection.Close();
                    }
                }
            }


        }
        public static void editCombo(Jewelry_shop_orders initial, Jewelry_shop_orders p, ObservableCollection<Product> children)
        {





            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);
            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_jw_orders WHERE id=" + initial.Id;
                cmd.ExecuteNonQuery();
                cmd.CommandText = "Insert into tec_jw_orders(date,customer_id,goldgiven,totalquantity,moneygiven,details) Values ('" + Convert.ToDateTime(p.Date).ToString("yyyy/MM/dd") + "'," + p.CustomerID + "," + p.GoldGiven + "," + p.TotalQuantity + "," + p.MoneyGiven + ",'" + p.Details + "');select last_insert_id() as x;";
                cmd.ExecuteNonQuery();


                int l = _allorders.IndexOf(initial);
                _allorders.RemoveAt(l);
                _allorders.Insert(l, p);


            }
            catch (Exception)
            {
                MessageBox.Show("Failed to add to Product", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
           
            for (int i = 0; i < children.Count(); i++)
            {
                try
                {

                    connection.Open();
                    MySqlCommand cmd;
                    cmd = connection.CreateCommand();
                    cmd.CommandText = "Insert into tec_jw_orders_items(jw_order_id,product_id,quantity) Values ('" + p.Id + "','" + children.ElementAt(i).Id + "','" + children.ElementAt(i).Quantity + "')";
                    cmd.ExecuteNonQuery();



                }
                catch (Exception)
                {
                    MessageBox.Show("Failed to add to Combo Product", "Error");
                    break;
                }

                finally
                {
                    if (connection.State == System.Data.ConnectionState.Open)
                    {

                        connection.Close();
                    }
                }
            }

        }
        public static void getComboProducts(int i, ObservableCollection<Product> toreturn)
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT tec_products.id,tec_products.code,tec_products.name,tec_products.type,tec_products.price,tec_products.cost,tec_products.details,tec_products.length,tec_products.width,tec_products.plasticsize,tec_products.silversize,tec_products.metalcolor,tec_jw_orders_items.quantity FROM tec_products INNER JOIN tec_jw_orders_items on tec_products.id=tec_jw_orders_items.product_id WHERE tec_jw_orders_items.jw_order_id=" + i;
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new Product();
                            product.Id = int.Parse(reader["id"].ToString());
                            product.Code = reader["code"].ToString();
                            product.Name = reader["name"].ToString() + "(" + reader["code"].ToString() + ")";
                            product.Type = reader["type"].ToString();
                            product.MetalColor = reader["metalcolor"].ToString();

                            product.Price = float.Parse(reader["price"].ToString());
                            product.Cost = float.Parse(reader["cost"].ToString());
                            product.Details = reader["details"].ToString();
                            product.Length = float.Parse(reader["length"].ToString());
                            product.Width = float.Parse(reader["width"].ToString());
                            product.SilverSize = float.Parse(reader["silversize"].ToString());

                            product.PlasticSize = float.Parse(reader["plasticsize"].ToString());
                            product.Quantity = float.Parse(reader["quantity"].ToString());



                            toreturn.Insert(0, product);
                        }

                    }
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Load Combo from database", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();

                }

            }

        }

        public static void getFromDatabase()
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT *,Date_format(date,'%m/%d/%Y') as x,time_format(date , '%r') as y,Date_format(datedue,'%m/%d/%Y') as xx,time_format(datedue , '%r') as yy FROM tec_jw_orders";
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var JWorder = new Jewelry_shop_orders();
                            JWorder.Id = int.Parse(reader["id"].ToString());
                            JWorder.Date = reader["x"].ToString();

                            JWorder.Time = reader["y"].ToString();
                            JWorder.Datedue = reader["xx"].ToString();
                            JWorder.Timedue = reader["yy"].ToString();
                            JWorder.CustomerID = int.Parse(reader["customer_id"].ToString());

                            
                            JWorder.GoldGiven= float.Parse(reader["goldgiven"].ToString());
                            JWorder.TotalQuantity = float.Parse(reader["totalquantity"].ToString());
                            
                            JWorder.MoneyGiven = float.Parse(reader["moneygiven"].ToString());
                            JWorder.Details = reader["details"].ToString();


                            _allorders.Insert(0, JWorder);
                        }

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Failed to Load from database /n "+e.ToString(), "Error-jwso");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

    }
}


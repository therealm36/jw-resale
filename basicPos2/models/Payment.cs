﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace basicPos2.models
{
    public partial class Payment : INotifyPropertyChanged
    {
        int _id;
        string _saleid;
        string _date;
        string _time;
        string _reference;
        float _amount;
        string _paidby;
        string _datedis;
        float _goldgiven;
        float _moneygiven;

        string _customer_id;

        //string _image;


        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
                RaisePropertyChanged("Id");
            }
        }
        public string CustomerId
        {
            get
            {
                return _customer_id;
            }

            set
            {
                _customer_id = value;
                RaisePropertyChanged("CustomerId");
            }
        }
        public string SaleID
        {
            get
            {
                return _saleid;
            }

            set
            {
                _saleid = value;
                RaisePropertyChanged("SaleID");
            }
        }

        public string Date
        {
            get
            {
                return _date;
            }

            set
            {
                _date = value;
                RaisePropertyChanged("Date");
                RaisePropertyChanged("Datedis");

            }
        }
        public string Datedis
        {
            get
            {
                return _date+" "+_time;
            }

            set
            {
                
                RaisePropertyChanged("Datedis");
            }
        }
        public string Time
        {
            get
            {
                return _time;
            }

            set
            {
                _time = value;
                RaisePropertyChanged("Time");

                RaisePropertyChanged("Datedis");
            }
        }
        public float Amount
        {
            get
            {
                return _amount;
            }

            set
            {
                _amount = value;
                RaisePropertyChanged("Amount");
            }
        }
        public float Goldgiven
        {
            get
            {
                return _goldgiven;
            }

            set
            {
                _goldgiven = value;
                RaisePropertyChanged("Goldgiven");
            }
        }
        public float Moneygiven
        {
            get
            {
                return _moneygiven;
            }

            set
            {
                _moneygiven = value;
                RaisePropertyChanged("Moneygiven");
            }
        }
        public string Reference
        {
            get
            {
                return _reference;
            }

            set
            {
                _reference = value;
                RaisePropertyChanged("Reference");
            }
        }

        public string PaidBy
        {
            get
            {
                return _paidby;
            }

            set
            {
                _paidby = value;
                RaisePropertyChanged("PaidBy");
            }
        }




        /*public string Image
        {
            get
            {
                return _image;
            }

            set
            {
                _image = value;
                RaisePropertyChanged("Image");
            }
        }*/




        private void RaisePropertyChanged(string propertyName)
        {

            if (null != PropertyChanged)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }


    partial class Payment
    {
        static ObservableCollection<Payment> _needepayments = new ObservableCollection<Payment>();


        public static ObservableCollection<Payment> AllPayment
        {
            get
            {
                _needepayments.Clear();
                MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

                try
                {
                    connection.Open();
                    string sql = "SELECT *,Date_format(date,'%d/%m/%Y') as x FROM tec_payments";
                    using (var command = new MySqlCommand(sql, connection))
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var product = new Payment();
                               product.Id = int.Parse(reader["id"].ToString());
                           //     product.SaleID = reader["sale_id"].ToString();
                              //  product.Reference = reader["reference"].ToString();
                            //    product.Date = reader["x"].ToString();
                              //  product.Amount = float.Parse(reader["amount"].ToString());
                             //   product.PaidBy = reader["paid_by"].ToString();




                                _needepayments.Insert(0,product);
                            }

                        }
                    }

                }
                catch (Exception e)
                {
                    MessageBox.Show("Failed to Load from database"+e.ToString(), "Error-Payments");
                }

                finally
                {
                    if (connection.State == System.Data.ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }

                return _needepayments;
            }
        }
        public static void DeleteFromAllGivenPayment(Payment item, ObservableCollection<Payment> givenlist)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_payments WHERE id=" + item.Id;
                cmd.ExecuteNonQuery();
                if (item.SaleID.Substring(0, 4).Equals("jwss"))
                {
                    cmd.CommandText = "UPDATE tec_jw_sales set goldgiven=goldgiven-" + item.Goldgiven + ",moneygiven=moneygiven-" + item.Moneygiven + " where id=" + item.SaleID.Substring(4);
                    cmd.ExecuteNonQuery();

                    Jewelry_shop_sales.ClearAllProducts();
                    Jewelry_shop_sales.getFromDatabase();
                }
                else
                {

                    if (item.SaleID.Substring(0, 4).Equals("jwso"))
                    {
                        cmd.CommandText = "UPDATE tec_jw_orders set goldgiven=goldgiven-" + item.Goldgiven + ",moneygiven=moneygiven-" + item.Moneygiven + " where id=" + item.SaleID.Substring(4);
                        cmd.ExecuteNonQuery();

                        Jewelry_shop_orders.ClearAllProducts();
                        Jewelry_shop_orders.getFromDatabase();
                    }
                    else
                    {

                        if (item.SaleID.Substring(0, 4).Equals("gssp"))
                        {
                            cmd.CommandText = "UPDATE tec_gs_purchases set goldgiven=goldgiven-" + item.Goldgiven + ",moneygiven=moneygiven-" + item.Moneygiven + " where id=" + item.SaleID.Substring(4);
                            cmd.ExecuteNonQuery();

                            Gold_Smith_Purchases.ClearAllProducts();
                            Gold_Smith_Purchases.getFromDatabase();
                        }
                        else
                        {

                            if (item.SaleID.Substring(0, 4).Equals("gsso"))
                            {
                                cmd.CommandText = "UPDATE tec_gs_orders set goldgiven=goldgiven-" + item.Goldgiven + ",moneygiven=moneygiven-" + item.Moneygiven + " where id=" + item.SaleID.Substring(4);
                                cmd.ExecuteNonQuery();

                                Gold_Smith_orders.ClearAllProducts();
                                Gold_Smith_orders.getFromDatabase();
                            }
                            else
                            {
                                MessageBox.Show("InvalidID", "Error-payment");
                            }
                        }
                    }
                }
                givenlist.Remove(item);

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to delete", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }

        public static void sDeleteFromAllGivenPayment(String s)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_payments WHERE sale_id='" + s+"'";
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show("Failed to delete" +e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }
        public static void editPaymentinGivenList(float i, int j, Payment item, ObservableCollection<Payment> givenlist)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE tec_payments set date='"+ Convert.ToDateTime(item.Date).ToString("yyyy/MM/dd") +" "+ RealUtility.convto24(item.Time) + "', paid_by='" +item.PaidBy+"', amount='"+item.Amount+"', reference='"+item.Reference+"' WHERE id="+item.Id;

                cmd.ExecuteNonQuery();
                cmd.CommandText = "UPDATE tec_sales set paid=paid-" + i + "+" + item.Amount + " where id=" + item.SaleID;
                cmd.ExecuteNonQuery();
                if (givenlist != null)
                {
                    givenlist.RemoveAt(j);
                    givenlist.Insert(j, item);
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to edit", "Error-edit");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }
        public static void AddPayment(Payment m)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "Insert into tec_payments(date,sale_id,customer_id,goldgiven,moneygiven,reference) Values ('" + Convert.ToDateTime(m.Date).ToString("yyyy/MM/dd")+" "+ RealUtility.convto24(m.Time) + "','"+m.SaleID + "','"  + m.CustomerId+ "','" + m.Goldgiven + "','" +m.Moneygiven+ "','" + m.Reference + "');select last_insert_id() as x;";
                MySqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    m.Id = int.Parse(dr["x"].ToString());
                }
             
            

            }
            catch (Exception e)
            {
                MessageBox.Show("Failed to add"+e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    specfunc(m);
                    connection.Close();
                }
            }
        }

        public static void specfunc(Payment m)
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                if (m.SaleID.Substring(0, 4).Equals("jwss"))
                {
                    cmd.CommandText = "UPDATE tec_jw_sales set goldgiven=goldgiven+" + m.Goldgiven + ",moneygiven=moneygiven+" + m.Moneygiven + " where id=" + m.SaleID.Substring(4);
                    cmd.ExecuteNonQuery();
                    Jewelry_shop_sales.ClearAllProducts();
                    Jewelry_shop_sales.getFromDatabase();
                }
                else
                {

                    if (m.SaleID.Substring(0, 4).Equals("jwso"))
                    {
                        cmd.CommandText = "UPDATE tec_jw_orders set goldgiven=goldgiven+" + m.Goldgiven + ",moneygiven=moneygiven+" + m.Moneygiven + " where id=" + m.SaleID.Substring(4);
                        cmd.ExecuteNonQuery();

                        Jewelry_shop_orders.ClearAllProducts();
                        Jewelry_shop_orders.getFromDatabase();
                    }
                    else
                    {

                        if (m.SaleID.Substring(0, 4).Equals("gssp"))
                        {
                            cmd.CommandText = "UPDATE tec_gs_purchases set goldgiven=goldgiven+" + m.Goldgiven + ",moneygiven=moneygiven+" + m.Moneygiven + " where id=" + m.SaleID.Substring(4);
                            cmd.ExecuteNonQuery();

                            Gold_Smith_Purchases.ClearAllProducts();
                            Gold_Smith_Purchases.getFromDatabase();
                        }
                        else
                        {

                            if (m.SaleID.Substring(0, 4).Equals("gsso"))
                            {
                                cmd.CommandText = "UPDATE tec_gs_orders set goldgiven=goldgiven+" + m.Goldgiven + ",moneygiven=moneygiven+" + m.Moneygiven + " where id=" + m.SaleID.Substring(4);
                                cmd.ExecuteNonQuery();

                                Gold_Smith_orders.ClearAllProducts();
                                Gold_Smith_orders.getFromDatabase();
                            }
                            else
                            {
                                MessageBox.Show("InvalidID", "Error-payment");
                            }
                        }
                    }
                }


            }
            catch (Exception e)
            {
                MessageBox.Show("Failed to add" + e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            
        }


        public static void getFromDatabase(Sales item, ObservableCollection<Payment> givenlist)
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT *,Date_format(date,'%m/%d/%Y') as x,time_format(date , '%r') as y FROM tec_payments WHERE sale_id=" + item.Id;

                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new Payment();
                            product.Id = int.Parse(reader["id"].ToString());
                            //  product.SaleID = int.Parse(reader["sale_id"].ToString());
                            product.Reference = reader["reference"].ToString();
                            product.Date = reader["x"].ToString();
                            product.Time = reader["y"].ToString();
                            product.Amount = float.Parse(reader["amount"].ToString());
                            product.PaidBy = reader["paid_by"].ToString();




                            givenlist.Insert(0, product);
                        }

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        public static void getFromDatabasejwso(Jewelry_shop_orders item, ObservableCollection<Payment> givenlist)
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT *,Date_format(date,'%m/%d/%Y') as x,time_format(date , '%r') as y FROM tec_payments WHERE sale_id='jwso" + item.Id+"'";

                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new Payment();
                            product.Id = int.Parse(reader["id"].ToString());
                            //  product.SaleID = int.Parse(reader["sale_id"].ToString());
                            product.Reference = reader["reference"].ToString();
                            product.Date = reader["x"].ToString();
                            product.Time = reader["y"].ToString();
                            product.Goldgiven = float.Parse(reader["goldgiven"].ToString());
                            product.Moneygiven = float.Parse(reader["moneygiven"].ToString());




                            givenlist.Insert(0, product);
                        }

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        public static void getFromDatabasejwss(Jewelry_shop_sales item, ObservableCollection<Payment> givenlist)
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT *,Date_format(date,'%m/%d/%Y') as x,time_format(date , '%r') as y FROM tec_payments WHERE sale_id='jwss" + item.Id+"'";

                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new Payment();
                            product.Id = int.Parse(reader["id"].ToString());
                             product.SaleID = reader["sale_id"].ToString();
                            product.Reference = reader["reference"].ToString();
                            product.Date = reader["x"].ToString();
                            product.Time = reader["y"].ToString();
                            product.Goldgiven = float.Parse(reader["goldgiven"].ToString());
                            product.Moneygiven = float.Parse(reader["moneygiven"].ToString());




                            givenlist.Insert(0, product);
                        }

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }
        public static void getFromDatabasegsso(Gold_Smith_orders item, ObservableCollection<Payment> givenlist)
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT *,Date_format(date,'%m/%d/%Y') as x,time_format(date , '%r') as y FROM tec_payments WHERE sale_id='gsso" + item.Id + "'";

                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new Payment();
                            product.Id = int.Parse(reader["id"].ToString());
                            //  product.SaleID = int.Parse(reader["sale_id"].ToString());
                            product.Reference = reader["reference"].ToString();
                            product.Date = reader["x"].ToString();
                            product.Time = reader["y"].ToString();
                            product.Goldgiven = float.Parse(reader["goldgiven"].ToString());
                            product.Moneygiven = float.Parse(reader["moneygiven"].ToString());




                            givenlist.Insert(0, product);
                        }

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }
        public static void getFromDatabasegssp(Gold_Smith_Purchases item, ObservableCollection<Payment> givenlist)
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT *,Date_format(date,'%m/%d/%Y') as x,time_format(date , '%r') as y FROM tec_payments WHERE sale_id='gssp" + item.Id + "'";

                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new Payment();
                            product.Id = int.Parse(reader["id"].ToString());
                            product.SaleID = reader["sale_id"].ToString();
                            product.Reference = reader["reference"].ToString();
                            product.Date = reader["x"].ToString();
                            product.Time = reader["y"].ToString();
                            product.Goldgiven = float.Parse(reader["goldgiven"].ToString());
                            product.Moneygiven = float.Parse(reader["moneygiven"].ToString());




                            givenlist.Insert(0, product);
                        }

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

    }
}

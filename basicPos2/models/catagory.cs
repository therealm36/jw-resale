﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace basicPos2.models
{
    public partial class Catagory : INotifyPropertyChanged
    {
        int _id;
        string _code;
        string _name;

        string _vis;
        byte[] _image;

        public byte[] Image
        {
            get
            {
                return _image;
            }

            set
            {
                _image = value;
                RaisePropertyChanged("Image");
            }
        }
        public string Vis
        {
            get
            {
                return _vis;
            }

            set
            {
                _vis = value;
                RaisePropertyChanged("Vis");
            }
        }

        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
                RaisePropertyChanged("Id");
            }
        }

        public string Code
        {
            get
            {
                return _code;
            }

            set
            {
                _code = value;
                RaisePropertyChanged("Code");
            }
        }
       

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
                RaisePropertyChanged("Name");
            }
        }

       
        

        
        
     

        private void RaisePropertyChanged(string propertyName)
        {

            if (null != PropertyChanged)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }
    

    partial class Catagory
    {
        static ObservableCollection<Catagory> _allcatagories = new ObservableCollection<Catagory>();


        public static ObservableCollection<Catagory> AllCatagories
        {
            get
            {
                if (_allcatagories.Count == 0)
                {

                    getFromDatabase();
                }

                return _allcatagories;
            }
        }

        public static void AddToAllCatagories(Catagory item)
        {


            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "Insert into tec_categories(id,code,name,image) Values ('" + item.Id + "','" + item.Code + "','" + item.Name +"',@image)";
                cmd.Parameters.Add("@image", MySqlDbType.LongBlob).Value = item.Image;
                

                cmd.ExecuteNonQuery();
                _allcatagories.Insert(0,item);

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }

        public static void DeleteFromAllCatagories(Catagory item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_categories WHERE id=" + item.Id;
                cmd.ExecuteNonQuery();
                _allcatagories.Remove(item);

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to delete", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }
        public static void editCategory(int i, Catagory item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_categories WHERE id=" + item.Id;
                cmd.ExecuteNonQuery();
                cmd.CommandText = "Insert into tec_categories(id,code,name,image) Values ('" + item.Id + "','" + item.Code + "','" + item.Name + "',@image)";
                cmd.Parameters.Add("@image", MySqlDbType.LongBlob).Value = item.Image;
                cmd.ExecuteNonQuery();
                
                _allcatagories.RemoveAt(i);
                _allcatagories.Insert(i, item);

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to edit", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        public static void ClearAllProducts()
        {
            _allcatagories.Clear();
        }

        public override string ToString()
        {
            return Name + " (" + Code + " )";

        }

       
       
        
       
        

        public static void getFromDatabase()
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT * FROM tec_categories";
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new Catagory();
                            product.Id = int.Parse(reader["id"].ToString());
                            product.Code = reader["code"].ToString();
                            product.Name = reader["name"].ToString();
                            if (!reader["image"].ToString().Equals(""))
                            {
                                product.Image = (byte[])reader["image"];
                                product.Vis = "Hidden";
                            }



                            _allcatagories.Insert(0,product);
                        }

                    }
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Load from database", "Error-Catagory");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

    }
}

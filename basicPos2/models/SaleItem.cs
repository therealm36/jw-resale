﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace basicPos2.models
{
    public partial class SaleItem:INotifyPropertyChanged
    {

        int _id;
        int _saleId;
        string _productId;
        float _quantity;
        float _unitPrice;
        float _itemDiscount;
        float _realUnitPrice;
        float _subTotal;
        float _cost;
        string _productCode;
        string _productName;
        string _comment;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int SaleId
        {
            get
            {
                return _saleId;
            }

            set
            {
                _saleId = value;
                RaisePropertyChanged("SaleId");
            }
        }

        public string ProductId
        {
            get
            {
                return _productId;
            }

            set
            {
                _productId = value;
                RaisePropertyChanged("ProductId");
            }
        }

        public float Quantity
        {
            get
            {
                return _quantity;
            }

            set
            {
                _quantity = value;
                RaisePropertyChanged("Quantity");
                RaisePropertyChanged("SubTotal");

            }
        }

        public float UnitPrice
        {
            get
            {
                return _realUnitPrice-_itemDiscount;
            }

            
        }

        public float ItemDiscount
        {
            get
            {
                return _itemDiscount;
            }

            set
            {
                _itemDiscount = value;
                RaisePropertyChanged("ItemDiscount");
                RaisePropertyChanged("UnitPrice");
                RaisePropertyChanged("SubTotal");

            }
        }

        public float RealUnitPrice
        {
            get
            {
                return _realUnitPrice;
            }

            set
            {
                _realUnitPrice = value;
                RaisePropertyChanged("RealUnitPrice");
                RaisePropertyChanged("UnitPrice");
                RaisePropertyChanged("SubTotal");
            }
        }

        public float Cost
        {
            get
            {
                return _cost;
            }

            set
            {
                _cost = value;
            }
        }

        public string ProductCode
        {
            get
            {
                return _productCode;
            }

            set
            {
                _productCode = value;
            }
        }

        public string ProductName
        {
            get
            {
                return _productName;
            }

            set
            {
                _productName = value;
            }
        }

        public string Comment
        {
            get
            {
                return _comment;
            }

            set
            {
                _comment = value;
            }
        }

        public float SubTotal
        {
            get
            {
                return _quantity*UnitPrice;
            }

       
        }

        private void RaisePropertyChanged(string propertyName)
        {

            if (null != PropertyChanged)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }

    partial class SaleItem
    {

        public static ObservableCollection<SaleItem> _currentBill = new ObservableCollection<SaleItem>();
       
        public static ObservableCollection<SaleItem> CurrentBill
        {
            get { return _currentBill; }
        }

        public static void AddToCurrentBill(SaleItem item)
        {
            _currentBill.Insert(0,item);
        }

        public static void AddToCurrentBill(Product item)
        {
            if (CurrentBill.Any(i=>Convert.ToInt32(i.ProductId) == item.Id))
            {
                var index = CurrentBill.IndexOf(CurrentBill.First(i => i.ProductCode == item.Code));
                CurrentBill[index].Quantity += 1; 
            }
            else
            {
                _currentBill.Insert(0,new SaleItem {ProductId = Convert.ToString(item.Id), Cost=item.Cost, ProductName = item.Name, RealUnitPrice = item.Price, Quantity = 1, ProductCode=item.Code });

            }
        }

        public static void DeleteFromCurrentBill(int position)
        {
            _currentBill.RemoveAt(position);
        }

        public static void ClearCurrentBill()
        {
            _currentBill.Clear();
        }


    }
}

﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace basicPos2.models
{
    public partial class ExpenseModel : INotifyPropertyChanged
    {
        int _id;
        string _date;
        string _time;
        string _datedis;
        string _reference;
        float _amount;
        string _note;
        string _createdby;


        //string _image;


        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
                RaisePropertyChanged("Id");
            }
        }
        public string Date
        {
            get
            {
                return _date;
            }

            set
            {
                _date = value;
                RaisePropertyChanged("Date");
                RaisePropertyChanged("Datedis");

            }
        }
        public string Time
        {
            get
            {
                return _time;
            }

            set
            {
                _time = value;
                RaisePropertyChanged("Time");

                RaisePropertyChanged("Datedis");
            }
        }
        public string Datedis
        {
            get
            {
                return _date + " " + _time;
            }

            set
            {

                RaisePropertyChanged("Datedis");
            }
        }
       

        public string Name
        {
            get
            {
                return _reference;
            }

            set
            {
                _reference = value;
                RaisePropertyChanged("Reference");
            }
        }
        public float Amount
        {
            get
            {
                return _amount;
            }

            set
            {
                _amount = value;
                RaisePropertyChanged("Amount");
            }
        }
        public string Note
        {
            get
            {
                return _note;
            }

            set
            {
                _note = value;
                RaisePropertyChanged("Note");
            }
        }

        public string Created_by
        {
            get
            {
                return _createdby;
            }

            set
            {
                _createdby= value;
                RaisePropertyChanged("CreatedBy");
            }
        }




        /*public string Image
        {
            get
            {
                return _image;
            }

            set
            {
                _image = value;
                RaisePropertyChanged("Image");
            }
        }*/




        private void RaisePropertyChanged(string propertyName)
        {

            if (null != PropertyChanged)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;

    }


    partial class ExpenseModel
    {
        static ObservableCollection<ExpenseModel> _allexpenses = new ObservableCollection<ExpenseModel>();


        public static ObservableCollection<ExpenseModel> AllExpenses
        {
            get
            {
                if (_allexpenses.Count == 0)
                {

                    getFromDatabase();
                }

                return _allexpenses;
            }
        }

        public static void AddToAllExpenses(ExpenseModel item)
        {


            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "Insert into tec_expenses(id,date,reference,amount,note) Values ('" + item.Id + "','" + Convert.ToDateTime(item.Date).ToString("yyyy/MM/dd") +" "+ RealUtility.convto24(item.Time) + "','" + item.Name + "','" + item.Amount + "','" + item.Note +"')";
                Console.WriteLine(cmd.CommandText);
                cmd.ExecuteNonQuery();
                _allexpenses.Insert(0,item);

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to add to Supplier", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }

        public static void DeleteFromAllExpenses(ExpenseModel item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_expenses WHERE id=" + item.Id;
                cmd.ExecuteNonQuery();
                _allexpenses.Remove(item);

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to delete", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }
        public static void editExpense(int i, ExpenseModel item)
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {

                connection.Open();
                MySqlCommand cmd;
                cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM tec_expenses WHERE id=" + item.Id;
                cmd.ExecuteNonQuery();
                cmd.CommandText = "Insert into tec_expenses(id,date,reference,amount,note) Values ('" + item.Id + "','" + Convert.ToDateTime(item.Date).ToString("yyyy/MM/dd") +" "+ RealUtility.convto24(item.Time) + "','" + item.Name + "','" + item.Amount + "','" + item.Note + "')";
                cmd.ExecuteNonQuery();
                _allexpenses.RemoveAt(i);
                _allexpenses.Insert(i, item);

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to edit", "Error");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        public static void ClearAllSuppliers()
        {
            _allexpenses.Clear();
        }

        
        public static void getFromDatabase()
        {

            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
                string sql = "SELECT *,Date_format(date,'%m/%d/%Y') as x,time_format(date , '%r') as y FROM tec_expenses";
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var product = new ExpenseModel();
                            product.Id = int.Parse(reader["id"].ToString());
                            product.Date = reader["x"].ToString();
                            product.Time = reader["y"].ToString();

                            product.Name = reader["reference"].ToString();
                            product.Note = reader["note"].ToString();
                            product.Amount = float.Parse(reader["amount"].ToString());
                           




                            _allexpenses.Insert(0,product);
                        }

                    }
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Failed to Load from database", "Error-Expense");
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }
    }
}

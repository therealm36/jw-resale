﻿using basicPos2.models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for Monthlysalesreport.xaml
    /// </summary>
    public partial class Monthlysalesreport : UserControl
    {
        public Monthlysalesreport()
        {
            InitializeComponent();
            produtsdataGrid.ItemsSource = basicPos2.models.MonthlyReport.Monthlyreport(DateTime.Today.AddDays(-180).ToString(), DateTime.Today.ToString());
            Datestart.Text = DateTime.Today.AddDays(-180).ToString();
            Dateend.Text = DateTime.Today.ToString();
            totallabel.Content = ((IEnumerable<basicPos2.models.MonthlyReport>)produtsdataGrid.ItemsSource).Sum(item => item.Total);
            totaldiscountlabel.Content = ((IEnumerable<basicPos2.models.MonthlyReport>)produtsdataGrid.ItemsSource).Sum(item => item.TotalDiscount);
            grandtotallabel.Content = ((IEnumerable<basicPos2.models.MonthlyReport>)produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
            totalpaidlabel.Content = ((IEnumerable<basicPos2.models.MonthlyReport>)produtsdataGrid.ItemsSource).Sum(item => item.TotalPaid);

        }
        private void button5_Click(object sender, RoutedEventArgs e)
        {
            Datestart.BorderBrush = Brushes.Black;
            Dateend.BorderBrush = Brushes.Black;
            if (Datestart.SelectedDate == null)
            {
                Datestart.BorderBrush = Brushes.Red;
                MessageBox.Show("Please input a valid starting date");
            }
            else
            {
                if (Dateend.SelectedDate == null)
                {
                    Dateend.BorderBrush = Brushes.Red;
                    MessageBox.Show("Please input a valid ending date");
                }
                else
                {
                    produtsdataGrid.ItemsSource = MonthlyReport.Monthlyreport(Datestart.Text, Dateend.Text);
                    totallabel.Content = ((IEnumerable<basicPos2.models.MonthlyReport>)produtsdataGrid.ItemsSource).Sum(item => item.Total);
                    totaldiscountlabel.Content = ((IEnumerable<basicPos2.models.MonthlyReport>)produtsdataGrid.ItemsSource).Sum(item => item.TotalDiscount);
                    grandtotallabel.Content = ((IEnumerable<basicPos2.models.MonthlyReport>)produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
                    totalpaidlabel.Content = ((IEnumerable<basicPos2.models. MonthlyReport>)produtsdataGrid.ItemsSource).Sum(item => item.TotalPaid);

                }
            }
        }
        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {

            IEnumerable<MonthlyReport> backup = new ObservableCollection<MonthlyReport>((IEnumerable<MonthlyReport>)produtsdataGrid.ItemsSource);

            if (e.Key == Key.Return)
            {
                if (!textBox.Text.ToString().Equals(""))
                {

                    IEnumerable<MonthlyReport> temp = new ObservableCollection<MonthlyReport>();
                    temp = ((IEnumerable<MonthlyReport>)produtsdataGrid.ItemsSource).Where(item => item.Date.ToLower().Contains(textBox.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                    totallabel.Content = ((IEnumerable<basicPos2.models.MonthlyReport>)produtsdataGrid.ItemsSource).Sum(item => item.Total);
                    totaldiscountlabel.Content = ((IEnumerable<basicPos2.models.MonthlyReport>)produtsdataGrid.ItemsSource).Sum(item => item.TotalDiscount);
                    grandtotallabel.Content = ((IEnumerable<basicPos2.models.MonthlyReport>)produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
                    totalpaidlabel.Content = ((IEnumerable<basicPos2.models.MonthlyReport>)produtsdataGrid.ItemsSource).Sum(item => item.TotalPaid);

                }
                else
                {
                    textBox.Text = "[Date]";
                    produtsdataGrid.ItemsSource = backup;
                    produtsdataGrid.Items.Refresh();
                    totallabel.Content = ((IEnumerable<basicPos2.models.MonthlyReport>)produtsdataGrid.ItemsSource).Sum(item => item.Total);
                    totaldiscountlabel.Content = ((IEnumerable<basicPos2.models.MonthlyReport>)produtsdataGrid.ItemsSource).Sum(item => item.TotalDiscount);
                    grandtotallabel.Content = ((IEnumerable<basicPos2.models.MonthlyReport>)produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
                    totalpaidlabel.Content = ((IEnumerable<basicPos2.models.MonthlyReport>)produtsdataGrid.ItemsSource).Sum(item => item.TotalPaid);

                }
            }
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            RealUtility.ExportToPdfMonthlyreport(produtsdataGrid);
        }




        private void button2_Click(object sender, RoutedEventArgs e)
        {
            RealUtility.Exporttocsvmonthlyreport(produtsdataGrid);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            try { 
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");
            RealUtility.GenerateExcelmonthlyreport(((ObservableCollection<MonthlyReport>)produtsdataGrid.ItemsSource).ToDataTable());
            RealUtility.workBook.SaveAs(RealUtility.pathfornarnia+ "MonthlySalesReport--" + RealUtility.todaydate + ".xls", System.IO.FileMode.Create, FileAccess.ReadWrite);
            RealUtility.workBook.Close();
            RealUtility.excel.Quit();

            MessageBox.Show("Successfuly Exported as Excel File", "Success");

            }
            catch
            {
                MessageBox.Show("Please close all instances of excel and try again", "Error");
            }
        }
        private void SelectAddress(object sender, RoutedEventArgs e)

        {

            System.Windows.Controls.TextBox tb = (sender as System.Windows.Controls.TextBox);

            if (tb != null)

            {

                tb.SelectAll();

            }

        }



        private void SelectivelyIgnoreMouseButton(object sender,

            MouseButtonEventArgs e)

        {

            System.Windows.Controls.TextBox tb = (sender as System.Windows.Controls.TextBox);

            if (tb != null)

            {

                if (!tb.IsKeyboardFocusWithin)

                {

                    e.Handled = true;

                    tb.Focus();

                }

            }

        }

        private void Datestart_CalendarOpened(object sender, RoutedEventArgs e)
        {
            var datepicker = sender as DatePicker;
            if (datepicker != null)
            {
                var popup = datepicker.Template.FindName(
                    "PART_Popup", datepicker) as System.Windows.Controls.Primitives.Popup;
                if (popup != null && popup.Child is Calendar)
                {
                    ((Calendar)popup.Child).DisplayMode = CalendarMode.Year;
                }
            }
        }
    }
}

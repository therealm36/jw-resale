﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using basicPos2.models;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class ItemDiscount : Window
    {
         float _unitPrice;
         float _discount;
         float _quantity;
         SaleItem _product;
         string _note;
        public ItemDiscount(float unitPrice, float discount, float quantity, string note,SaleItem saleItem)
        {
            InitializeComponent();

            _unitPrice = unitPrice;
            _discount = discount;
            _quantity = quantity;
            _note = note;
            _product = saleItem;

            discountTxt.Text = string.Format("{0:N2}", _discount);
            unitPriceLbl.Content = string.Format("{0:N2}", _unitPrice);
            quantityTxt.Text = string.Format("{0:N2}", _quantity);
            noteTxt.Text = _note;



        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            int _index = SaleItem.CurrentBill.IndexOf(_product);
            SaleItem.CurrentBill[_index].ItemDiscount = float.Parse(discountTxt.Text);
            SaleItem.CurrentBill[_index].Comment = noteTxt.Text;
            SaleItem.CurrentBill[_index].Quantity = float.Parse(quantityTxt.Text);
            this.Close();


        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

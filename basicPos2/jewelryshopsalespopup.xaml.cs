﻿using basicPos2.models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for jewelryshopsalespopup.xaml
    /// </summary>
    public partial class jewelryshopsalespopup : Window
    {
        private ObservableCollection<Product> combogridpopulator=new ObservableCollection<Product>();
        public jewelryshopsalespopup(Jewelry_shop_sales populator)
        {
            InitializeComponent();
            comboaddproduct.ItemsSource = Product.AllProducts;
            comboproductdatagrid.ItemsSource = combogridpopulator;
            Date.SelectedDate = DateTime.Today;
            Time.SelectedTime = DateTime.Now;
            customercombo.ItemsSource = Customer.AllCustomers;
            if (populator != null)
            {
                Pricebox.Text = populator.Price + "";
                buttonn.Visibility = Visibility.Collapsed;
                try
                {
                    Date.SelectedDate = DateTime.ParseExact(populator.Date, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    Time.SelectedTime = Convert.ToDateTime(populator.Time);
                }
                catch (Exception e)
                {
                    Date.Text = populator.Date;
                }
                Jewelry_shop_sales.getComboProducts(populator.Id, combogridpopulator);
                MoneyGiven.Text = populator.MoneyGiven+"";
                GoldGiven.Text = populator.GoldGiven + "";

                if (Customer.AllCustomers.First(item => item.Id == populator.CustomerID) != null)
                {
                    customercombo.SelectedItem = Customer.AllCustomers.First(item => item.Id == populator.CustomerID);
                }
            }
        }
        private void comboaddproduct_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((Product)comboaddproduct.SelectedItem != null)
            {
                if (!combogridpopulator.Any(item => item.Code.Equals(((Product)comboaddproduct.SelectedItem).Code)))
                {


                    combogridpopulator.Add(new Product {Price= ((Product)comboaddproduct.SelectedItem).Price, Weight = -((Product)comboaddproduct.SelectedItem).Weight, Id = ((Product)comboaddproduct.SelectedItem).Id, Name = ((Product)comboaddproduct.SelectedItem).ToString(), Code = ((Product)comboaddproduct.SelectedItem).Code, Quantity = 1 });
                    Pricebox.Text = ""+combogridpopulator.Sum(item=>(item.Price*item.Quantity));
                    GoldGiven.Text = "" + combogridpopulator.Sum(item => (item.Weight * item.Quantity));

                }
            }
        }
        private void Quantity_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                try
                {
                    if (!(sender as TextBox).Text.ToString().Equals("") && (float.Parse((sender as TextBox).Text) != 0))
                    {

                        ((Product)comboproductdatagrid.SelectedItem).Quantity = float.Parse((sender as TextBox).Text);
                        Pricebox.Text = "" + combogridpopulator.Sum(item => (item.Price * item.Quantity));
                        GoldGiven.Text = "" + combogridpopulator.Sum(item => (item.Weight * item.Quantity));

                    }
                    else
                    {
                        MessageBox.Show("Please enter a valid number other than 0 for quantity");
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Please enter a valid number other than 0 for quantity");

                }
            }
            else
            {

            }
        }
        private void comboBoxn1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        public void refresh()
        {
            ((Product)comboproductdatagrid.SelectedItem).Price = Product.AllProducts.First(item => item.Id == ((Product)comboproductdatagrid.SelectedItem).Id).Price;
            ((Product)comboproductdatagrid.SelectedItem).Weight= Product.AllProducts.First(item => item.Id == ((Product)comboproductdatagrid.SelectedItem).Id).Weight;

            Pricebox.Text = "" + combogridpopulator.Sum(item => (item.Price * item.Quantity));
            GoldGiven.Text = "" + combogridpopulator.Sum(item => (item.Weight * item.Quantity));

        }
        private static popupwindow editProduts = null;
        private void edit_Click(object sender, RoutedEventArgs e)
        {
            if (editProduts == null || editProduts.IsLoaded == false)
            {
                editProduts = new popupwindow(this,null, Product.AllProducts.First(item => item.Id == ((Product)comboproductdatagrid.SelectedItem).Id), true);
                editProduts.Title = "Edit Product Condition";
                editProduts.Show();
                }
            else
            {
                addProduts.Activate();
            }
            
        }


        private void delete_Click(object sender, RoutedEventArgs e)
        {
            combogridpopulator.Remove((Product)comboproductdatagrid.SelectedItem);
            Pricebox.Text = "" + combogridpopulator.Sum(item => (item.Price * item.Quantity));
            GoldGiven.Text = "" + combogridpopulator.Sum(item => (item.Weight * item.Quantity));

        }

        private void buttonn_Click(object sender, RoutedEventArgs e)
        {
            customercombo.BorderBrush = Brushes.Black;
            Date.BorderBrush = Brushes.Black;
            Time.BorderBrush = Brushes.Black;

            if (customercombo.SelectedItem==null)
            {
                customercombo.BorderBrush = Brushes.Red;
                scroller.ScrollToTop();
                MessageBox.Show("Please select a Customer");

            }
            else
            {
                if (Time.SelectedTime == null)
                {
                    Time.BorderBrush = Brushes.Red;
                    scroller.ScrollToTop();
                    MessageBox.Show("Please select a valid Time");

                }
                else
                {
                    if (Date.SelectedDate == null)
                    {
                        Date.BorderBrush = Brushes.Red;
                        scroller.ScrollToTop();
                        MessageBox.Show("Please select a valid date");
                    }
                    else
                    {
                        int j = 0;
                        try
                        {
                            for (int i = 0; i < combogridpopulator.Count(); i++)
                            {
                                if ((combogridpopulator.ElementAt(i).Name.Length - 14 - combogridpopulator.ElementAt(i).Code.Length - 2 - 2) >= 0)
                                {
                                    if (!combogridpopulator.ElementAt(i).Name.Substring(combogridpopulator.ElementAt(i).Name.Length - 14 - combogridpopulator.ElementAt(i).Code.Length - 2 - 2, 14).Equals("(ONLY ORDERED)"))
                                    {

                                       
                                    }
                                    else
                                    {
                                        MessageBox.Show("The Item -->" + combogridpopulator.ElementAt(i).Name + "<-- is only ordered and doesn't exists in your inventory, you will have to purchase it from a goldsmith or change its status to (COMPLETED) instead of (ONLY ORDERED) if you wish to sell it", "ERROR");

                                        j = 1;
                                    }
                                }
                                else
                                {
                                    
                                }
                            }

                            if (j == 0)
                            {
                                Jewelry_shop_sales.addJWSale(new Jewelry_shop_sales { Price = float.Parse(Pricebox.Text), Date = Date.Text, Time = Time.Text, GoldGiven = float.Parse(GoldGiven.Text), MoneyGiven = float.Parse(MoneyGiven.Text), CustomerID = ((Customer)customercombo.SelectedItem).Id }, combogridpopulator);
                                this.Close();
                            }
                            else
                            {

                            }



                        }
                        catch (Exception er)
                        {
                            MessageBox.Show("Please enter number for gold given, money given and price");
                        }
                    }
                }
            }
        }
        private void comboaddproduct_KeyDown(object sender, KeyEventArgs e)
        {

        }
        private static CustomerWindow addProdutsPop = null;
        private void button5_Click(object sender, RoutedEventArgs e)
        {
            if (addProdutsPop == null || addProdutsPop.IsLoaded == false)
            {
                addProdutsPop = new CustomerWindow(null);
                addProdutsPop.Title = "Add Customer";
                addProdutsPop.Show();
            }
            else
            {
                addProdutsPop.Activate();
            }

        }
        private static popupwindow addProduts = null;
        private void addprod_Click(object sender, RoutedEventArgs e)
        {
            if (addProduts == null || addProduts.IsLoaded == false)
            {
                addProduts = new popupwindow(null,null, null, true);
                addProduts.Title = "Add Gold Smith";
                addProduts.Show();
            }
            else
            {
                addProduts.Activate();
            }

        }


    }
}

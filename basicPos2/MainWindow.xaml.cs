﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using basicPos2.models;
using MySql.Data.MySqlClient;
using Microsoft.Win32;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            
            InitializeComponent();



            jwreport.Visibility = Visibility.Hidden;
            gsreport.Visibility = Visibility.Hidden;
            productsUC.Visibility = Visibility.Hidden;
            CatagoriesUC.Visibility = Visibility.Hidden;
            SalesUC.Visibility = Visibility.Hidden;
            PurchasesUC.Visibility = Visibility.Hidden;
            ExpensesUC.Visibility = Visibility.Hidden;
            VendorsUC.Visibility = Visibility.Hidden;
            UsersUC.Visibility = Visibility.Hidden;
            CustomersUC.Visibility = Visibility.Hidden;
            DailySalesReport.Visibility = Visibility.Hidden;
            MonthlySalesReport.Visibility = Visibility.Hidden;
            DailyPurchaseReport.Visibility = Visibility.Hidden;
            MonthlyPurchaseReport.Visibility = Visibility.Hidden;
            DailyExpenseReport.Visibility = Visibility.Hidden;
            MonthlyExpenseReport.Visibility = Visibility.Hidden;
            productsReport.Visibility = Visibility.Hidden;

            jwss.Visibility = Visibility.Hidden;
            jwso.Visibility = Visibility.Hidden;
            gsso.Visibility = Visibility.Hidden;
            gssp.Visibility = Visibility.Hidden;

            //showOrHidePos();

        }

       

        private void posBtn_Click(object sender, RoutedEventArgs e)
        {



            jwreport.Visibility = Visibility.Hidden;
            gsreport.Visibility = Visibility.Hidden;
            jwss.Visibility = Visibility.Hidden;
            jwso.Visibility = Visibility.Hidden;
            gsso.Visibility = Visibility.Hidden;
            gssp.Visibility = Visibility.Hidden;
            productsUC.Visibility = Visibility.Hidden;
            CatagoriesUC.Visibility = Visibility.Hidden;
            SalesUC.Visibility = Visibility.Hidden;
            PurchasesUC.Visibility = Visibility.Hidden;
            ExpensesUC.Visibility = Visibility.Hidden;
            VendorsUC.Visibility = Visibility.Hidden;
            UsersUC.Visibility = Visibility.Hidden;
            CustomersUC.Visibility = Visibility.Hidden;
            DailySalesReport.Visibility = Visibility.Hidden;
            MonthlySalesReport.Visibility = Visibility.Hidden;
            DailyPurchaseReport.Visibility = Visibility.Hidden;
            MonthlyPurchaseReport.Visibility = Visibility.Hidden;
            DailyExpenseReport.Visibility = Visibility.Hidden;
            MonthlyExpenseReport.Visibility = Visibility.Hidden;
            productsReport.Visibility = Visibility.Hidden;




        }

        private void productsBtn_Click(object sender, RoutedEventArgs e)
        {


            jwreport.Visibility = Visibility.Hidden;
            gsreport.Visibility = Visibility.Hidden;
            jwss.Visibility = Visibility.Hidden;
            jwso.Visibility = Visibility.Hidden;
            gsso.Visibility = Visibility.Hidden;
            gssp.Visibility = Visibility.Hidden;
            productsUC.Visibility = Visibility.Visible;
            CatagoriesUC.Visibility = Visibility.Hidden;
            SalesUC.Visibility = Visibility.Hidden;
            PurchasesUC.Visibility = Visibility.Hidden;
            ExpensesUC.Visibility = Visibility.Hidden;
            VendorsUC.Visibility = Visibility.Hidden;
            UsersUC.Visibility = Visibility.Hidden;
            CustomersUC.Visibility = Visibility.Hidden;
            DailySalesReport.Visibility = Visibility.Hidden;
            MonthlySalesReport.Visibility = Visibility.Hidden;
            DailyPurchaseReport.Visibility = Visibility.Hidden;
            MonthlyPurchaseReport.Visibility = Visibility.Hidden;
            DailyExpenseReport.Visibility = Visibility.Hidden;
            MonthlyExpenseReport.Visibility = Visibility.Hidden;
            productsReport.Visibility = Visibility.Hidden;

        }

        private void CatagoriesBtn_Click(object sender, RoutedEventArgs e)
        {


            jwreport.Visibility = Visibility.Hidden;
            gsreport.Visibility = Visibility.Hidden;
            jwss.Visibility = Visibility.Hidden;
            jwso.Visibility = Visibility.Hidden;
            gsso.Visibility = Visibility.Hidden;
            gssp.Visibility = Visibility.Hidden;
            productsUC.Visibility = Visibility.Hidden;
            CatagoriesUC.Visibility = Visibility.Visible;
            SalesUC.Visibility = Visibility.Hidden;
            PurchasesUC.Visibility = Visibility.Hidden;
            ExpensesUC.Visibility = Visibility.Hidden;
            VendorsUC.Visibility = Visibility.Hidden;
            UsersUC.Visibility = Visibility.Hidden;
            CustomersUC.Visibility = Visibility.Hidden;
            DailySalesReport.Visibility = Visibility.Hidden;
            MonthlySalesReport.Visibility = Visibility.Hidden;
            DailyPurchaseReport.Visibility = Visibility.Hidden;
            MonthlyPurchaseReport.Visibility = Visibility.Hidden;
            DailyExpenseReport.Visibility = Visibility.Hidden;
            MonthlyExpenseReport.Visibility = Visibility.Hidden;
            productsReport.Visibility = Visibility.Hidden;
        }

        private void SalesBtnClick(object sender, RoutedEventArgs e)
        {


            jwreport.Visibility = Visibility.Hidden;
            gsreport.Visibility = Visibility.Hidden;
            jwss.Visibility = Visibility.Hidden;
            jwso.Visibility = Visibility.Hidden;
            gsso.Visibility = Visibility.Hidden;
            gssp.Visibility = Visibility.Hidden;
            productsUC.Visibility = Visibility.Hidden;
            CatagoriesUC.Visibility = Visibility.Hidden;
            SalesUC.Visibility = Visibility.Visible;
            PurchasesUC.Visibility = Visibility.Hidden;
            ExpensesUC.Visibility = Visibility.Hidden;
            VendorsUC.Visibility = Visibility.Hidden;
            UsersUC.Visibility = Visibility.Hidden;
            CustomersUC.Visibility = Visibility.Hidden;
            DailySalesReport.Visibility = Visibility.Hidden;
            MonthlySalesReport.Visibility = Visibility.Hidden;
            DailyPurchaseReport.Visibility = Visibility.Hidden;
            MonthlyPurchaseReport.Visibility = Visibility.Hidden;
            DailyExpenseReport.Visibility = Visibility.Hidden;
            MonthlyExpenseReport.Visibility = Visibility.Hidden;
            productsReport.Visibility = Visibility.Hidden;

        }

        private void PurchasesBtn_Click(object sender, RoutedEventArgs e)
        {



            jwreport.Visibility = Visibility.Hidden;
            gsreport.Visibility = Visibility.Hidden;
            jwss.Visibility = Visibility.Hidden;
            jwso.Visibility = Visibility.Hidden;
            gsso.Visibility = Visibility.Hidden;
            gssp.Visibility = Visibility.Hidden;
            productsUC.Visibility = Visibility.Hidden;
            CatagoriesUC.Visibility = Visibility.Hidden;
            SalesUC.Visibility = Visibility.Hidden;
            PurchasesUC.Visibility = Visibility.Visible;
            ExpensesUC.Visibility = Visibility.Hidden;
            VendorsUC.Visibility = Visibility.Hidden;
            UsersUC.Visibility = Visibility.Hidden;
            CustomersUC.Visibility = Visibility.Hidden;
            DailySalesReport.Visibility = Visibility.Hidden;
            MonthlySalesReport.Visibility = Visibility.Hidden;
            DailyPurchaseReport.Visibility = Visibility.Hidden;
            MonthlyPurchaseReport.Visibility = Visibility.Hidden;
            DailyExpenseReport.Visibility = Visibility.Hidden;
            MonthlyExpenseReport.Visibility = Visibility.Hidden;
            productsReport.Visibility = Visibility.Hidden;

        }

        private void ExpensesBtn_Click(object sender, RoutedEventArgs e)
        {


            jwreport.Visibility = Visibility.Hidden;
            gsreport.Visibility = Visibility.Hidden;
            jwss.Visibility = Visibility.Hidden;
            jwso.Visibility = Visibility.Hidden;
            gsso.Visibility = Visibility.Hidden;
            gssp.Visibility = Visibility.Hidden;
            productsUC.Visibility = Visibility.Hidden;
            CatagoriesUC.Visibility = Visibility.Hidden;
            SalesUC.Visibility = Visibility.Hidden;
            PurchasesUC.Visibility = Visibility.Hidden;
            ExpensesUC.Visibility = Visibility.Visible;
            VendorsUC.Visibility = Visibility.Hidden;
            UsersUC.Visibility = Visibility.Hidden;
            CustomersUC.Visibility = Visibility.Hidden;
            DailySalesReport.Visibility = Visibility.Hidden;
            MonthlySalesReport.Visibility = Visibility.Hidden;
            DailyPurchaseReport.Visibility = Visibility.Hidden;
            MonthlyPurchaseReport.Visibility = Visibility.Hidden;
            DailyExpenseReport.Visibility = Visibility.Hidden;
            MonthlyExpenseReport.Visibility = Visibility.Hidden;
            productsReport.Visibility = Visibility.Hidden;

        }

        private void CustomersBtn_Click(object sender, RoutedEventArgs e)
        {



            jwreport.Visibility = Visibility.Hidden;
            gsreport.Visibility = Visibility.Hidden;
            jwss.Visibility = Visibility.Hidden;
            jwso.Visibility = Visibility.Hidden;
            gsso.Visibility = Visibility.Hidden;
            gssp.Visibility = Visibility.Hidden;
            productsUC.Visibility = Visibility.Hidden;
            CatagoriesUC.Visibility = Visibility.Hidden;
            SalesUC.Visibility = Visibility.Hidden;
            PurchasesUC.Visibility = Visibility.Hidden;
            ExpensesUC.Visibility = Visibility.Hidden;
            VendorsUC.Visibility = Visibility.Hidden;
            UsersUC.Visibility = Visibility.Hidden;
            CustomersUC.Visibility = Visibility.Visible;
            DailySalesReport.Visibility = Visibility.Hidden;
            MonthlySalesReport.Visibility = Visibility.Hidden;
            DailyPurchaseReport.Visibility = Visibility.Hidden;
            MonthlyPurchaseReport.Visibility = Visibility.Hidden;
            DailyExpenseReport.Visibility = Visibility.Hidden;
            MonthlyExpenseReport.Visibility = Visibility.Hidden;
            productsReport.Visibility = Visibility.Hidden;
        }

        private void VendorsBtn_Click(object sender, RoutedEventArgs e)
        {

            jwreport.Visibility = Visibility.Hidden;
            gsreport.Visibility = Visibility.Hidden;
            jwss.Visibility = Visibility.Hidden;
            jwso.Visibility = Visibility.Hidden;
            gsso.Visibility = Visibility.Hidden;
            gssp.Visibility = Visibility.Visible;
            productsUC.Visibility = Visibility.Hidden;
            CatagoriesUC.Visibility = Visibility.Hidden;
            SalesUC.Visibility = Visibility.Hidden;
            PurchasesUC.Visibility = Visibility.Hidden;
            ExpensesUC.Visibility = Visibility.Hidden;
            VendorsUC.Visibility = Visibility.Hidden;
            UsersUC.Visibility = Visibility.Hidden;
            CustomersUC.Visibility = Visibility.Hidden;
            DailySalesReport.Visibility = Visibility.Hidden;
            MonthlySalesReport.Visibility = Visibility.Hidden;
            DailyPurchaseReport.Visibility = Visibility.Hidden;
            MonthlyPurchaseReport.Visibility = Visibility.Hidden;
            DailyExpenseReport.Visibility = Visibility.Hidden;
            MonthlyExpenseReport.Visibility = Visibility.Hidden;
            productsReport.Visibility = Visibility.Hidden;
        }

        private void UsersBtn_Click(object sender, RoutedEventArgs e)
        {


            jwreport.Visibility = Visibility.Hidden;
            gsreport.Visibility = Visibility.Hidden;
            jwss.Visibility = Visibility.Hidden;
            jwso.Visibility = Visibility.Hidden;
            gsso.Visibility = Visibility.Visible;
            gssp.Visibility = Visibility.Hidden;
            productsUC.Visibility = Visibility.Hidden;
            CatagoriesUC.Visibility = Visibility.Hidden;
            SalesUC.Visibility = Visibility.Hidden;
            PurchasesUC.Visibility = Visibility.Hidden;
            ExpensesUC.Visibility = Visibility.Hidden;
            VendorsUC.Visibility = Visibility.Hidden;
            UsersUC.Visibility = Visibility.Hidden;
            CustomersUC.Visibility = Visibility.Hidden;
            DailySalesReport.Visibility = Visibility.Hidden;
            MonthlySalesReport.Visibility = Visibility.Hidden;
            DailyPurchaseReport.Visibility = Visibility.Hidden;
            MonthlyPurchaseReport.Visibility = Visibility.Hidden;
            DailyExpenseReport.Visibility = Visibility.Hidden;
            MonthlyExpenseReport.Visibility = Visibility.Hidden;
            productsReport.Visibility = Visibility.Hidden;
        }

        private void dailySalesBtn_Click(object sender, RoutedEventArgs e)
        {


            jwreport.Visibility = Visibility.Hidden;
            gsreport.Visibility = Visibility.Hidden;
            jwss.Visibility = Visibility.Hidden;
            jwso.Visibility = Visibility.Hidden;
            gsso.Visibility = Visibility.Hidden;
            gssp.Visibility = Visibility.Hidden;
            productsUC.Visibility = Visibility.Hidden;
            CatagoriesUC.Visibility = Visibility.Hidden;
            SalesUC.Visibility = Visibility.Hidden;
            PurchasesUC.Visibility = Visibility.Hidden;
            ExpensesUC.Visibility = Visibility.Hidden;
            VendorsUC.Visibility = Visibility.Hidden;
            UsersUC.Visibility = Visibility.Hidden;
            CustomersUC.Visibility = Visibility.Hidden;
            DailySalesReport.Visibility = Visibility.Visible;
            MonthlySalesReport.Visibility = Visibility.Hidden;
            DailyPurchaseReport.Visibility = Visibility.Hidden;
            MonthlyPurchaseReport.Visibility = Visibility.Hidden;
            DailyExpenseReport.Visibility = Visibility.Hidden;
            MonthlyExpenseReport.Visibility = Visibility.Hidden;
            productsReport.Visibility = Visibility.Hidden;

        }

        private void monthlySalesBtn_Click(object sender, RoutedEventArgs e)
        {


            jwreport.Visibility = Visibility.Hidden;
            gsreport.Visibility = Visibility.Hidden;
            jwss.Visibility = Visibility.Hidden;
            jwso.Visibility = Visibility.Hidden;
            gsso.Visibility = Visibility.Hidden;
            gssp.Visibility = Visibility.Hidden;
            productsUC.Visibility = Visibility.Hidden;
            CatagoriesUC.Visibility = Visibility.Hidden;
            SalesUC.Visibility = Visibility.Hidden;
            PurchasesUC.Visibility = Visibility.Hidden;
            ExpensesUC.Visibility = Visibility.Hidden;
            VendorsUC.Visibility = Visibility.Hidden;
            UsersUC.Visibility = Visibility.Hidden;
            CustomersUC.Visibility = Visibility.Hidden;
            DailySalesReport.Visibility = Visibility.Hidden;
            MonthlySalesReport.Visibility = Visibility.Visible;
            DailyPurchaseReport.Visibility = Visibility.Hidden;
            MonthlyPurchaseReport.Visibility = Visibility.Hidden;
            DailyExpenseReport.Visibility = Visibility.Hidden;
            MonthlyExpenseReport.Visibility = Visibility.Hidden;
            productsReport.Visibility = Visibility.Hidden;
        }

        private void dailyPurchaseBtn_Click(object sender, RoutedEventArgs e)
        {


            jwreport.Visibility = Visibility.Hidden;
            gsreport.Visibility = Visibility.Hidden;
            jwss.Visibility = Visibility.Hidden;
            jwso.Visibility = Visibility.Hidden;
            gsso.Visibility = Visibility.Hidden;
            gssp.Visibility = Visibility.Hidden;
            productsUC.Visibility = Visibility.Hidden;
            CatagoriesUC.Visibility = Visibility.Hidden;
            SalesUC.Visibility = Visibility.Hidden;
            PurchasesUC.Visibility = Visibility.Hidden;
            ExpensesUC.Visibility = Visibility.Hidden;
            VendorsUC.Visibility = Visibility.Hidden;
            UsersUC.Visibility = Visibility.Hidden;
            CustomersUC.Visibility = Visibility.Hidden;
            DailySalesReport.Visibility = Visibility.Hidden;
            MonthlySalesReport.Visibility = Visibility.Hidden;
            DailyPurchaseReport.Visibility = Visibility.Visible;
            MonthlyPurchaseReport.Visibility = Visibility.Hidden;
            DailyExpenseReport.Visibility = Visibility.Hidden;
            MonthlyExpenseReport.Visibility = Visibility.Hidden;
            productsReport.Visibility = Visibility.Hidden;
        }

        private void logoutBtn_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are youn sure you want to sighn Out?", "Sighn Out", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
            {
               
            }
            else
            {
                RealUtility.currentUserId = 0;
                RealUtility.userLevel = -1;
                new Loginxaml().Show();
                this.Close();
                
            }


            
        }

        private void registerDetailBtn_Click(object sender, RoutedEventArgs e)
        {
            new RegisterDetailWindow().ShowDialog();
        }

        private void closeRegisterBtn_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are youn sure you want to close the register", "Closing Register", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
            {

            }
            else
            {
                Register.closeRegister();
      

            }
        }

        private void produtsReport_Click(object sender, RoutedEventArgs e)
        {


            jwreport.Visibility = Visibility.Hidden;
            gsreport.Visibility = Visibility.Hidden;
            jwss.Visibility = Visibility.Hidden;
            jwso.Visibility = Visibility.Hidden;
            gsso.Visibility = Visibility.Hidden;
            gssp.Visibility = Visibility.Hidden;
            productsUC.Visibility = Visibility.Hidden;
            CatagoriesUC.Visibility = Visibility.Hidden;
            SalesUC.Visibility = Visibility.Hidden;
            PurchasesUC.Visibility = Visibility.Hidden;
            ExpensesUC.Visibility = Visibility.Hidden;
            VendorsUC.Visibility = Visibility.Hidden;
            UsersUC.Visibility = Visibility.Hidden;
            CustomersUC.Visibility = Visibility.Hidden;
            DailySalesReport.Visibility = Visibility.Hidden;
            MonthlySalesReport.Visibility = Visibility.Hidden;
            DailyPurchaseReport.Visibility = Visibility.Hidden;
            MonthlyPurchaseReport.Visibility = Visibility.Hidden;
            DailyExpenseReport.Visibility = Visibility.Hidden;
            MonthlyExpenseReport.Visibility = Visibility.Hidden;
            productsReport.Visibility = Visibility.Visible;
        }

        private void monthlyExpense_Click(object sender, RoutedEventArgs e)
        {


            jwreport.Visibility = Visibility.Hidden;
            gsreport.Visibility = Visibility.Hidden;
            jwss.Visibility = Visibility.Hidden;
            jwso.Visibility = Visibility.Hidden;
            gsso.Visibility = Visibility.Hidden;
            gssp.Visibility = Visibility.Hidden;
            productsUC.Visibility = Visibility.Hidden;
            CatagoriesUC.Visibility = Visibility.Hidden;
            SalesUC.Visibility = Visibility.Hidden;
            PurchasesUC.Visibility = Visibility.Hidden;
            ExpensesUC.Visibility = Visibility.Hidden;
            VendorsUC.Visibility = Visibility.Hidden;
            UsersUC.Visibility = Visibility.Hidden;
            CustomersUC.Visibility = Visibility.Hidden;
            DailySalesReport.Visibility = Visibility.Hidden;
            MonthlySalesReport.Visibility = Visibility.Hidden;
            DailyPurchaseReport.Visibility = Visibility.Hidden;
            MonthlyPurchaseReport.Visibility = Visibility.Hidden;
            DailyExpenseReport.Visibility = Visibility.Hidden;
            MonthlyExpenseReport.Visibility = Visibility.Visible;
            productsReport.Visibility = Visibility.Hidden;
        }

        private void dailyExpense_Click(object sender, RoutedEventArgs e)
        {


            jwreport.Visibility = Visibility.Hidden;
            gsreport.Visibility = Visibility.Hidden;
            jwss.Visibility = Visibility.Hidden;
            jwso.Visibility = Visibility.Hidden;
            gsso.Visibility = Visibility.Hidden;
            gssp.Visibility = Visibility.Hidden;
            productsUC.Visibility = Visibility.Hidden;
            CatagoriesUC.Visibility = Visibility.Hidden;
            SalesUC.Visibility = Visibility.Hidden;
            PurchasesUC.Visibility = Visibility.Hidden;
            ExpensesUC.Visibility = Visibility.Hidden;
            VendorsUC.Visibility = Visibility.Hidden;
            UsersUC.Visibility = Visibility.Hidden;
            CustomersUC.Visibility = Visibility.Hidden;
            DailySalesReport.Visibility = Visibility.Hidden;
            MonthlySalesReport.Visibility = Visibility.Hidden;
            DailyPurchaseReport.Visibility = Visibility.Hidden;
            MonthlyPurchaseReport.Visibility = Visibility.Hidden;
            DailyExpenseReport.Visibility = Visibility.Visible;
            MonthlyExpenseReport.Visibility = Visibility.Hidden;
            productsReport.Visibility = Visibility.Hidden;
        }

        private void monthlyPurchase_Click(object sender, RoutedEventArgs e)
        {


            jwreport.Visibility = Visibility.Hidden;
            gsreport.Visibility = Visibility.Hidden;
            jwss.Visibility = Visibility.Hidden;
            jwso.Visibility = Visibility.Hidden;
            gsso.Visibility = Visibility.Hidden;
            gssp.Visibility = Visibility.Hidden;
            productsUC.Visibility = Visibility.Hidden;
            CatagoriesUC.Visibility = Visibility.Hidden;
            SalesUC.Visibility = Visibility.Hidden;
            PurchasesUC.Visibility = Visibility.Hidden;
            ExpensesUC.Visibility = Visibility.Hidden;
            VendorsUC.Visibility = Visibility.Hidden;
            UsersUC.Visibility = Visibility.Hidden;
            CustomersUC.Visibility = Visibility.Hidden;
            DailySalesReport.Visibility = Visibility.Hidden;
            MonthlySalesReport.Visibility = Visibility.Hidden;
            DailyPurchaseReport.Visibility = Visibility.Hidden;
            MonthlyPurchaseReport.Visibility = Visibility.Visible;
            DailyExpenseReport.Visibility = Visibility.Hidden;
            MonthlyExpenseReport.Visibility = Visibility.Hidden;
            productsReport.Visibility = Visibility.Hidden;
        }

        private void Timemachine_Click(object sender, RoutedEventArgs e)
        {
            (sender as Button).ContextMenu.IsEnabled = true;
            (sender as Button).ContextMenu.PlacementTarget = (sender as Button);
            (sender as Button).ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            (sender as Button).ContextMenu.IsOpen = true;
        }
        private void restore_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openfile = new OpenFileDialog();
            openfile.DefaultExt = ".sql";
            openfile.Filter = "(.sql)|*.sql";
            //openfile.ShowDialog();

            var browsefile = openfile.ShowDialog();

            if (browsefile == true)
            {try
                {
                    string file = openfile.FileName;
                    using (MySqlConnection conn = new MySqlConnection(RealUtility.dbString))
                    {
                        using (MySqlCommand cmd = new MySqlCommand())
                        {
                            using (MySqlBackup mb = new MySqlBackup(cmd))
                            {
                                cmd.Connection = conn;
                                conn.Open();
                                mb.ImportFromFile(file);
                                conn.Close();
                            }
                        }
                    }
                    MessageBox.Show("Successfully Restored","Success");
                }
                catch
                {
                    MessageBox.Show("Failed to restore", "Error");
                }
            }
        }
        private void backup_Click(object sender, RoutedEventArgs e)
        {
            try {
                string file = RealUtility.pathfornarnia + "backup.sql";
                using (MySqlConnection conn = new MySqlConnection(RealUtility.dbString))
                {
                    using (MySqlCommand cmd = new MySqlCommand())
                    {
                        using (MySqlBackup mb = new MySqlBackup(cmd))
                        {
                            cmd.Connection = conn;
                            conn.Open();
                            mb.ExportToFile(file);
                            conn.Close();
                        }
                    }
                }
                MessageBox.Show("Backup Success", "Success");
            }
            catch
            {
                MessageBox.Show("Backup Failed", "Error");
            }
            }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Support support = new Support();
            support.ShowDialog();
        }

        private void priceBtn_Click(object sender, RoutedEventArgs e)
        {
            PerGramPriceXaml updatePrice = new PerGramPriceXaml();
            updatePrice.ShowDialog();
        }

        private void jwsaleBtn_Click(object sender, RoutedEventArgs e)
        {


            jwreport.Visibility = Visibility.Hidden;
            gsreport.Visibility = Visibility.Hidden;
            jwss.Visibility = Visibility.Visible;
            jwso.Visibility = Visibility.Hidden;
            gsso.Visibility = Visibility.Hidden;
            gssp.Visibility = Visibility.Hidden;
            productsUC.Visibility = Visibility.Hidden;
            CatagoriesUC.Visibility = Visibility.Hidden;
            SalesUC.Visibility = Visibility.Hidden;
            PurchasesUC.Visibility = Visibility.Hidden;
            ExpensesUC.Visibility = Visibility.Hidden;
            VendorsUC.Visibility = Visibility.Hidden;
            UsersUC.Visibility = Visibility.Hidden;
            CustomersUC.Visibility = Visibility.Hidden;
            DailySalesReport.Visibility = Visibility.Hidden;
            MonthlySalesReport.Visibility = Visibility.Hidden;
            DailyPurchaseReport.Visibility = Visibility.Hidden;
            MonthlyPurchaseReport.Visibility = Visibility.Hidden;
            DailyExpenseReport.Visibility = Visibility.Hidden;
            MonthlyExpenseReport.Visibility = Visibility.Hidden;
            productsReport.Visibility = Visibility.Hidden;
        }

        private void jwordersBtn_Click(object sender, RoutedEventArgs e)
        {


            jwreport.Visibility = Visibility.Hidden;
            gsreport.Visibility = Visibility.Hidden;
            jwss.Visibility = Visibility.Hidden;
            jwso.Visibility = Visibility.Visible;
            gsso.Visibility = Visibility.Hidden;
            gssp.Visibility = Visibility.Hidden;
            productsUC.Visibility = Visibility.Hidden;
            CatagoriesUC.Visibility = Visibility.Hidden;
            SalesUC.Visibility = Visibility.Hidden;
            PurchasesUC.Visibility = Visibility.Hidden;
            ExpensesUC.Visibility = Visibility.Hidden;
            VendorsUC.Visibility = Visibility.Hidden;
            UsersUC.Visibility = Visibility.Hidden;
            CustomersUC.Visibility = Visibility.Hidden;
            DailySalesReport.Visibility = Visibility.Hidden;
            MonthlySalesReport.Visibility = Visibility.Hidden;
            DailyPurchaseReport.Visibility = Visibility.Hidden;
            MonthlyPurchaseReport.Visibility = Visibility.Hidden;
            DailyExpenseReport.Visibility = Visibility.Hidden;
            MonthlyExpenseReport.Visibility = Visibility.Hidden;
            productsReport.Visibility = Visibility.Hidden;
        }

        private void goldBtn_Click(object sender, RoutedEventArgs e)
        {


            jwreport.Visibility = Visibility.Hidden;
            gsreport.Visibility = Visibility.Hidden;
            jwss.Visibility = Visibility.Hidden;
            jwso.Visibility = Visibility.Hidden;
            gsso.Visibility = Visibility.Hidden;
            gssp.Visibility = Visibility.Hidden;
            productsUC.Visibility = Visibility.Hidden;
            CatagoriesUC.Visibility = Visibility.Hidden;
            SalesUC.Visibility = Visibility.Hidden;
            PurchasesUC.Visibility = Visibility.Hidden;
            ExpensesUC.Visibility = Visibility.Hidden;
            VendorsUC.Visibility = Visibility.Visible;
            UsersUC.Visibility = Visibility.Hidden;
            CustomersUC.Visibility = Visibility.Hidden;
            DailySalesReport.Visibility = Visibility.Hidden;
            MonthlySalesReport.Visibility = Visibility.Hidden;
            DailyPurchaseReport.Visibility = Visibility.Hidden;
            MonthlyPurchaseReport.Visibility = Visibility.Hidden;
            DailyExpenseReport.Visibility = Visibility.Hidden;
            MonthlyExpenseReport.Visibility = Visibility.Hidden;
            productsReport.Visibility = Visibility.Hidden;
        }

        private void gsreportBtn_Click(object sender, RoutedEventArgs e)
        {
            jwreport.Visibility = Visibility.Hidden;
            gsreport.Visibility = Visibility.Visible;

            jwss.Visibility = Visibility.Hidden;
            jwso.Visibility = Visibility.Hidden;
            gsso.Visibility = Visibility.Hidden;
            gssp.Visibility = Visibility.Hidden;
            productsUC.Visibility = Visibility.Hidden;
            CatagoriesUC.Visibility = Visibility.Hidden;
            SalesUC.Visibility = Visibility.Hidden;
            PurchasesUC.Visibility = Visibility.Hidden;
            ExpensesUC.Visibility = Visibility.Hidden;
            VendorsUC.Visibility = Visibility.Hidden;
            UsersUC.Visibility = Visibility.Hidden;
            CustomersUC.Visibility = Visibility.Hidden;
            DailySalesReport.Visibility = Visibility.Hidden;
            MonthlySalesReport.Visibility = Visibility.Hidden;
            DailyPurchaseReport.Visibility = Visibility.Hidden;
            MonthlyPurchaseReport.Visibility = Visibility.Hidden;
            DailyExpenseReport.Visibility = Visibility.Hidden;
            MonthlyExpenseReport.Visibility = Visibility.Hidden;
            productsReport.Visibility = Visibility.Hidden;
        }

        private void jwreportBtn_Click(object sender, RoutedEventArgs e)
        {
            jwreport.Visibility = Visibility.Visible;
            gsreport.Visibility = Visibility.Hidden;

            jwss.Visibility = Visibility.Hidden;
            jwso.Visibility = Visibility.Hidden;
            gsso.Visibility = Visibility.Hidden;
            gssp.Visibility = Visibility.Hidden;
            productsUC.Visibility = Visibility.Hidden;
            CatagoriesUC.Visibility = Visibility.Hidden;
            SalesUC.Visibility = Visibility.Hidden;
            PurchasesUC.Visibility = Visibility.Hidden;
            ExpensesUC.Visibility = Visibility.Hidden;
            VendorsUC.Visibility = Visibility.Hidden;
            UsersUC.Visibility = Visibility.Hidden;
            CustomersUC.Visibility = Visibility.Hidden;
            DailySalesReport.Visibility = Visibility.Hidden;
            MonthlySalesReport.Visibility = Visibility.Hidden;
            DailyPurchaseReport.Visibility = Visibility.Hidden;
            MonthlyPurchaseReport.Visibility = Visibility.Hidden;
            DailyExpenseReport.Visibility = Visibility.Hidden;
            MonthlyExpenseReport.Visibility = Visibility.Hidden;
            productsReport.Visibility = Visibility.Hidden;
        }
    }

}

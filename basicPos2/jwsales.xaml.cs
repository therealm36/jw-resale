﻿using basicPos2.models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for Vendors.xaml
    /// </summary>
    public partial class jwsales : UserControl
    {

        public jwsales()
        {
            InitializeComponent();
            produtsdataGrid.ItemsSource = Jewelry_shop_sales.AllJewelrySales;


        }

        private void button_Click(object sender, RoutedEventArgs e)
        {

        }



        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void dataGrid_Loaded(object sender, RoutedEventArgs e)
        {


        }


        private void deletebtn_Click(object sender, RoutedEventArgs e)
        {

            Jewelry_shop_sales.DeleteFromAllJWSales((Jewelry_shop_sales)produtsdataGrid.SelectedItem);
        }

        private static AddPayment addpay = null;
        private void addpaybtn_Click(object sender, RoutedEventArgs e)
        {
            if (addpay == null || addpay.IsLoaded == false)
            {
                addpay = new AddPayment((Jewelry_shop_sales)produtsdataGrid.SelectedItem,null,null,null,null,null);
                addpay.Title = "Add Payment";
                addpay.Show();
            }
            else
            {
                addpay.Activate();
            }

        }

        private static viewPaymentsjwlrysales viewpay = null;
        private void viewpaybtn_Click(object sender, RoutedEventArgs e)
        {
            if (viewpay == null || viewpay.IsLoaded == false)
            {
                viewpay = new viewPaymentsjwlrysales(null,(Jewelry_shop_sales)produtsdataGrid.SelectedItem);
                viewpay.Title = "View Payment";
                viewpay.Show();
            }
            else
            {
                viewpay.Activate();
            }

        }


        private static jewelryshopsalespopup viewpop = null;
        private void editbtn_Click(object sender, RoutedEventArgs e)
        {
            if (viewpop == null || viewpop.IsLoaded == false)
            {
                viewpop = new jewelryshopsalespopup((Jewelry_shop_sales)produtsdataGrid.SelectedItem);
                viewpop.Title = "View Sale";
                viewpop.Show();
            }
            else
            {
                viewpop.Activate();
            }

        }
        private static jewelryshopsalespopup addjwsalesPop = null;
        private void button5_Click(object sender, RoutedEventArgs e)
        {
            if (addjwsalesPop == null || addjwsalesPop.IsLoaded == false)
            {
                addjwsalesPop = new jewelryshopsalespopup(null);
                addjwsalesPop.Title = "Add Sale";
                addjwsalesPop.Show();
            }
            else
            {
                addjwsalesPop.Activate();
            }

        }
        private void button3_Click(object sender, RoutedEventArgs e)
        {
            RealUtility.ExportToPdfjwsales(produtsdataGrid);
        }




      /*  private void button2_Click(object sender, RoutedEventArgs e)
        {
            RealUtility.Exporttocsvsupliers(produtsdataGrid);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");
                RealUtility.GenerateExcelSuppliers(Supplier.AllSupliers.ToDataTable());
                RealUtility.workBook.SaveAs(RealUtility.pathfornarnia + "SuppliersList--" + RealUtility.todaydate + ".xls", System.IO.FileMode.Create, FileAccess.ReadWrite);
                RealUtility.workBook.Close();
                RealUtility.excel.Quit();
                0
                MessageBox.Show("Successfuly Exported as Excel File", "Success");
            }
            catch
            {
                MessageBox.Show("Please close all instances of excel and try again", "Error");
            }

        }

        */

        private void button6_Click(object sender, RoutedEventArgs e)
        {
            // RealUtility.ImportFromExcel();
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {


            (sender as System.Windows.Controls.Button).ContextMenu.IsEnabled = true;
            (sender as System.Windows.Controls.Button).ContextMenu.PlacementTarget = (sender as System.Windows.Controls.Button);
            (sender as System.Windows.Controls.Button).ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            (sender as System.Windows.Controls.Button).ContextMenu.IsOpen = true;
        }

        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {

            IEnumerable<Jewelry_shop_sales> backup = new ObservableCollection<Jewelry_shop_sales>((IEnumerable<Jewelry_shop_sales>)produtsdataGrid.ItemsSource);

            if (e.Key == Key.Return)
            {
                if (!textBox.Text.ToString().Equals(""))
                {

                    IEnumerable<Jewelry_shop_sales> temp = new ObservableCollection<Jewelry_shop_sales>();
                    temp = ((IEnumerable<Jewelry_shop_sales>)produtsdataGrid.ItemsSource).Where(item => item.Date.ToLower().Contains(textBox.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                }
                else
                {
                    if (textBox.Text.Equals("") && textBox_Copy1.Text.Equals("[Price]") && textBox_Copy3.Text.Equals("[Gold Given]") && createdby_Copy3.Text.Equals("[Money Given]") && textemail.Text.Equals("[Customer Name]"))
                    {
                        textBox.Text = "[Date]";
                        produtsdataGrid.ItemsSource = Jewelry_shop_sales.AllJewelrySales;
                        produtsdataGrid.Items.Refresh();
                    }
                    else
                    {
                        textBox.Text = "[Date]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();
                    }
                }
            }
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void textBox_MouseUp(object sender, MouseButtonEventArgs e)
        {

        }
        private void SelectAddress(object sender, RoutedEventArgs e)

        {

            System.Windows.Controls.TextBox tb = (sender as System.Windows.Controls.TextBox);

            if (tb != null)

            {

                tb.SelectAll();

            }

        }



        private void SelectivelyIgnoreMouseButton(object sender,

            MouseButtonEventArgs e)

        {

            System.Windows.Controls.TextBox tb = (sender as System.Windows.Controls.TextBox);

            if (tb != null)

            {

                if (!tb.IsKeyboardFocusWithin)

                {

                    e.Handled = true;

                    tb.Focus();

                }

            }

        }

        private void textBox_Copy_MouseUp(object sender, MouseButtonEventArgs e)
        {

        }


        private void textBox_Copy1_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void textBox_Copy1_KeyDown(object sender, KeyEventArgs e)
        {
            IEnumerable<Jewelry_shop_sales> backup = new ObservableCollection<Jewelry_shop_sales>((IEnumerable<Jewelry_shop_sales>)produtsdataGrid.ItemsSource);

            if (e.Key == Key.Return)
            {
                if (!textBox_Copy1.Text.ToString().Equals(""))
                {

                    IEnumerable<Jewelry_shop_sales> temp = new ObservableCollection<Jewelry_shop_sales>();
                    temp = ((IEnumerable<Jewelry_shop_sales>)produtsdataGrid.ItemsSource).Where(item => item.Price.ToString().ToLower().Contains(textBox_Copy1.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                }
                else
                {
                    if (textBox.Text.Equals("[Date]") && textBox_Copy1.Text.Equals("") && textBox_Copy3.Text.Equals("[Gold Given]") && createdby_Copy3.Text.Equals("[Money Given]") && textemail.Text.Equals("[Customer Name]"))
                    {
                        textBox_Copy1.Text = "[Price]";
                        produtsdataGrid.ItemsSource = Jewelry_shop_sales.AllJewelrySales;
                        produtsdataGrid.Items.Refresh();
                    }
                    else
                    {
                        textBox_Copy1.Text = "[Price]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();
                    }
                }
            }
        }



        private void textBox_Copy3_TextChanged(object sender, TextChangedEventArgs e)
        {


        }

        private void textBox_Copy3_KeyDown(object sender, KeyEventArgs e)
        {
            IEnumerable<Jewelry_shop_sales> backup = new ObservableCollection<Jewelry_shop_sales>((IEnumerable<Jewelry_shop_sales>)produtsdataGrid.ItemsSource);

            if (e.Key == Key.Return)
            {
                if (!textBox_Copy3.Text.ToString().Equals(""))
                {

                    IEnumerable<Jewelry_shop_sales> temp = new ObservableCollection<Jewelry_shop_sales>();
                    temp = ((IEnumerable<Jewelry_shop_sales>)produtsdataGrid.ItemsSource).Where(item => item.GoldGiven.ToString().ToLower().Contains(textBox_Copy3.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                }
                else
                {
                    if (textBox.Text.Equals("[Date]") && textBox_Copy1.Text.Equals("[Price]") && textBox_Copy3.Text.Equals("") && createdby_Copy3.Text.Equals("[Money Given]") && textemail.Text.Equals("[Customer Name]"))
                    {
                        textBox_Copy3.Text = "[Gold Given]";
                        produtsdataGrid.ItemsSource = Jewelry_shop_sales.AllJewelrySales;
                        produtsdataGrid.Items.Refresh();
                    }
                    else
                    {
                        textBox_Copy3.Text = "[Gold Given]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();
                    }
                }
            }
        }

        private void textBox_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox.Text = "[Code]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void textBox_Copy_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy.Text = "[Name]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void textBox_Copy1_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy1.Text = "[Type]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void textBox_Copy2_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy2.Text = "[Catagory]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void textBox_Copy3_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy3.Text = "[Quantity]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void createdby_KeyDown(object sender, KeyEventArgs e)
        {
            IEnumerable<Jewelry_shop_sales> backup = new ObservableCollection<Jewelry_shop_sales>((IEnumerable<Jewelry_shop_sales>)produtsdataGrid.ItemsSource);

            if (e.Key == Key.Return)
            {
                if (!createdby_Copy3.Text.ToString().Equals(""))
                {

                    IEnumerable<Jewelry_shop_sales> temp = new ObservableCollection<Jewelry_shop_sales>();
                    temp = ((IEnumerable<Jewelry_shop_sales>)produtsdataGrid.ItemsSource).Where(item => item.MoneyGiven.ToString().ToLower().Contains(createdby_Copy3.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                }
                else
                {
                    if (textBox.Text.Equals("[Date]") && textBox_Copy1.Text.Equals("[Price]") && textBox_Copy3.Text.Equals("[Gold Given]") && createdby_Copy3.Text.Equals("") && textemail.Text.Equals("[Customer Name]"))
                    {
                        createdby_Copy3.Text = "[Money Given]";
                        produtsdataGrid.ItemsSource = Jewelry_shop_sales.AllJewelrySales;
                        produtsdataGrid.Items.Refresh();
                    }
                    else
                    {
                        createdby_Copy3.Text = "[Money Given]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();
                    }
                }
            }
        }

        private void textemail_KeyDown(object sender, KeyEventArgs e)
        {
            IEnumerable<Jewelry_shop_sales> backup = new ObservableCollection<Jewelry_shop_sales>((IEnumerable<Jewelry_shop_sales>)produtsdataGrid.ItemsSource);

            if (e.Key == Key.Return)
            {
                if (!textemail.Text.ToString().Equals(""))
                {

                    IEnumerable<Jewelry_shop_sales> temp = new ObservableCollection<Jewelry_shop_sales>();
                    temp = ((IEnumerable<Jewelry_shop_sales>)produtsdataGrid.ItemsSource).Where(item => item.CustomerName.ToLower().Contains(textemail.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                }
                else
                {
                    if (textBox.Text.Equals("[Date]") && textBox_Copy1.Text.Equals("[Price]") && textBox_Copy3.Text.Equals("[Gold Given]") && createdby_Copy3.Text.Equals("[Money Given]") && textemail.Text.Equals(""))
                    {
                        textemail.Text = "[Customer Name]";
                        produtsdataGrid.ItemsSource = Jewelry_shop_sales.AllJewelrySales;
                        produtsdataGrid.Items.Refresh();
                    }
                    else
                    {
                        textemail.Text = "[Customer Name]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();
                    }
                }
            }
        }
    }
    /* public class item :items
     {





         }
     public class item2 :items
     {

         public String Image { set; get; }



     }
     public class items {
         public BitmapImage Image { set; get; }
         public String Code { set; get; }
         public string Name { set; get; }
         public String Type { set; get; }
         public string Catagory { set; get; }
         public String Quantity { set; get; }
         public string Cost { set; get; }
         public string AlertQuantity { set; get; }

         public String Price { set; get; }
         public String Description { set; get; }
     }*/


}

﻿using basicPos2.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for userwindow.xaml
    /// </summary>
    public partial class userwindow : Window
    {
        User populator;
        public userwindow(User u)
        {
            InitializeComponent();
            this.populator = u;
            if (populator != null)
            {

                if (u.Level==2)
                {
                    Group.SelectedIndex = 0 ;
                }else
                {
                    Group.SelectedIndex=1;

                }
                Name.Text = u.Name;
                Phone.Text = u.Phone;
                
                Gender.Text = u.Gender;
                Email.Text = u.Email;
                Password.Text = u.Password;
                Status.Text = u.Status;
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Name.BorderBrush = Brushes.Black;
            Phone.BorderBrush = Brushes.Black;
            Email.BorderBrush = Brushes.Black;
            Password.BorderBrush = Brushes.Black;
            if (Name.Text.Equals(""))
            {
                Name.BorderBrush = Brushes.Red;
                MessageBox.Show("Please enter a Name");
            }
            else
            {
                if (Phone.Text.Equals(""))
                {
                    Phone.BorderBrush = Brushes.Red;
                    MessageBox.Show("Please enter a phone number");
                }
                else
                {
                    if (Email.Text.Equals(""))
                    {
                        Email.BorderBrush = Brushes.Red;
                        MessageBox.Show("Please enter a Email ID");

                    }
                    else
                    {
                        if (Password.Text.Equals(""))
                        {
                            Password.BorderBrush = Brushes.Red;
                            MessageBox.Show("Please enter a Password");

                        }else
                        {
                            if (populator != null)
                            {
                                if (Group.SelectedIndex==0)
                                {
                                    User.editUser(User.AllUser.IndexOf(populator), new User {Level=2, Id = populator.Id, Group = Group.Text, Name = Name.Text, Phone = Phone.Text, Gender = Gender.Text, Email = Email.Text, Password = Password.Text, Status = Status.Text });
                                    this.Close();
                                }else
                                {
                                    User.editUser(User.AllUser.IndexOf(populator), new User { Level = 0, Id = populator.Id, Group = Group.Text, Name = Name.Text, Phone = Phone.Text, Gender = Gender.Text, Email = Email.Text, Password = Password.Text, Status = Status.Text });
                                    this.Close();

                                }
                            }
                            else
                            {
                                if (Group.SelectedIndex==0)
                                {
                                    User.AddToAllUsers(new User { Id = User.AllUser.Count + 1, Level = 2, Group = Group.Text, Name = Name.Text, Phone = Phone.Text, Gender = Gender.Text, Email = Email.Text, Password = Password.Text, Status = Status.Text });
                                    this.Close();
                                }else
                                {
                                    User.AddToAllUsers(new User { Id = User.AllUser.Count + 1, Level = 0, Group = Group.Text, Name = Name.Text, Phone = Phone.Text, Gender = Gender.Text, Email = Email.Text, Password = Password.Text, Status = Status.Text });
                                    this.Close();
                                }
                                
                            }
                        }
                    }
                }
            }
                        
        }
    }
}

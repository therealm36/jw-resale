﻿using basicPos2.models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for ExpenseWindow.xaml
    /// </summary>
    public partial class ExpenseWindow : Window
    {
        ExpenseModel populator;
        Expenses i;
        public ExpenseWindow(Expenses i,ExpenseModel p)
        {
            this.i = i;
            InitializeComponent();
            this.populator = p;
            Date.SelectedDate = DateTime.Today;
            Time.SelectedTime = DateTime.Now;
            if (populator!=null)
            {

                try
                {
                    Date.SelectedDate = DateTime.ParseExact(populator.Date, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    Time.SelectedTime = Convert.ToDateTime(populator.Time);
                }
                catch (Exception e)
                {
                    Date.Text = populator.Date;
                }
                Reference.Text = p.Name;
                Amount.Text = p.Amount+"";
                Note.Text = p.Note;

            }
        }
        private void SelectAddress(object sender, RoutedEventArgs e)

        {

            System.Windows.Controls.TextBox tb = (sender as System.Windows.Controls.TextBox);

            if (tb != null)

            {

                tb.SelectAll();

            }

        }



        private void SelectivelyIgnoreMouseButton(object sender,

            MouseButtonEventArgs e)

        {

            System.Windows.Controls.TextBox tb = (sender as System.Windows.Controls.TextBox);

            if (tb != null)

            {

                if (!tb.IsKeyboardFocusWithin)

                {

                    e.Handled = true;

                    tb.Focus();

                }

            }

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Date.BorderBrush = Brushes.Black;
            Amount.BorderBrush = Brushes.Black;
            Time.BorderBrush = Brushes.Black;
            if (Time.SelectedTime == null)
            {
                Time.BorderBrush = Brushes.Red;
                MessageBox.Show("Please select a valid Time");

            }
            else
            {


                if (Date.SelectedDate == null)
                {
                    Date.BorderBrush = Brushes.Red;
                    MessageBox.Show("Please select a valid date");
                }
                else
                {
                    try
                    {
                        float.Parse(Amount.Text);
                        float test=1 / float.Parse(Amount.Text);

                        if (populator != null)
                        {
                            ExpenseModel.editExpense(ExpenseModel.AllExpenses.IndexOf(populator), new ExpenseModel { Time=Time.Text,Id = populator.Id, Name = Reference.Text, Note = Note.Text, Amount = float.Parse(Amount.Text), Created_by = populator.Created_by, Date = Date.Text });
                            this.Close();
                            i.amount_label.Content = ((ObservableCollection<ExpenseModel>)i.produtsdataGrid.ItemsSource).Sum(item => item.Amount);

                        }
                        else
                        {
                            ExpenseModel.AddToAllExpenses(new ExpenseModel { Id = ExpenseModel.AllExpenses.Count + 1,Time=Time.Text, Name = Reference.Text, Note = Note.Text, Amount = float.Parse(Amount.Text), Date = Date.Text });
                            this.Close();
                            i.amount_label.Content = ((ObservableCollection<ExpenseModel>)i.produtsdataGrid.ItemsSource).Sum(item => item.Amount);

                        }
                    }
                    catch
                    {
                        Amount.BorderBrush = Brushes.Red;
                        MessageBox.Show("Please enter a valid amount other than 0");
                    }
                }

            }
        }
    }
}

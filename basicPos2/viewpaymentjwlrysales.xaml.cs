﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using basicPos2.models;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for viewPayments.xaml
    /// </summary>
    /// 

    public partial class viewPaymentsjwlrysales : Window
    {
        Salesuser p;
        ObservableCollection<Payment> topoulate = new ObservableCollection<Payment>();
        public viewPaymentsjwlrysales(Salesuser p, Jewelry_shop_sales item)
        {
            InitializeComponent();

            this.p = p;
            Payment.getFromDatabasejwss(item, topoulate);

            paymentdatagrid.ItemsSource = topoulate;
        }

        private void deletebtn_Click(object sender, RoutedEventArgs e)
        {

            basicPos2.models.Payment.DeleteFromAllGivenPayment((basicPos2.models.Payment)paymentdatagrid.SelectedItem, topoulate);
            Sales.ClearAllSales();
            Sales.getFromDatabase();
            //p.Total.Content = ((ObservableCollection<Sales>)p.produtsdataGrid.ItemsSource).Sum(item => item.Total);
           // p.Discount.Content = ((ObservableCollection<Sales>)p.produtsdataGrid.ItemsSource).Sum(item => item.TotalDiscount);
          //  p.GrandTotal.Content = ((ObservableCollection<Sales>)p.produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
          //  p.Paid.Content = ((ObservableCollection<Sales>)p.produtsdataGrid.ItemsSource).Sum(item => item.Paid);

        }
        private static AddPayment editp = null;
        private void viewbtn_Click(object sender, RoutedEventArgs e)
        {
            if (editp == null || editp.IsLoaded == false)
            {
                editp = new AddPayment( null, null, null, null, (Payment)paymentdatagrid.SelectedItem, topoulate);
                editp.Title = "Edit Payment";
                editp.Show();
            }
            else
            {
                editp.Activate();
            }
        }
    }
}

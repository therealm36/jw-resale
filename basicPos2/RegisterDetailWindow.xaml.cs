﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using basicPos2.models;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for RegisterDetailWindow.xaml
    /// </summary>
    public partial class RegisterDetailWindow : Window
    {
        public RegisterDetailWindow()
        {
            InitializeComponent();

            Register.updateFromDb();

            cashInHandLbl.Content = Register.currentRegister.CashInHand.ToString("0.00");
            totalExpenseLbl.Content = Register.currentRegister.TotalExpenses.ToString("0.00");
            totalSalesLbl.Content = (Register.currentRegister.CashSalesTotal + Register.currentRegister.NoOfCheques).ToString("0.00");
            chasSalesLbl.Content = Register.currentRegister.CashSalesTotal.ToString("0.00");
            chequeSalesLbl.Content = Register.currentRegister.NoOfCheques.ToString("0.00");
            totalCashLbl.Content = (Register.currentRegister.CashInHand + Register.currentRegister.CashSalesTotal - Register.currentRegister.TotalExpenses).ToString("0.00");
        }
    }
}

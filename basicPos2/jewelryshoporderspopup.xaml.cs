﻿using System;
using System.Collections.Generic;
using System.Linq;

using basicPos2.models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for jewelryshopsalespopup.xaml
    /// </summary>
    public partial class jewelryshoporderspopup : Window
    {
        private ObservableCollection<Product> combogridpopulator = new ObservableCollection<Product>();
        public jewelryshoporderspopup(Jewelry_shop_orders populator)
        {
            InitializeComponent();
            comboaddproduct.ItemsSource = Product.AllProducts;
            comboproductdatagrid.ItemsSource = combogridpopulator;
            Date.SelectedDate = DateTime.Today;
            Time.SelectedTime = DateTime.Now;
            DueDate.SelectedDate = DateTime.Today.AddDays(7);
            DueTime.SelectedTime = DateTime.Now;
            customercombo.ItemsSource = Customer.AllCustomers;
            if (populator != null)
            {
                
                buttonn.Visibility = Visibility.Collapsed;
                try
                {
                    Date.SelectedDate = DateTime.ParseExact(populator.Date, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    Time.SelectedTime = Convert.ToDateTime(populator.Time);
                    DueDate.SelectedDate = DateTime.ParseExact(populator.Datedue, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    DueTime.SelectedTime = Convert.ToDateTime(populator.Timedue);


                }
                catch (Exception e)
                {
                    Date.Text = populator.Date;
                }
                Jewelry_shop_orders.getComboProducts(populator.Id, combogridpopulator);
                MoneyGiven.Text = populator.MoneyGiven + "";
                GoldGiven.Text = populator.GoldGiven + "";

                if (Customer.AllCustomers.First(item => item.Id == populator.CustomerID) != null)
                {
                    customercombo.SelectedItem = Customer.AllCustomers.First(item => item.Id == populator.CustomerID);
                }
            }
        }
        private void comboaddproduct_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((Product)comboaddproduct.SelectedItem != null)
            {
                if (!combogridpopulator.Any(item => item.Code.Equals(((Product)comboaddproduct.SelectedItem).Code)))
                {


                    combogridpopulator.Add(new Product { Id = ((Product)comboaddproduct.SelectedItem).Id, Weight = ((Product)comboaddproduct.SelectedItem).Weight, Name = ((Product)comboaddproduct.SelectedItem).ToString(), Code = ((Product)comboaddproduct.SelectedItem).Code, Quantity = 1 });

                    GoldGiven.Text = "" + combogridpopulator.Sum(item => (item.Weight * item.Quantity));
                }
            }
        }
        private void Quantity_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                try
                {
                    if (!(sender as TextBox).Text.ToString().Equals("") && (float.Parse((sender as TextBox).Text) != 0))
                    {

                        ((Product)comboproductdatagrid.SelectedItem).Quantity = float.Parse((sender as TextBox).Text);

                        GoldGiven.Text = "" + combogridpopulator.Sum(item => (item.Weight * item.Quantity));

                    }
                    else
                    {
                        MessageBox.Show("Please enter a valid number other than 0 for quantity");
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Please enter a valid number other than 0 for quantity");

                }
            }
            else
            {

            }
        }
        private void delete_Click(object sender, RoutedEventArgs e)
        {
            combogridpopulator.Remove((Product)comboproductdatagrid.SelectedItem);

            GoldGiven.Text = "" + combogridpopulator.Sum(item => (item.Weight * item.Quantity));

        }

        private void buttonn_Click(object sender, RoutedEventArgs e)
        {
            customercombo.BorderBrush = Brushes.Black;
            Date.BorderBrush = Brushes.Black;
            Time.BorderBrush = Brushes.Black;
            DueDate.BorderBrush = Brushes.Black;
            DueTime.BorderBrush = Brushes.Black;
            if (DueDate.SelectedDate == null)
            {
                DueDate.BorderBrush = Brushes.Red;
                scroller.ScrollToTop();
                MessageBox.Show("Please select a proper due date");

            }
            else
            {
                if (DueTime.SelectedTime == null)
                {
                    DueTime.BorderBrush = Brushes.Red;
                    scroller.ScrollToTop();
                    MessageBox.Show("Please select a proper due time");

                }
                else
                {
                    if (customercombo.SelectedItem == null)
                    {
                        customercombo.BorderBrush = Brushes.Red;
                        scroller.ScrollToTop();
                        MessageBox.Show("Please select a Customer");

                    }
                    else
                    {
                        if (Time.SelectedTime == null)
                        {
                            Time.BorderBrush = Brushes.Red;
                            scroller.ScrollToTop();
                            MessageBox.Show("Please select a valid Time");

                        }
                        else
                        {
                            if (Date.SelectedDate == null)
                            {
                                Date.BorderBrush = Brushes.Red;
                                scroller.ScrollToTop();
                                MessageBox.Show("Please select a valid date");
                            }
                            else
                            {
                                Jewelry_shop_orders.addJWOrder(new Jewelry_shop_orders {Datedue=DueDate.Text,Timedue=DueTime.Text, Date = Date.Text, Time = Time.Text, GoldGiven = float.Parse(GoldGiven.Text), MoneyGiven = float.Parse(MoneyGiven.Text), CustomerID = ((Customer)customercombo.SelectedItem).Id }, combogridpopulator);
                                this.Close();
                            }
                        }
                    }
                }
            }
        }
        private void comboBoxn1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }




        private void comboaddproduct_KeyDown(object sender, KeyEventArgs e)
        {

        }
        private static CustomerWindow addProdutsPop = null;
        private void button5_Click(object sender, RoutedEventArgs e)
        {
            if (addProdutsPop == null || addProdutsPop.IsLoaded == false)
            {
                addProdutsPop = new CustomerWindow(null);
                addProdutsPop.Title = "Add Customer";
                addProdutsPop.Show();
            }
            else
            {
                addProdutsPop.Activate();
            }

        }

        private static popupwindow addProduts = null;
        private void addprod_Click(object sender, RoutedEventArgs e)
        {
            if (addProduts == null || addProduts.IsLoaded == false)
            {
                addProduts = new popupwindow(null,null, null, false);
                addProduts.Title = "Add Gold Smith";
                addProduts.Show();
            }
            else
            {
                addProduts.Activate();
            }

        }

    }
}

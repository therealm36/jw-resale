﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using System.IO;
using System.Collections;
using System.Windows.Controls.Primitives;
using Microsoft.Office.Interop.Excel;
using Microsoft.Win32;
using basicPos2.models;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using ZXing;
using System.Drawing;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for Products.xaml
    /// </summary>
    public partial class Products : UserControl
    {

        
        public Products()
        {

            InitializeComponent();
            produtsdataGrid.ItemsSource = Product.AllProducts;
           
            
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {

        }

        
       
        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void dataGrid_Loaded(object sender, RoutedEventArgs e)
        {

            
        }
        
        private void viewbtn_Click(object sender, RoutedEventArgs e)
        {
            Product i = (Product)produtsdataGrid.SelectedItem;

             ImageViewer imagepop = new ImageViewer(i.Image);
            //ProductConfirmation imagepop = new ProductConfirmation(i);
            imagepop.Show();

        }

        private void deletebtn_Click(object sender, RoutedEventArgs e)
        {
            
            Product.DeleteFromAllProducts((Product)produtsdataGrid.SelectedItem);
        }
        private static popupwindow editpop = null;
        private void editbtn_Click(object sender, RoutedEventArgs e)
        {
            if (editpop==null || editpop.IsLoaded==false)
            {
                editpop = new popupwindow(null,null, (Product)produtsdataGrid.SelectedItem,true);
                editpop.Title = "Edit Product";
                editpop.Show();
            }else
            {
                editpop.Activate();
            }
            
        }
        private static popupwindow addProductsPop = null;
        private void button5_Click(object sender, RoutedEventArgs e)
        {
            if (addProductsPop==null || addProductsPop.IsLoaded == false)
            {
                addProductsPop = new popupwindow(null,null, null,true);
                addProductsPop.Title = "Add Product";
                addProductsPop.Show();
            }else
            {
                addProductsPop.Activate();

            }

        }
        private void button3_Click(object sender, RoutedEventArgs e)
        {
            RealUtility.ExportToPdf(produtsdataGrid);
        }
        
        

        
        private void button2_Click(object sender, RoutedEventArgs e)
        {
            RealUtility.Exporttocsv(produtsdataGrid);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            try { 
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");
            RealUtility.GenerateExcel(Product.AllProducts.ToDataTable());
            RealUtility.workBook.SaveAs(RealUtility.pathfornarnia+ "ProductList--" + RealUtility.todaydate + ".xls", System.IO.FileMode.Create, FileAccess.ReadWrite);
            RealUtility.workBook.Close();
            RealUtility.excel.Quit();
            MessageBox.Show("Successfully exported", "Success");

            }
            catch
            {
                MessageBox.Show("Please close all instances of excel and try again", "Error");
            }
        }

        
        
        private void button6_Click(object sender, RoutedEventArgs e)
        {
            RealUtility.ImportFromExcel();
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            

            (sender as System.Windows.Controls.Button).ContextMenu.IsEnabled = true;
            (sender as System.Windows.Controls.Button).ContextMenu.PlacementTarget = (sender as System.Windows.Controls.Button);
            (sender as System.Windows.Controls.Button).ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            (sender as System.Windows.Controls.Button).ContextMenu.IsOpen = true;
        }

        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            
            IEnumerable<Product> backup = new ObservableCollection<Product>((IEnumerable<Product>)produtsdataGrid.ItemsSource);
            
            if (e.Key == Key.Return)
            {
                if (!textBox.Text.ToString().Equals(""))
                {
                    
                    IEnumerable<Product> temp = new ObservableCollection<Product>();
                    temp=((IEnumerable<Product>)produtsdataGrid.ItemsSource).Where(item => item.Code.ToLower().Contains(textBox.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                }else
                {
                    if (textBox.Text.Equals("") && textBox_Copy.Text.Equals("[Name]") && textBox_Copy1.Text.Equals("[Metal Color]") && textBox_Copy2.Text.Equals("[Catagory]") && textBox_Copy3.Text.Equals("[Quantity]") && DesignLabel.Text.Equals("[Design]") && Caratslabel.Text.Equals("[Carats]"))
                    {
                        textBox.Text = "[Code]";
                        produtsdataGrid.ItemsSource = Product.AllProducts;
                        produtsdataGrid.Items.Refresh();
                    }else
                    {
                        textBox.Text = "[Code]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();
                    }
                }
            }
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void textBox_MouseUp(object sender, MouseButtonEventArgs e)
        {

        }
        private void SelectAddress(object sender, RoutedEventArgs e)

        {

            System.Windows.Controls.TextBox tb = (sender as System.Windows.Controls.TextBox);

            if (tb != null)

            {

                tb.SelectAll();

            }

        }



        private void SelectivelyIgnoreMouseButton(object sender,

            MouseButtonEventArgs e)

        {

            System.Windows.Controls.TextBox tb = (sender as System.Windows.Controls.TextBox);

            if (tb != null)

            {

                if (!tb.IsKeyboardFocusWithin)

                {

                    e.Handled = true;

                    tb.Focus();

                }

            }

        }

        private void textBox_Copy_MouseUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void textBox_Copy_KeyDown(object sender, KeyEventArgs e)
        {
            
            IEnumerable<Product> backup = new ObservableCollection<Product>((IEnumerable<Product>)produtsdataGrid.ItemsSource);
           
            if (e.Key == Key.Return)
            {
                if (!textBox_Copy.Text.ToString().Equals(""))
                {

                    IEnumerable<Product> temp = new ObservableCollection<Product>();
                    temp = ((IEnumerable<Product>)produtsdataGrid.ItemsSource).Where(item => item.Name.ToLower().Contains(textBox_Copy.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                }
                else
                {
                    if (textBox.Text.Equals("[Code]") && DesignLabel.Text.Equals("[Design]") && Caratslabel.Text.Equals("[Quantity]") && textBox_Copy.Text.Equals("") && textBox_Copy1.Text.Equals("[Metal Color]") && textBox_Copy2.Text.Equals("[Catagory]") && textBox_Copy3.Text.Equals("[Carats]"))
                    {
                        textBox_Copy.Text = "[Name]";
                        produtsdataGrid.ItemsSource = Product.AllProducts;
                        produtsdataGrid.Items.Refresh();
                    }
                    else
                    {
                        textBox_Copy.Text = "[Name]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();
                    }
                }
            }
        }

        private void textBox_Copy1_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void textBox_Copy1_KeyDown(object sender, KeyEventArgs e)
        {
            IEnumerable<Product> backup = new ObservableCollection<Product>((IEnumerable<Product>)produtsdataGrid.ItemsSource);

            if (e.Key == Key.Return)
            {
                if (!textBox_Copy1.Text.ToString().Equals(""))
                {

                    IEnumerable<Product> temp = new ObservableCollection<Product>();
                    temp = ((IEnumerable<Product>)produtsdataGrid.ItemsSource).Where(item => item.MetalColor.ToLower().Contains(textBox_Copy1.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                }
                else
                {
                    if (textBox.Text.Equals("[Code]") && textBox_Copy.Text.Equals("[Name]") && textBox_Copy1.Text.Equals("") && textBox_Copy2.Text.Equals("[Catagory]") && textBox_Copy3.Text.Equals("[Quantity]") && DesignLabel.Text.Equals("[Design]") && Caratslabel.Text.Equals("[Carats]"))
                    {
                        textBox_Copy1.Text = "[Metal Color]";
                        produtsdataGrid.ItemsSource = Product.AllProducts;
                        produtsdataGrid.Items.Refresh();
                    }
                    else
                    {
                        textBox_Copy1.Text = "[Metal Color]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();
                    }
                }
            }
        }

        private void textBox_Copy2_KeyDown(object sender, KeyEventArgs e)
        {
            IEnumerable<Product> backup = new ObservableCollection<Product>((IEnumerable<Product>)produtsdataGrid.ItemsSource);

            if (e.Key == Key.Return)
            {
                if (!textBox_Copy2.Text.ToString().Equals(""))
                {

                    IEnumerable<Product> temp = new ObservableCollection<Product>();
                    temp = ((IEnumerable<Product>)produtsdataGrid.ItemsSource).Where(item => item.Catagory.ToLower().Contains(textBox_Copy2.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                }
                else
                {
                    if (textBox.Text.Equals("[Code]") && textBox_Copy.Text.Equals("[Name]") && textBox_Copy1.Text.Equals("[Metal Color]") && textBox_Copy2.Text.Equals("") && textBox_Copy3.Text.Equals("[Quantity]") && DesignLabel.Text.Equals("[Design]") && Caratslabel.Text.Equals("[Carats]"))
                    {
                        textBox_Copy2.Text = "[Catagory]";
                        produtsdataGrid.ItemsSource = Product.AllProducts;
                        produtsdataGrid.Items.Refresh();
                    }
                    else
                    {
                        textBox_Copy2.Text = "[Catagory]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();
                    }
                }
            }
        }

        private void textBox_Copy3_TextChanged(object sender, TextChangedEventArgs e)
        {

           
        }

        private void textBox_Copy3_KeyDown(object sender, KeyEventArgs e)
        {
            IEnumerable<Product> backup = new ObservableCollection<Product>((IEnumerable<Product>)produtsdataGrid.ItemsSource);

            if (e.Key == Key.Return)
            { 
                if (!textBox_Copy3.Text.Equals(""))
                {
    
                    IEnumerable<Product> temp = new ObservableCollection<Product>();
                   
                    temp = Product.AllProducts.Where(item => item.Quantity.ToString().ToLower().Contains(textBox_Copy3.Text.ToString().ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                     produtsdataGrid.Items.Refresh();
                    
                }
                else
                {
                    if (textBox.Text.Equals("[Code]") && textBox_Copy.Text.Equals("[Name]") && textBox_Copy1.Text.Equals("[Metal Color]") && textBox_Copy2.Text.Equals("[Catagory]") && textBox_Copy3.Text.Equals("") && DesignLabel.Text.Equals("[Design]") && Caratslabel.Text.Equals("[Carats]"))
                    {
                        textBox_Copy3.Text = "[Quantity]";
                        produtsdataGrid.ItemsSource = Product.AllProducts;
                        produtsdataGrid.Items.Refresh();
                    }
                    else
                    {
                        textBox_Copy3.Text = "[Quantity]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();
                    }
                }
            }
        }
        private void Design_KeyDown(object sender, KeyEventArgs e)
        {
            IEnumerable<Product> backup = new ObservableCollection<Product>((IEnumerable<Product>)produtsdataGrid.ItemsSource);

            if (e.Key == Key.Return)
            {
                if (!DesignLabel.Text.Equals(""))
                {

                    IEnumerable<Product> temp = new ObservableCollection<Product>();

                    temp = Product.AllProducts.Where(item => item.Design.ToString().ToLower().Contains(DesignLabel.Text.ToString().ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();

                }
                else
                {
                    if (textBox.Text.Equals("[Code]") && textBox_Copy.Text.Equals("[Name]") && textBox_Copy1.Text.Equals("[Metal Color]") && textBox_Copy2.Text.Equals("[Catagory]") && textBox_Copy3.Text.Equals("[Quantity]") && DesignLabel.Text.Equals("") && Caratslabel.Text.Equals("[Carats]"))
                    {
                        DesignLabel.Text = "[Design]";
                        produtsdataGrid.ItemsSource = Product.AllProducts;
                        produtsdataGrid.Items.Refresh();
                    }
                    else
                    {
                        DesignLabel.Text = "[Design]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();
                    }
                }
            }
        }
        private void Carats_KeyDown(object sender, KeyEventArgs e)
        {
            IEnumerable<Product> backup = new ObservableCollection<Product>((IEnumerable<Product>)produtsdataGrid.ItemsSource);

            if (e.Key == Key.Return)
            {
                if (!Caratslabel.Text.Equals(""))
                {

                    IEnumerable<Product> temp = new ObservableCollection<Product>();

                    temp = Product.AllProducts.Where(item => item.MetalType.ToString().ToLower().Contains(Caratslabel.Text.ToString().ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();

                }
                else
                {
                    if (textBox.Text.Equals("[Code]") && textBox_Copy.Text.Equals("[Name]") && textBox_Copy1.Text.Equals("[Metal Color]") && textBox_Copy2.Text.Equals("[Catagory]") && textBox_Copy3.Text.Equals("[Quantity]") && DesignLabel.Text.Equals("[Designs]") && Caratslabel.Text.Equals(""))
                    {
                        Caratslabel.Text = "[Carats]";
                        produtsdataGrid.ItemsSource = Product.AllProducts;
                        produtsdataGrid.Items.Refresh();
                    }
                    else
                    {
                        Caratslabel.Text = "[Carats]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();
                    }
                }
            }
        }
        private void textBox_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox.Text = "[Code]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void textBox_Copy_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy.Text = "[Name]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void textBox_Copy1_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy1.Text = "[Type]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void textBox_Copy2_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy2.Text = "[Catagory]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void Design_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy2.Text = "[Catagory]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void Design_TextChanged(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy2.Text = "[Catagory]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }
        private void Carats_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy2.Text = "[Catagory]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void Carats_TextChanged(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy2.Text = "[Catagory]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }
        private void textBox_Copy3_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy3.Text = "[Quantity]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void barCodeBtn_Click(object sender, RoutedEventArgs e)
        {
            var product = (Product)produtsdataGrid.SelectedItem;
            var content = product.Code;
            var writer = new BarcodeWriter
            {
                Format = BarcodeFormat.CODE_128
            };
            Bitmap bitmap = writer.Write(content);
            SingleBarCodeWindow barcdoeWindow = new SingleBarCodeWindow(bitmap);
            barcdoeWindow.Title = product.Name + " Bar Code";
            barcdoeWindow.ShowDialog();
        }

        private void allBarCodeBtn_Click(object sender, RoutedEventArgs e)
        {
            ObservableCollection<BarcodeWithName> items = new ObservableCollection<BarcodeWithName>();

            for (int i=0; i<Product.AllProducts.Count;i++)
            {
                Product product = Product.AllProducts[i];
                var content = product.Code;
                var writer = new BarcodeWriter
                {
                    Format = BarcodeFormat.CODE_128
                };
                Bitmap bitmap = writer.Write(content);
                items.Add(new BarcodeWithName { Name = product.Name, Image = ConvertBitmap(bitmap) });
            }

            AllBarCodesWindow allBarcodeWindow = new AllBarCodesWindow(items);
            allBarcodeWindow.ShowDialog();

            
        }

        public BitmapImage ConvertBitmap(System.Drawing.Bitmap bitmap)
        {
            MemoryStream ms = new MemoryStream();
            bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            ms.Seek(0, SeekOrigin.Begin);
            image.StreamSource = ms;
            image.EndInit();

            return image;
        }

        
    }

    public class BarcodeWithName
    {
         BitmapImage _image;
        string _name;

        public BitmapImage Image
        {
            get
            {
                return _image;
            }

            set
            {
                _image = value;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }
    }
    /* public class item :items
     {





         }
     public class item2 :items
     {

         public String Image { set; get; }



     }
     public class items {
         public BitmapImage Image { set; get; }
         public String Code { set; get; }
         public string Name { set; get; }
         public String Type { set; get; }
         public string Catagory { set; get; }
         public String Quantity { set; get; }
         public string Cost { set; get; }
         public string AlertQuantity { set; get; }

         public String Price { set; get; }
         public String Description { set; get; }
     }*/
    

}


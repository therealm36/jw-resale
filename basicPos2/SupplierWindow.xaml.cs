﻿using basicPos2.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for SupplierWindow.xaml
    /// </summary>
    public partial class SupplierWindow : Window
    {
        Supplier populator;
        public SupplierWindow(Supplier p)
        {
            InitializeComponent();
            this.populator = p;
            if (populator != null)
            {
                
                Name.Text = p.Name;
                Phone.Text = p.Phone;
                Email.Text = p.Email;
                cf1.Text = p.CF1;
                cf2.Text = p.CF2;

            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Name.BorderBrush = Brushes.Black;
            Phone.BorderBrush = Brushes.Black;
            Email.BorderBrush = Brushes.Black;
            if (Name.Text.Equals(""))
            {
                Name.BorderBrush = Brushes.Red;
                MessageBox.Show("Please enter a Name");
            }
            else
            {
                if (false)
                {
                    Phone.BorderBrush = Brushes.Red;
                    MessageBox.Show("Please enter a phone number");
                }
                else
                {
                    if (false)
                    {
                        Email.BorderBrush = Brushes.Red;
                        MessageBox.Show("Please enter a Email ID");

                    }
                    else
                    {
                        if (populator != null)
                        {
                            Supplier.editProduct(Supplier.AllSupliers.IndexOf(populator), new Supplier { Id = populator.Id, Name = Name.Text, Phone = Phone.Text, Email = Email.Text, CF1 = cf1.Text, CF2 = cf2.Text });
                            basicPos2.models.Purchases.AllPurchases.Clear();
                            basicPos2.models.Purchases.getFromDatabase();
                            this.Close();
                        }
                        else
                        {
                            Supplier.AddToAllSuppliers(new Supplier { Id = Supplier.AllSupliers.Count + 1, Name = Name.Text, Phone = Phone.Text, Email = Email.Text, CF1 = cf1.Text, CF2 = cf2.Text });
                            basicPos2.models.Purchases.AllPurchases.Clear();
                            basicPos2.models.Purchases.getFromDatabase();
                            this.Close();
                        }
                    }
                }
            }
                        
        }
    }
}

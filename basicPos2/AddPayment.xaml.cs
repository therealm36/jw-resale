﻿using basicPos2.models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for AddPayment.xaml
    /// </summary>
    public partial class AddPayment : Window
    {
        private string s;
        private string s1;
        private Payment m;
        private ObservableCollection<Payment> list;
       // private Salesuser p;
        public AddPayment(Jewelry_shop_sales jwss, Jewelry_shop_orders jwso, Gold_Smith_orders gsso, Gold_Smith_Purchases gssp, Payment m,ObservableCollection<Payment> list)
        {
            InitializeComponent();

            if (jwss != null)
            {
                s = "jwss"+jwss.Id;
                s1 = "j"+jwss.CustomerID;
            }else
            {

                if (jwso != null)
                {
                    s = "jwso" + jwso.Id;
                    s1 = "j"+jwso.CustomerID;
                }
                else
                {

                    if (gssp != null)
                    {
                        s = "gssp" + gssp.Id;
                        s1 = "g"+gssp.CustomerID;
                    }
                    else
                    {

                        if (gsso != null)
                        {
                            s = "gsso" + gsso.Id;
                            s1 = "g"+gsso.GoldSmithID;
                        }
                        else
                        {

                        }
                    }
                }
            }

            //this.s = s;
            this.m = m;
           // this.p = p;
            this.list = list;
            this.Date.SelectedDate = DateTime.Today;
            this.Time.SelectedTime = DateTime.Now;
            if (this.m != null)
            {
                try
                {
                    Date.SelectedDate = DateTime.ParseExact(m.Date, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    Time.SelectedTime = Convert.ToDateTime(m.Time);
                }
                catch (Exception e)
                {
                    Date.Text = m.Date;
                }
                button.Visibility = Visibility.Collapsed;
                Reference.Text = m.Reference;
                Goldgiven.Text = m.Goldgiven+"";
                MoneyGiven.Text = m.Moneygiven + "";
            }

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Date.BorderBrush = Brushes.Black;
            Time.BorderBrush = Brushes.Black;

            if (Time.SelectedTime == null)
            {
                Time.BorderBrush = Brushes.Red;
                MessageBox.Show("Please select Time", "over");

            }
            else
            {
                if (Date.SelectedDate == null)
                {
                    Date.BorderBrush = Brushes.Red;
                    MessageBox.Show("Please select date", "over");
                }
                else
                {
                    if (true)
                    {
                        try
                        {
                            float.Parse(MoneyGiven.Text);
                            float.Parse(Goldgiven.Text);

                            if (this.m != null)
                            {
                                    Payment.editPaymentinGivenList(this.m.Amount, list.IndexOf(this.m), new Payment { Time = Time.Text, Id = this.m.Id, Date = Date.Text, SaleID = this.m.SaleID, Moneygiven = float.Parse(MoneyGiven.Text.ToString()), Goldgiven = float.Parse(Goldgiven.Text.ToString()), Reference = Reference.Text,CustomerId=m.CustomerId }, list);
                                   // Sales.ClearAllSales();
                                    //Sales.getFromDatabase();
                                  //  p.Total.Content = ((ObservableCollection<Sales>)p.produtsdataGrid.ItemsSource).Sum(item => item.Total);
                                  //  p.Discount.Content = ((ObservableCollection<Sales>)p.produtsdataGrid.ItemsSource).Sum(item => item.TotalDiscount);
                                   // p.GrandTotal.Content = ((ObservableCollection<Sales>)p.produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
                                  //  p.Paid.Content = ((ObservableCollection<Sales>)p.produtsdataGrid.ItemsSource).Sum(item => item.Paid);

                                    this.Close();
                                
                            }
                            else
                            {
                                    Payment.AddPayment(new Payment { SaleID=s,Time = Time.Text, Id = Payment.AllPayment.Count + 1, Date = Date.Text, Moneygiven=float.Parse(MoneyGiven.Text.ToString()), Goldgiven = float.Parse(Goldgiven.Text.ToString()), Reference = Reference.Text,CustomerId=s1 });
                                    //Sales.ClearAllSales();
                                    //Sales.getFromDatabase();

                               //     p.Total.Content = ((ObservableCollection<Sales>)p.produtsdataGrid.ItemsSource).Sum(item => item.Total);
                                //    p.Discount.Content = ((ObservableCollection<Sales>)p.produtsdataGrid.ItemsSource).Sum(item => item.TotalDiscount);
                               //     p.GrandTotal.Content = ((ObservableCollection<Sales>)p.produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
                             //       p.Paid.Content = ((ObservableCollection<Sales>)p.produtsdataGrid.ItemsSource).Sum(item => item.Paid);
//
                                    this.Close();
                                }
                            

                        }
                        catch
                        {
                        }
                    }
                    else
                    {
                    }
                }

            }
        }

    
    }
}

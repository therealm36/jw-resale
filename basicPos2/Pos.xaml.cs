﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using basicPos2.models;
using System.Windows.Interop;
using MaterialDesignThemes.Wpf;
using System.Windows.Media.Effects;
using basicPos2;
using System.Text.RegularExpressions;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for Pos.xaml
    /// </summary>
    public partial class Pos : UserControl
    {

        CollectionViewSource products;
        public Pos()
        {
            InitializeComponent();
            addItemCombo.ItemsSource = Product.AllProducts;                
            customerCombo.ItemsSource = Customer.AllCustomers;
            customerCombo.SelectedIndex = customerCombo.Items.Count-1;
            billGrid.ItemsSource = SaleItem.CurrentBill;
            catagoryCombo.ItemsSource = Catagory.AllCatagories;
            this.DataContext = SaleItem.CurrentBill;

           /* products = new CollectionViewSource();
            products.Filter += Products_Filter;
            products.Source = Product.AllProducts;

            addItemCombo.ItemsSource = products.View;*/
           

            updateProductGrid();
        }

      

        /*void Products_Filter(object sender, FilterEventArgs e)
        {
            if (e.Item != null)
            {
                e.Accepted = ((Product)e.Item).Name.Contains(addItemCombo.Text.ToUpper()) || ((Product)e.Item).Code.Contains(addItemCombo.Text.ToUpper());
            }
               
        }*/

        private void updateProductGrid()
        {
            productGridList.ItemsSource = Product.AllProducts.Where(item => item.CatagoryId == ((Catagory)catagoryCombo.SelectedValue).Id);
        }
        public void updateBillDetails()
        {
            var total = SaleItem.CurrentBill.Sum(item => item.UnitPrice * item.Quantity);
            var itemDiscount = SaleItem.CurrentBill.Sum(item => item.Quantity*item.ItemDiscount);
            var orderDiscount = Sales.currentSale.OrderDiscount;
            var totalDiscount = itemDiscount + orderDiscount;
            var totalPayable = total - orderDiscount;
            var totalItems = SaleItem.CurrentBill.Count;
            var totalQuantity = SaleItem.CurrentBill.Sum(item => item.Quantity);

            totalItemsLbl.Content = totalItems + "(" + totalQuantity + ")";
            totalLbl.Content = total;
            discountLbl.Content = orderDiscount;
            totalPayableLbl.Content = totalPayable ;


            Sales.currentSale.Total = total;
            Sales.currentSale.GrandTotal = totalPayable;
            Sales.currentSale.ProductDiscount = itemDiscount;
            Sales.currentSale.TotalDiscount = totalDiscount;
            Sales.currentSale.TotalItems = totalItems;
            Sales.currentSale.TotalQuantity = totalQuantity;

        }

        private void addItemCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
 
            if ((Product)addItemCombo.SelectedItem != null)
            {
                SaleItem.AddToCurrentBill((Product)addItemCombo.SelectedItem);
                updateBillDetails();
            }
            
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int index = billGrid.SelectedIndex;
            SaleItem.DeleteFromCurrentBill(index);
            updateBillDetails();
        }



        private void addCustomerBtn_Click(object sender, RoutedEventArgs e)
        {
            AddCustomerPos addCustomerDialog = new AddCustomerPos();

            addCustomerDialog.Owner = Window.GetWindow(this);
          
            addCustomerDialog.ShowDialog();

   
        }

      

        private void productGridList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((Product)productGridList.SelectedItem != null)
            {
                ProductConfirmation productViewWindow = new ProductConfirmation((Product)productGridList.SelectedItem, this);
                productViewWindow.ShowDialog();
                
            }

            productGridList.SelectedIndex = -1;
            
        }

        private void customerCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Sales.currentSale.CustomerId = (customerCombo.SelectedItem as Customer).Id;
            Sales.currentSale.CustomerName = (customerCombo.SelectedItem as Customer).Name;
        }

        private void billGrid_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {
           
        }

        private void billGrid_AddingNewItem(object sender, DataGridCellEditEndingEventArgs e)
        {
           
        }

        private void billGrid_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
          
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            updateBillDetails();
        }

        private void paymentBtn_Click(object sender, RoutedEventArgs e)
        {
            PaymentWindow payment = new PaymentWindow(this);
            payment.ShowDialog();
            
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            SaleItem item = SaleItem.CurrentBill[billGrid.SelectedIndex];
            ItemDiscount itemDiscount = new ItemDiscount(item.UnitPrice, item.ItemDiscount, item.Quantity, item.Comment, (SaleItem)billGrid.SelectedItem);
            itemDiscount.Title = item.ProductName;

            itemDiscount.Owner = Window.GetWindow(this);
            itemDiscount.ShowDialog();
            updateBillDetails();
        }

        private void discountBtn_Click(object sender, RoutedEventArgs e)
        {
            BillDiscount billDiscount = new BillDiscount();
            billDiscount.Owner = Window.GetWindow(this);
            billDiscount.ShowDialog();
            updateBillDetails();
        }

        private void catagoryCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            updateProductGrid();
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[+-]?([0-9]*[.])?[0-9]+");
            e.Handled = !regex.IsMatch(e.Text);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            productGridList.SelectedItem = ((ListBoxItem)productGridList.ContainerFromElement((Button)sender)).Content;
        }

        private void cancelBtn_Click(object sender, RoutedEventArgs e)
        {
            SaleItem.ClearCurrentBill();
            updateBillDetails();
        }
    }

   
}

﻿using basicPos2.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for Users.xaml
    /// </summary>
    public partial class Users : UserControl
    {
        public Users()
        {
            InitializeComponent();
            produtsdataGrid.ItemsSource = User.AllUser;
        }
        private static userwindow adduser = null;
        private void button5_Click(object sender, RoutedEventArgs e)
        {
            if (adduser == null||adduser.IsLoaded == false)
            {
                adduser = new userwindow(null);
                adduser.Title = "Add User";
                adduser.Show();
            }else
            {
                adduser.Activate();
            }
        }
        private static userwindow edituser = null;
        private void editbtn_Click(object sender, RoutedEventArgs e)
        {
            if (edituser == null||edituser.IsLoaded == false)
            {
                edituser = new userwindow((User)produtsdataGrid.SelectedItem);
                edituser.Title = "Edit User";
                edituser.Show();
            }else
            {
                edituser.Activate();
            }
        }

        private void deletebtn_Click(object sender, RoutedEventArgs e)
        {
            User.DeleteFromAllUsers((User)produtsdataGrid.SelectedItem);
        }
    }
}

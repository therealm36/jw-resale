﻿using basicPos2.models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for Loginxaml.xaml
    /// </summary>
    public partial class Loginxaml : Window
    {
        public Loginxaml()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            int result = this.checkInDb(userNameTxt.Text, passwordTxt.Password);
      
            if (result != -1)
            {
                Register.getFromDb();
                getGoldPrice();

                MainWindow mainWindow = new MainWindow();
                mainWindow.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Wrong user name or passwor", "Login Error");
            }
        }

        private void getGoldPrice()
        {
            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();

                string sql = "select * from tec_gold_price";
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {

                            RealUtility.K22 = float.Parse(reader["kt24"].ToString());
                            RealUtility.K21 = float.Parse(reader["kt21"].ToString());
                            RealUtility.K22 = float.Parse(reader["kt18"].ToString());

                        }
                    }
                }

            }
            catch (Exception)
            {

            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        private int checkInDb(string username, string password)
        {


            MySqlConnection connection = new MySqlConnection(RealUtility.dbString);

            try
            {
                connection.Open();
              
                string sql = "select * from tec_users where username='"+username+"' and password='"+password+"'";
                using (var command = new MySqlCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {

                            int previllage = Convert.ToInt32( reader["level"].ToString());
                            int userId = Convert.ToInt32(reader["level"].ToString());

                            if (userId >0 && previllage >= 0)
                            {
                                RealUtility.currentUserId = userId;
                                RealUtility.userLevel = previllage;
                            }
                            return previllage;
                        }
                        return -1;

                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Wrong user name or Password", "Login Failed");
                MessageBox.Show(e.Message);
                return -1;
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        
    }

    
}

﻿using basicPos2.models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for ProductConfirmation.xaml
    /// </summary>
    public partial class ProductConfirmation : Window
    {
        Pos _pos;
        Product _product;
        public ProductConfirmation(Product product, Pos pos)
        {
            InitializeComponent();
            _pos = pos;
            _product = product;
            if (product.Image != null)
            {
                productimage.Source = LoadImage(product.Image);
            }
            else
            {
                productimage.Source = new BitmapImage(new Uri(@"/basicPos2;component/icons/edit.png", UriKind.Relative));
            }
            productnamecode.Content = "Name: "+product.ToString();
            productweight.Content = "Weight: "+product.Weight+"g";
            productmetaltype.Content = "Purity: "+product.MetalType+" Carats";
            productdesign.Content = "Design: \n"+product.Design;
            productLabourcost.Content = "Labour Cost: " + product.LabourCost + " Rs";
            productOthercost.Content = "Other Cost: " + product.OtherCost + " Rs";
         
            productdetail.Text = "Detail: \n" + product.Details;
            producttotalcost.Content = "Total Cost: "+product.Cost+" Rs";
            productprice.Content = "Price: "+product.Price+" Rs";

        }
        private static BitmapImage LoadImage(byte[] imageData)
        {
            if (imageData == null || imageData.Length == 0) return null;
            var image = new BitmapImage();
            using (var mem = new MemoryStream(imageData))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();
            return image;
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void confirm_Click(object sender, RoutedEventArgs e)
        {
            SaleItem.AddToCurrentBill(_product);
            _pos.updateBillDetails();
            this.Close();
        }
    }
}

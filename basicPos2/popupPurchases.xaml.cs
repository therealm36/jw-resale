﻿using basicPos2.models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for popupPurchases.xaml
    /// </summary>
    public partial class popupPurchases : Window
    {
        private ObservableCollection<PurchaseItem> combogridpopulator;
        private ObservableCollection<basicPos2.models.Product> comboaddpopulator;
        private BitmapImage selectedphoto = null;
        private ObservableCollection<PurchaseItem> initialchildren;
        
        private basicPos2.models.Purchases populator;
        private Purchases p;
        public popupPurchases( Purchases p,basicPos2.models.Purchases pop)
        {
            this.p = p;
            this.populator = pop;
            InitializeComponent();
            Date.SelectedDate = DateTime.Today;
            Time.SelectedTime = DateTime.Now;
            combogridpopulator = new ObservableCollection<PurchaseItem>();
  
            initialchildren = new ObservableCollection<PurchaseItem>();
            comboaddpopulator = new ObservableCollection<Product>(Product.AllProducts);
            comboaddproduct.ItemsSource = comboaddpopulator;
            comboproductdatagrid.ItemsSource = combogridpopulator;
            
            supliercombo.ItemsSource = Supplier.AllSupliers;
            if (pop != null)
            {
                try
                {
                    Date.SelectedDate = DateTime.ParseExact(populator.Date, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    Time.SelectedTime = Convert.ToDateTime(pop.Time);
                } catch(Exception e)
                {
                    Date.Text = populator.Date;
                }
                basicPos2.models.Purchases.getComboItems(populator.Id,combogridpopulator);
                basicPos2.models.Purchases.getComboItems(populator.Id, initialchildren);
                
                Combonote.Text = populator.Note;
                total.Content = populator.Total;
                
                comboreference.Text = populator.Reference;
                if (Supplier.AllSupliers.First(item => item.Id == populator.SupplierID) != null)
                {
                    supliercombo.SelectedItem = Supplier.AllSupliers.First(item => item.Id == populator.SupplierID);
                }
                comborecieved.Text = populator.Recieved;
                
                }

               

            }
        


       

       

        
        

        private void buttonn_Click(object sender, RoutedEventArgs e)
        {
            Date.BorderBrush = Brushes.Black;
            comboaddproduct.BorderBrush = Brushes.Black;
            supliercombo.BorderBrush = Brushes.Black;
            Time.BorderBrush = Brushes.Black;
            
            if (Time.SelectedTime == null)
            {
                Time.BorderBrush = Brushes.Red;
                scroler.ScrollToTop();
                MessageBox.Show("Please select a valid Time");

            }
            else
            {
                if (Date.SelectedDate == null)
                {
                    Date.BorderBrush = Brushes.Red;
                    scroler.ScrollToTop();
                    MessageBox.Show("Please select a valid date");
                }
                else
                {
                    if (combogridpopulator.Count == 0)
                    {
                        comboaddproduct.BorderBrush = Brushes.Red;
                        scroler.ScrollToTop();
                        MessageBox.Show("Please select a product");

                    }
                    else
                    {
                        if (supliercombo.SelectedItem == null)
                        {
                            supliercombo.BorderBrush = Brushes.Red;
                            scroler.ScrollToTop();
                            MessageBox.Show("Please select a supplier");

                        }
                        else
                        {

                            if (populator == null)
                            {
                                basicPos2.models.Purchases.addCombo(new models.Purchases { Time=Time.Text,Id = basicPos2.models.Purchases.AllPurchases.Count + 1, Reference = comboreference.Text, Date = Date.Text, Note = Combonote.Text, Total = float.Parse(total.Content.ToString()), SupplierID = ((Supplier)supliercombo.SelectedItem).Id, Recieved = comborecieved.Text }, combogridpopulator);

                                p.total_label.Content = ((IEnumerable<basicPos2.models.Purchases>)p.produtsdataGrid.ItemsSource).Sum(item => item.Total);
                                this.Close();
                            }
                            else
                            {
                                basicPos2.models.Purchases.editCombo(populator, new models.Purchases {Time=Time.Text, Id = populator.Id, Reference = comboreference.Text, Date = Date.Text, Note = Combonote.Text, Total = float.Parse(total.Content.ToString()), SupplierID = ((Supplier)supliercombo.SelectedItem).Id, Recieved = comborecieved.Text }, combogridpopulator, initialchildren);
                                p.total_label.Content = ((IEnumerable<basicPos2.models.Purchases>)p.produtsdataGrid.ItemsSource).Sum(item => item.Total);

                                this.Close();
                            }
                        }
                    }
                }
            }
        }

        private void Type_GiveFeedback(object sender, GiveFeedbackEventArgs e)
        {

        }

        private void Type_FocusableChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

        }

        private void Type_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void comboBoxn1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void comboaddproduct_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void comboaddproduct_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((Product)comboaddproduct.SelectedItem != null)
            {
                if (!combogridpopulator.Any(item => item.Name.Equals(((Product)comboaddproduct.SelectedItem).ToString())))
                {
                    
                    combogridpopulator.Add(new PurchaseItem {Name= ((Product)comboaddproduct.SelectedItem).ToString(),ProductID=((Product)comboaddproduct.SelectedItem).Id,Quantity=1,Cost= ((Product)comboaddproduct.SelectedItem).Cost,Subtotal= ((Product)comboaddproduct.SelectedItem).Cost });
                    total.Content = combogridpopulator.Sum(item => item.Subtotal);

                }
            }
        }

        private void Quantity_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                try
                {
                    if ((!(sender as TextBox).Text.ToString().Equals("")) && (float.Parse((sender as TextBox).Text) != 0))
                    {

                        ((PurchaseItem)comboproductdatagrid.SelectedItem).Quantity = float.Parse((sender as TextBox).Text);
                        total.Content = combogridpopulator.Sum(item => item.Subtotal);
                    }
                    else
                    {
                        MessageBox.Show("Please input a valid number for quantity other than 0");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Please input a valid number for quantity other than 0");

                }
            }
           
        }

        private void Quantit_KeyDown(object sender, KeyEventArgs e)
        {
            //god i created this bymistake
        }

        private void delete_Click(object sender, RoutedEventArgs e)
        {
            
            combogridpopulator.Remove((PurchaseItem)comboproductdatagrid.SelectedItem);
            total.Content = combogridpopulator.Sum(item => item.Subtotal);
        }
        private void UnitCost_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {try
                {
                    if ((!(sender as TextBox).Text.ToString().Equals("")) && (float.Parse((sender as TextBox).Text) != 0))
                    {

                        ((PurchaseItem)comboproductdatagrid.SelectedItem).Cost = float.Parse((sender as TextBox).Text);
                        total.Content = combogridpopulator.Sum(item => item.Subtotal);
                    }
                    else
                    {
                        MessageBox.Show("Please input a valid number for amount other than 0");

                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Please input a valid number for amount other than 0");

                }
            }
            
        }

        private void comborecieved_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        private static popupwindow addProductsPop = null;
        private void button5_Click(object sender, RoutedEventArgs e)
        {
            if (addProductsPop == null || addProductsPop.IsLoaded == false)
            {
                addProductsPop = new popupwindow(null,null, null,true);
                addProductsPop.Title = "Add Product";
                addProductsPop.Show();
            }
            else
            {
                addProductsPop.Activate();

            }

        }
        private static SupplierWindow addProdutsPop = null;
        private void butto5_Click(object sender, RoutedEventArgs e)
        {
            if (addProdutsPop == null || addProdutsPop.IsLoaded == false)
            {
                addProdutsPop = new SupplierWindow(null);
                addProdutsPop.Title = "Add Supplier";
                addProdutsPop.Show();
            }
            else
            {
                addProdutsPop.Activate();
            }

        }
    }
}

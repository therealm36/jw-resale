﻿using basicPos2.models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for Expenses.xaml
    /// </summary>
    public partial class Expenses : UserControl
    {
        
        public Expenses()
        {
            InitializeComponent();
            produtsdataGrid.ItemsSource = ExpenseModel.AllExpenses;

            amount_label.Content = ((ObservableCollection<ExpenseModel>)produtsdataGrid.ItemsSource).Sum(item => item.Amount);
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {

        }



        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        private void SelectAddress(object sender, RoutedEventArgs e)

        {

            System.Windows.Controls.TextBox tb = (sender as System.Windows.Controls.TextBox);

            if (tb != null)

            {

                tb.SelectAll();

            }

        }



        private void SelectivelyIgnoreMouseButton(object sender,

            MouseButtonEventArgs e)

        {

            System.Windows.Controls.TextBox tb = (sender as System.Windows.Controls.TextBox);

            if (tb != null)

            {

                if (!tb.IsKeyboardFocusWithin)

                {

                    e.Handled = true;

                    tb.Focus();

                }

            }

        }

        private void dataGrid_Loaded(object sender, RoutedEventArgs e)
        {


        }

        private void viewbtn_Click(object sender, RoutedEventArgs e)
        {
            //Product i = (Product)dataGrid.SelectedItem;
            //ImageViewer imagepop = new ImageViewer(i.Image);
            //imagepop.Show();

        }

        private void deletebtn_Click(object sender, RoutedEventArgs e)
        {

            ExpenseModel.DeleteFromAllExpenses((ExpenseModel)produtsdataGrid.SelectedItem);
            amount_label.Content = ((ObservableCollection<ExpenseModel>)produtsdataGrid.ItemsSource).Sum(item => item.Amount);

        }
        private static ExpenseWindow editpop = null;
        private void editbtn_Click(object sender, RoutedEventArgs e)
        {
            if (editpop == null||editpop.IsLoaded == false)
            {
                editpop = new ExpenseWindow(this, (ExpenseModel)produtsdataGrid.SelectedItem);
                editpop.Title = "Edit Expenses";
                editpop.Show();
                amount_label.Content = ((ObservableCollection<ExpenseModel>)produtsdataGrid.ItemsSource).Sum(item => item.Amount);
            }else
            {
                editpop.Activate();
            }

        }
        private static ExpenseWindow addProductsPop = null;
        private void button5_Click(object sender, RoutedEventArgs e)
        {
            if (addProductsPop == null||addProductsPop.IsLoaded == false)
            {
                addProductsPop = new ExpenseWindow(this, null);
                addProductsPop.Title = "Add Expense";
                addProductsPop.Show();
            }else
            {
                addProductsPop.Activate();
            }

        }
        private void button3_Click(object sender, RoutedEventArgs e)
        {
            RealUtility.ExportToPdfExpenses(produtsdataGrid);
        }




        private void button2_Click(object sender, RoutedEventArgs e)
        {
            RealUtility.Exporttocsvexpenses(produtsdataGrid);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            try { 
            System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");
            RealUtility.GenerateExcelExpenses(ExpenseModel.AllExpenses.ToDataTable());
            RealUtility.workBook.SaveAs(RealUtility.pathfornarnia+ "ExpensesList--" + RealUtility.todaydate + ".xls", System.IO.FileMode.Create, FileAccess.ReadWrite);
            RealUtility.workBook.Close();
            RealUtility.excel.Quit();

            MessageBox.Show("Successfuly Exported as Excel File", "Success");
            }
            catch
            {
                MessageBox.Show("Please close all instances of excel and try again", "Error");
            }

        }



        private void button6_Click(object sender, RoutedEventArgs e)
        {
            // RealUtility.ImportFromExcel();
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {


            (sender as System.Windows.Controls.Button).ContextMenu.IsEnabled = true;
            (sender as System.Windows.Controls.Button).ContextMenu.PlacementTarget = (sender as System.Windows.Controls.Button);
            (sender as System.Windows.Controls.Button).ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            (sender as System.Windows.Controls.Button).ContextMenu.IsOpen = true;
        }

        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {

            IEnumerable<ExpenseModel> backup = new ObservableCollection<ExpenseModel>((IEnumerable<ExpenseModel>)produtsdataGrid.ItemsSource);
           
            if (e.Key == Key.Return)
            {
                if (!textBox.Text.ToString().Equals(""))
                {

                    IEnumerable<ExpenseModel> temp = new ObservableCollection<ExpenseModel>();
                    temp = ((IEnumerable<ExpenseModel>)produtsdataGrid.ItemsSource).Where(item => item.Date.ToLower().Contains(textBox.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                    amount_label.Content = ((IEnumerable<ExpenseModel>)produtsdataGrid.ItemsSource).Sum(item => item.Amount);

                }
                else
                {
                    if (textBox.Text.Equals("") && textBox_Copy1.Text.Equals("[Reference]") && textBox_Copy3.Text.Equals("[Note]") && createdby_Copy3.Text.Equals("[Created By]"))
                    {
                        textBox.Text = "[Date]";
                        produtsdataGrid.ItemsSource = ExpenseModel.AllExpenses;
                        produtsdataGrid.Items.Refresh();
                        amount_label.Content = ((IEnumerable<ExpenseModel>)produtsdataGrid.ItemsSource).Sum(item => item.Amount);

                    }
                    else
                    {
                        textBox.Text = "[Date]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();
                        amount_label.Content = ((IEnumerable<ExpenseModel>)produtsdataGrid.ItemsSource).Sum(item => item.Amount);

                    }
                }
            }
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void textBox_MouseUp(object sender, MouseButtonEventArgs e)
        {

        }
       
        private void textBox_Copy_MouseUp(object sender, MouseButtonEventArgs e)
        {

        }


        private void textBox_Copy1_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void textBox_Copy1_KeyDown(object sender, KeyEventArgs e)
        {
            IEnumerable<ExpenseModel> backup = new ObservableCollection<ExpenseModel>((IEnumerable<ExpenseModel>)produtsdataGrid.ItemsSource);

            if (e.Key == Key.Return)
            {
                if (!textBox_Copy1.Text.ToString().Equals(""))
                {

                    IEnumerable<ExpenseModel> temp = new ObservableCollection<ExpenseModel>();
                    temp = ((IEnumerable<ExpenseModel>)produtsdataGrid.ItemsSource).Where(item => item.Name.ToLower().Contains(textBox_Copy1.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                    amount_label.Content = ((IEnumerable<ExpenseModel>)produtsdataGrid.ItemsSource).Sum(item => item.Amount);

                }
                else
                {
                    if (textBox.Text.Equals("[Date]") && textBox_Copy1.Text.Equals("") && textBox_Copy3.Text.Equals("[Note]") && createdby_Copy3.Text.Equals("[Created By]"))
                    {
                        textBox_Copy1.Text = "[Reference]";
                        produtsdataGrid.ItemsSource = ExpenseModel.AllExpenses;
                        produtsdataGrid.Items.Refresh();
                        amount_label.Content = ((IEnumerable<ExpenseModel>)produtsdataGrid.ItemsSource).Sum(item => item.Amount);

                    }
                    else
                    {
                        textBox_Copy1.Text = "[Date]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();
                        amount_label.Content = ((IEnumerable<ExpenseModel>)produtsdataGrid.ItemsSource).Sum(item => item.Amount);

                    }
                }
            }
        }



        private void textBox_Copy3_TextChanged(object sender, TextChangedEventArgs e)
        {


        }

        private void textBox_Copy3_KeyDown(object sender, KeyEventArgs e)
        {
            IEnumerable<ExpenseModel> backup = new ObservableCollection<ExpenseModel>((IEnumerable<ExpenseModel>)produtsdataGrid.ItemsSource);

            if (e.Key == Key.Return)
            {
                if (!textBox_Copy3.Text.ToString().Equals(""))
                {

                    IEnumerable<ExpenseModel> temp = new ObservableCollection<ExpenseModel>();
                    temp = ((IEnumerable<ExpenseModel>)produtsdataGrid.ItemsSource).Where(item => item.Note.ToLower().Contains(textBox_Copy3.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                    amount_label.Content = ((IEnumerable<ExpenseModel>)produtsdataGrid.ItemsSource).Sum(item => item.Amount);

                }
                else
                {
                    if (textBox.Text.Equals("[Date]") && textBox_Copy1.Text.Equals("[Reference]") && textBox_Copy3.Text.Equals("") && textBox_Copy3.Text.Equals("[Created By]"))
                    {
                        textBox_Copy3.Text = "[Note]";
                        produtsdataGrid.ItemsSource = ExpenseModel.AllExpenses;
                        produtsdataGrid.Items.Refresh();
                        amount_label.Content = ((IEnumerable<ExpenseModel>)produtsdataGrid.ItemsSource).Sum(item => item.Amount);

                    }
                    else
                    {
                        textBox_Copy3.Text = "[Note]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();
                        amount_label.Content = ((IEnumerable<ExpenseModel>)produtsdataGrid.ItemsSource).Sum(item => item.Amount);

                    }
                }
            }
        }

        private void textBox_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox.Text = "[Code]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void textBox_Copy_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy.Text = "[Name]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void textBox_Copy1_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy1.Text = "[Type]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void textBox_Copy2_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy2.Text = "[Catagory]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void textBox_Copy3_LostFocus(object sender, RoutedEventArgs e)
        {
            /*textBox_Copy3.Text = "[Quantity]";
            produtsdataGrid.ItemsSource = Product.AllProducts;
            produtsdataGrid.Items.Refresh();*/
        }

        private void createdby_KeyDown(object sender, KeyEventArgs e)
        {
            IEnumerable<ExpenseModel> backup = new ObservableCollection<ExpenseModel>((IEnumerable<ExpenseModel>)produtsdataGrid.ItemsSource);

            if (e.Key == Key.Return)
            {
                if (!createdby_Copy3.Text.ToString().Equals(""))
                {

                    IEnumerable<ExpenseModel> temp = new ObservableCollection<ExpenseModel>();
                    temp = ((IEnumerable<ExpenseModel>)produtsdataGrid.ItemsSource).Where(item => item.Created_by.ToLower().Contains(createdby_Copy3.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                    amount_label.Content = ((IEnumerable<ExpenseModel>)produtsdataGrid.ItemsSource).Sum(item => item.Amount);

                }
                else
                {
                    if (textBox.Text.Equals("[Date]") && textBox_Copy1.Text.Equals("[Reference]") && textBox_Copy3.Text.Equals("[Note]") && textBox_Copy3.Text.Equals(""))
                    {
                        createdby_Copy3.Text = "[Created By]";
                        produtsdataGrid.ItemsSource = ExpenseModel.AllExpenses;
                        produtsdataGrid.Items.Refresh();
                        amount_label.Content = ((IEnumerable<ExpenseModel>)produtsdataGrid.ItemsSource).Sum(item => item.Amount);

                    }
                    else
                    {
                        createdby_Copy3.Text = "[Created By]";
                        produtsdataGrid.ItemsSource = backup;
                        produtsdataGrid.Items.Refresh();
                        amount_label.Content = ((IEnumerable<ExpenseModel>)produtsdataGrid.ItemsSource).Sum(item => item.Amount);

                    }
                }
            }
        }

        private void produtsdataGrid_Loaded(object sender, RoutedEventArgs e)
        {
            amount_label.Content = ((ObservableCollection<ExpenseModel>)produtsdataGrid.ItemsSource).Sum(item => item.Amount);

        }

        private void produtsdataGrid_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            amount_label.Content = ((ObservableCollection<ExpenseModel>)produtsdataGrid.ItemsSource).Sum(item => item.Amount);

        }

        private void produtsdataGrid_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            amount_label.Content = ((ObservableCollection<ExpenseModel>)produtsdataGrid.ItemsSource).Sum(item => item.Amount);

        }
    }
    /* public class item :items
     {





         }
     public class item2 :items
     {

         public String Image { set; get; }



     }
     public class items {
         public BitmapImage Image { set; get; }
         public String Code { set; get; }
         public string Name { set; get; }
         public String Type { set; get; }
         public string Catagory { set; get; }
         public String Quantity { set; get; }
         public string Cost { set; get; }
         public string AlertQuantity { set; get; }

         public String Price { set; get; }
         public String Description { set; get; }
     }*/


}

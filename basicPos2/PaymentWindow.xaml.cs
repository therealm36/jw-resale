﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using basicPos2.models;
using System.Text.RegularExpressions;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for Payment.xaml
    /// </summary>
    public partial class PaymentWindow : Window
    {
        Pos _pos;
        public PaymentWindow(Pos pos)
        {
            InitializeComponent();

            _pos = pos;

            totalItemsLbl.Content = Sales.currentSale.TotalItems + "(" + Sales.currentSale.TotalQuantity + ")";
            totalPayableLbl.Content = Sales.currentSale.GrandTotal;
            allBtn.Content = Sales.currentSale.GrandTotal;
            
        }

        private void submitBtn_Click(object sender, RoutedEventArgs e)
        {
            if (totalPayingLbl.Text == "")
            {
                MessageBox.Show("Please insert the paying amount");
                return;
            }

            if (float.Parse(totalPayingLbl.Text)<Sales.currentSale.GrandTotal && Sales.currentSale.CustomerId == 1)
            {
                MessageBox.Show("Please add a customer for due payment");
                return;
            }

            Sales.currentSale.Note = noteTxt.Text;
            if (float.Parse(totalPayingLbl.Text) > Sales.currentSale.GrandTotal)
            {
                Sales.currentSale.Paid = Sales.currentSale.GrandTotal;
            }
            else
            {
                Sales.currentSale.Paid = float.Parse(totalPayingLbl.Text);
            }
            

            Sales.RunSalesTransaction(RealUtility.dbString, paymentNoteTxt.Text, ((ComboBoxItem)(payingByCombo.SelectedItem)).Content.ToString(), _pos);
            
            this.Close();
            
            
        }

        private void cancelBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void _10Btn_Click(object sender, RoutedEventArgs e)
        {
            amountTxt.Text = string.Format("{0:N2}", (float.Parse((string)(totalPayingLbl.Text)) + 10));
        }

        private void _20Btn_Click(object sender, RoutedEventArgs e)
        {
            amountTxt.Text = string.Format("{0:N2}", (float.Parse((string)(totalPayingLbl.Text)) + 20));
        }

        private void _50Btn_Click(object sender, RoutedEventArgs e)
        {
            amountTxt.Text = string.Format("{0:N2}", (float.Parse((string)(totalPayingLbl.Text)) + 50));
        }

        private void _100Btn_Click(object sender, RoutedEventArgs e)
        {
            amountTxt.Text = string.Format("{0:N2}", (float.Parse((string)(totalPayingLbl.Text)) + 100));
        }

        private void _500Btn_Click(object sender, RoutedEventArgs e)
        {
            amountTxt.Text = string.Format("{0:N2}", (float.Parse((string)(totalPayingLbl.Text)) + 500));
        }

        private void _1000Btn_Click(object sender, RoutedEventArgs e)
        {
            amountTxt.Text = string.Format("{0:N2}", (float.Parse((string)(totalPayingLbl.Text)) + 1000));
        }

        private void _2000Btn_Click(object sender, RoutedEventArgs e)
        {
            amountTxt.Text = string.Format("{0:N2}", (float.Parse((string)(totalPayingLbl.Text)) + 2000));
        }

        private void _5000Btn_Click(object sender, RoutedEventArgs e)
        {
            amountTxt.Text = string.Format("{0:N2}", (float.Parse((string)(totalPayingLbl.Text)) + 5000));
        }

        private void allBtn_Click(object sender, RoutedEventArgs e)
        {
            amountTxt.Text = string.Format("{0:N2}", Sales.currentSale.GrandTotal);
        }

        private void totalPayingLbl_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (balanceLbl !=null && (string)totalPayingLbl.Text != "")
            {
                balanceLbl.Content = float.Parse(totalPayingLbl.Text) - Sales.currentSale.GrandTotal;
            }
           
        }

        private void amountTxt_TextChanged(object sender, TextChangedEventArgs e)
        {
            totalPayingLbl.Text = string.Format("{0:N2}", amountTxt.Text);
        }

        private void payingByCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (paymentNoteLbl == null) return;
            if (payingByCombo.SelectedIndex == 1)
            {
                paymentNoteLbl.Content = "Checque No.";
            }
            else
            {
                paymentNoteLbl.Content = "Payment Note";
            }
        }

        private void amountTxt_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[+-]?([0-9]*[.])?[0-9]+");
            e.Handled = !regex.IsMatch(e.Text);
        }
    }
}

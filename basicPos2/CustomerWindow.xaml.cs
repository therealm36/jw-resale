﻿using basicPos2.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for CustomerWindow.xaml
    /// </summary>
    public partial class CustomerWindow : Window
    {
        Customer populator;
        public CustomerWindow(Customer p)
        {
            InitializeComponent();
            this.populator = p;
            if (populator != null)
            {

                Name.Text = p.Name;
                Phone.Text = p.Phone;
                Email.Text = p.Email;
                cf1.Text = p.Cf1;
                cf2.Text = p.Cf2;

            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Name.BorderBrush = Brushes.Black;
            Phone.BorderBrush = Brushes.Black;
            Email.BorderBrush = Brushes.Black;
            if (Name.Text.Equals(""))
            {
                Name.BorderBrush = Brushes.Red;
                MessageBox.Show("Please enter a Name");
            }else
            {
                if (false)
                {
                    Phone.BorderBrush = Brushes.Red;
                    MessageBox.Show("Please enter a phone number");
                }else
                {
                    if (false)
                    {
                        Email.BorderBrush = Brushes.Red;
                        MessageBox.Show("Please enter a Email ID");

                    }else
                    {
                        if (populator != null)
                        {

                            Customer.editCustomer(Customer.AllCustomers.IndexOf(populator), new Customer { Id = populator.Id, Name = Name.Text, Phone = Phone.Text, Email = Email.Text, Cf1 = cf1.Text, Cf2 = cf2.Text });
                            this.Close();
                        }
                        else
                        {
                            Customer.AddToAllCustomers(new Customer { Id = Customer.AllCustomers.Count + 1, Name = Name.Text, Phone = Phone.Text, Email = Email.Text, Cf1 = cf1.Text, Cf2 = cf2.Text });
                            this.Close();
                        }
                    }
                }
            }
           
        }
    }
}

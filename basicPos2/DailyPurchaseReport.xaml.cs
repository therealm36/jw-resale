﻿using basicPos2.models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace basicPos2
{
    /// <summary>
    /// Interaction logic for DailyReport.xaml
    /// </summary>
    public partial class DailyPurchaseReport : UserControl
    {
        public DailyPurchaseReport()
        {
            InitializeComponent();
           // produtsdataGrid.ItemsSource = basicPos2.models.DailyReport.DailyGoldSmithreport(1);
            catagorycombobox.ItemsSource = Supplier.AllSupliers;
            try
            {
                //totallabel.Content = "Money Owed to me-->" + (((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.MoneyGiven) - ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.MoneyToGive));
               // totaldiscountlabel.Content = "Gold Owed to me-->" + (((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.GoldGiven) - ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.GoldToGive));
            }
            catch { }
            //Datestart.Text = DateTime.Today.AddDays(-7).ToString();
            //Dateend.Text = DateTime.Today.ToString();
            //totallabel.Content = ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.Total);
            //totaldiscountlabel.Content = ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.TotalDiscount);
            //grandtotallabel.Content = ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
            //totalpaidlabel.Content = ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.TotalPaid);
        }

        private void button5_Click(object sender, RoutedEventArgs e)
        {
            // Datestart.BorderBrush = Brushes.Black;
            //Dateend.BorderBrush = Brushes.Black;
            catagorycombobox.BorderBrush = Brushes.Black;

            if (catagorycombobox.SelectedItem == null)
            {
                catagorycombobox.BorderBrush = Brushes.Red;
                MessageBox.Show("Please input a valid customer");
            }
            else
            {
                produtsdataGrid.ItemsSource = basicPos2.models.DailyReport.DailyGoldSmithreport(((Supplier)catagorycombobox.SelectedItem).Id);
                try
                {
                    totallabel.Content = "Money currently Owed to him-->" + (basicPos2.models.DailyReport.Dailygoldpricereport(((Supplier)catagorycombobox.SelectedItem).Id) - ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.MoneyGiven));
                    totaldiscountlabel.Content = "Gold given to him-->" +  ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.GoldGiven);
                    textBox.Content = "Total Money Owed to him = " + basicPos2.models.DailyReport.Dailygoldpricereport(((Supplier)catagorycombobox.SelectedItem).Id);
                }
                catch(Exception er) { MessageBox.Show(er.ToString()); }
                // totallabel.Content = ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.Total);
                //totaldiscountlabel.Content = ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.TotalDiscount);
                //grandtotallabel.Content = ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
                //totalpaidlabel.Content = ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.TotalPaid);

            }

        }

        private static AddPayment addpay = null;
        private void addpaybtn_Click(object sender, RoutedEventArgs e)
        {
            if (addpay == null || addpay.IsLoaded == false)
            {
                try { 
                addpay = new AddPayment(null, null, new Gold_Smith_orders { Id = -1, GoldSmithID = ((Supplier)catagorycombobox.SelectedItem).Id }, null, null, null);

                addpay.Title = "Add Payment";
                addpay.Show();
            }catch (Exception er)
            {
                MessageBox.Show("Please select a Gold Smith", "Error");
            }
        }
            else
            {
                addpay.Activate();
            }

        }

      /*  private void textBox_KeyDown(object sender, KeyEventArgs e)
        {

            IEnumerable<basicPos2.models.DailyReport> backup = new ObservableCollection<basicPos2.models.DailyReport>((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource);

            if (e.Key == Key.Return)
            {
                if (!textBox.Text.ToString().Equals(""))
                {

                    IEnumerable<basicPos2.models.DailyReport> temp = new ObservableCollection<basicPos2.models.DailyReport>();
                    temp = ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Where(item => item.GoldSmithName.Contains(textBox.Text.ToLower()));
                    produtsdataGrid.ItemsSource = null;
                    produtsdataGrid.ItemsSource = temp;
                    produtsdataGrid.Items.Refresh();
                    try
                    {
                        totallabel.Content = "Money Owed to me-->" + (((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.MoneyGiven) - ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.MoneyToGive));
                        totaldiscountlabel.Content = "Gold Owed to me-->" + (((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.GoldGiven) - ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.GoldToGive));
                    }
                    catch { }
                        // grandtotallabel.Content = ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
                    // totalpaidlabel.Content = ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.TotalPaid);

                }
                else
                {
                    textBox.Text = "[GoldSmith Name]";
                    produtsdataGrid.ItemsSource = backup;
                    produtsdataGrid.Items.Refresh();

                    try
                    {
                        totallabel.Content = "Money Owed to me-->" + (((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.MoneyGiven) - ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.MoneyToGive));
                        totaldiscountlabel.Content = "Gold Owed to me-->" + (((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.GoldGiven) - ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.GoldToGive));
                    }
                    catch { }
                    // totallabel.Content = ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.Total);
                    //  totaldiscountlabel.Content = ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.TotalDiscount);
                    //  grandtotallabel.Content = ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.GrandTotal);
                    //  totalpaidlabel.Content = ((IEnumerable<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).Sum(item => item.TotalPaid);

                }
            }
        }
        */
        private void button3_Click(object sender, RoutedEventArgs e)
        {
            RealUtility.ExportToPdfpurchasereport(produtsdataGrid);
        }




        private void button2_Click(object sender, RoutedEventArgs e)
        {
            RealUtility.Exporttocsvreport(produtsdataGrid);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.IO.Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\pos");
                RealUtility.GenerateExcelreport(((ObservableCollection<basicPos2.models.DailyReport>)produtsdataGrid.ItemsSource).ToDataTable());
                RealUtility.workBook.SaveAs(RealUtility.pathfornarnia + "DailySalesReport--" + RealUtility.todaydate + ".xls", System.IO.FileMode.Create, FileAccess.ReadWrite);
                RealUtility.workBook.Close();
                RealUtility.excel.Quit();

                MessageBox.Show("Successfuly Exported as Excel File", "Success");
            }
            catch
            {
                MessageBox.Show("Please close all instances of excel and try again", "Error");
            }

        }
        private void SelectAddress(object sender, RoutedEventArgs e)

        {

            System.Windows.Controls.TextBox tb = (sender as System.Windows.Controls.TextBox);

            if (tb != null)

            {

                tb.SelectAll();

            }

        }



        private void SelectivelyIgnoreMouseButton(object sender,

            MouseButtonEventArgs e)

        {

            System.Windows.Controls.TextBox tb = (sender as System.Windows.Controls.TextBox);

            if (tb != null)

            {

                if (!tb.IsKeyboardFocusWithin)

                {

                    e.Handled = true;

                    tb.Focus();

                }

            }

        }
    }
}
